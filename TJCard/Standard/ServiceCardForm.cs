﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;

namespace TJCard
{
  public class ServiceCardForm
  {
    private ServiceCardForm() { }

    public static ServiceCardForm Create() => new ServiceCardForm();

    public static Exception ErrorDuplicateRowFound { get; } = new Exception("An error occurred while filling out the form with lines. Some lines have the same value for the [IdRow] property.");

    public FxCardMain AddCardForm(Control control, Action<FxCardMain> FillFormWithData, bool ModeCreateNewObject, bool ModeDebug)
    {
      FxCardMain card = new FxCardMain();
      card.Visible = false;
      card.FormBorderStyle = FormBorderStyle.None;
      card.TopLevel = false;
      control.Controls.Add(card);
      card.Dock = DockStyle.Fill;
      card.SetMode(ModeCreateNewObject, ModeDebug);

      card.BeginInit();
      card.GxCard.BeginUpdate();

      FillFormWithData(card);
      if (card.CardData.FindDuplicates()) throw ErrorDuplicateRowFound;

      card.GxCard.EndUpdate();
      card.EndInit();
      card.Show();

      return card;
    }

    public FxCardImages AddCardImagesForm(Control control, string entityName)
    {
      FxCardImages card = new FxCardImages();
      card.Visible = false;
      card.FormBorderStyle = FormBorderStyle.None;
      card.TopLevel = false;
      control.Controls.Add(card);
      card.Dock = DockStyle.Fill;
      card.EntityName = entityName;
      card.Configure();
      card.Show();
      return card;
    }

    public FxCardFiles AddCardFilesForm(Control control, string entityName)
    {
      FxCardFiles card = new FxCardFiles();
      card.Visible = false;
      card.FormBorderStyle = FormBorderStyle.None;
      card.TopLevel = false;
      control.Controls.Add(card);
      card.Dock = DockStyle.Fill;
      card.EntityName = entityName;
      card.Configure();
      card.Show();
      return card;
    }

    public FxCardText AddCardTextForm(Control control, BindingList<HierarchicalClassifier> dataSource, string NameOfTextPropertiesGroup, string entityName)
    {
      FxCardText card = new FxCardText();
      card.Visible = false;
      card.FormBorderStyle = FormBorderStyle.None;
      card.TopLevel = false;
      control.Controls.Add(card);
      card.Dock = DockStyle.Fill;
      card.EntityName = entityName;
      card.Configure();
      card.CardData.AddGroupTextProperties(dataSource, NameOfTextPropertiesGroup);
      card.Show();
      return card;
    }

    public FxCardDelete AddCardDelete(Control control, string entityName)
    {
      FxCardDelete card = new FxCardDelete();
      card.Visible = false;
      card.FormBorderStyle = FormBorderStyle.None;
      card.TopLevel = false;
      control.Controls.Add(card);
      card.Dock = DockStyle.Fill;
      card.EntityName = entityName;
      card.Configure();
      card.CheckIdObject();
      card.Show();
      return card;
    }
  }
}
