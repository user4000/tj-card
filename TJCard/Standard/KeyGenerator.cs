﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace TJCard
{
  public class KeyGenerator
  {
    internal static readonly char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyuiopasdfghjklzxcvbnm1234567890".ToCharArray();

    public static string GetKey() => GetUniqueKey(10);
  
    public static string GetNewIdRow() => GetSecondsBase36() + "-" + GetKey();

    public static DateTime DateOrigin { get; } = new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

    public static string GetUniqueKey(int size)
    {
      byte[] data = new byte[4 * size];
      using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
      {
        crypto.GetBytes(data);
      }
      StringBuilder result = new StringBuilder(size);
      for (int i = 0; i < size; i++)
      {
        var rnd = BitConverter.ToUInt32(data, i * 4);
        var idx = rnd % chars.Length;

        result.Append(chars[idx]);
      }
      return result.ToString();
    }

    public static string GetUniqueKeyOriginal_BIASED(int size)
    {
      char[] chars =
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
      byte[] data = new byte[size];
      using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
      {
        crypto.GetBytes(data);
      }
      StringBuilder result = new StringBuilder(size);
      foreach (byte b in data)
      {
        result.Append(chars[b % (chars.Length)]);
      }
      return result.ToString();
    }

    public static DateTime ConvertFromUnixTimestamp(double timestamp)
    {     
      return DateOrigin.AddSeconds(timestamp);
    }

    public static double ConvertToUnixTimestamp(DateTime date)
    {
      TimeSpan diff = date.ToUniversalTime() - DateOrigin;
      return Math.Floor(diff.TotalSeconds);
    }

    public static string GetSeconds() => ConvertToUnixTimestamp(DateTime.Now).ToString();

    public static string GetSecondsBase36()
    {
      string s = ConvertToUnixTimestamp(DateTime.Now).ToString();
      if (Int64.TryParse(s, out long x))
        return Standard.DecimalToBase36(x);
      else
        return s;
    }
  }
}