﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using TJCard.Tools;
using System.Drawing;
using System.Diagnostics;
using System.Globalization;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace TJCard
{
  public static class Standard
  {
    public static NumberFormatInfo nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();

    public static Font MainFont { get; set; }

    static Standard()
    {
      nfi.NumberGroupSeparator = " ";
      MainFont = new Font("Verdana", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
    }

    public static char MessageHeaderSeparator { get; } = '|';

    public static string GridColumnPrefix { get; } = "Cc";

    public static string GetGridColumnName(string ColumnName) => $"{GridColumnPrefix}{ColumnName}";

    public static string HeaderAndMessage(string header, string message)
    {
      return header + MessageHeaderSeparator + message;
    }

    public static bool IsEmpty(string value) => string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value.Trim());

    public static string GetDateOfPdbFile() => CxConvert.ToString(File.GetLastWriteTime($"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.pdb"));

    public static string GetDateOfFile(string FileName) => CxConvert.ToString(File.GetLastWriteTime(FileName));

    public static string GetVersionOfFile(string FileName) => FileVersionInfo.GetVersionInfo(FileName).FileVersion;

    public static void Debug(string value) => Console.WriteLine(value); // TODO: В релизе нужно убрать все вызовы этого метода

    public static string GetExecutingAssemblyPath() => Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

    public static Tuple<string, string> HeaderAndMessage(string MessageWithHeader)
    {
      string header, message; header = message = string.Empty; int count = 0;
      string[] words = MessageWithHeader.Split(MessageHeaderSeparator);
      foreach (string word in words) if (++count == 1) { header = word; } else { message += word; }
      return Tuple.Create(header, message);
    }

    public static int CheckRange(int Variable, int MinValue, int MaxValue)
    {
      if (MinValue > MaxValue) MinValue = MaxValue;
      if (Variable > MaxValue) Variable = MaxValue;
      if (Variable < MinValue) Variable = MinValue;
      return Variable;
    }

    public static List<string> GetFiles(string directory, string subDirectory, string searchPattern, char separator = ' ')
    {
      List<string> result = new List<string>();
      if (String.IsNullOrWhiteSpace(directory)) directory = Environment.CurrentDirectory;
      string Folder = String.IsNullOrWhiteSpace(subDirectory) ? directory : Path.Combine(directory, subDirectory);
      string[] array = searchPattern.Split(separator);
      if (Directory.Exists(Folder))
        foreach (string pattern in array)
          foreach (string file in Directory.EnumerateFiles(Folder, pattern))
            result.Add(file);
      return result;
    }

    public static long FileLength(string FileName)
    {
      return new FileInfo(FileName).Length;
    }

    public static string FormatInteger(long number)
    {
      return number.ToString("#,0", nfi);
    }

    public static string DecimalToBase36(long decimalNumber) => DecimalToArbitrarySystem(decimalNumber, 36);

    public static string DecimalToArbitrarySystem(long decimalNumber, int radix = 36)
    {
      const int BitsInLong = 64;
      const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

      if (radix < 2 || radix > Digits.Length)
        throw new ArgumentException("The radix must be >= 2 and <= " +
            Digits.Length.ToString());

      if (decimalNumber == 0)
        return "0";

      int index = BitsInLong - 1;
      long currentNumber = Math.Abs(decimalNumber);
      char[] charArray = new char[BitsInLong];

      while (currentNumber != 0)
      {
        int remainder = (int)(currentNumber % radix);
        charArray[index--] = Digits[remainder];
        currentNumber = currentNumber / radix;
      }

      string result = new String(charArray, index + 1, BitsInLong - index - 1);
      if (decimalNumber < 0)
      {
        result = "-" + result;
      }

      return result;
    }

    public static string ToHexString(byte[] bytes)
    {
      StringBuilder sb = new StringBuilder();
      foreach (byte b in bytes)
        sb.Append(b.ToString("x2").ToLower());
      return sb.ToString();
    }

    public static string HidePasswordValue(string Password) // Returns hidden symbols instead of a plain text //
    {
      return string.Concat(Enumerable.Repeat("# ", Password.Length));
    }

    public static string HashMd5(string FileName)
    {
      try
      {
        using (FileStream fs = File.OpenRead(FileName))
        using (HashAlgorithm hashAlgorithm = MD5.Create())
        {
          byte[] hash = hashAlgorithm.ComputeHash(fs);
          return (ToHexString(hash));
        }
      }
      catch
      {
        return string.Empty;
      }
    }

    public static string HashSha1(string FileName)
    {
      using (FileStream fs = File.OpenRead(FileName))
      using (HashAlgorithm hashAlgorithm = SHA1.Create())
      {
        byte[] hash = hashAlgorithm.ComputeHash(fs);
        return (ToHexString(hash));
      }
    }

    public static string HashSha256(string FileName)
    {
      using (FileStream fs = File.OpenRead(FileName))
      using (HashAlgorithm hashAlgorithm = SHA256.Create())
      {
        byte[] hash = hashAlgorithm.ComputeHash(fs);
        return (ToHexString(hash));
      }
    }

    public static string HashAndSize(string fileName, long fileLength = 0)
    {
      if (fileLength < 1) fileLength = Standard.FileLength(fileName);
      return Standard.HashSha256(fileName) + "-" + fileLength.ToString();
    }
  }
}
