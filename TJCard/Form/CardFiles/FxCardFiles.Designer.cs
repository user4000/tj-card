﻿namespace TJCard
{
    partial class FxCardFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FxCardFiles));
      this.PnGrid = new Telerik.WinControls.UI.RadPanel();
      this.PnGridCenter = new Telerik.WinControls.UI.RadPanel();
      this.GxCard = new Telerik.WinControls.UI.RadGridView();
      this.PnGridTop = new Telerik.WinControls.UI.RadPanel();
      this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
      this.BxFileSaveToDisk = new Telerik.WinControls.UI.RadButton();
      this.radSeparator5 = new Telerik.WinControls.UI.RadSeparator();
      this.BxFileDelete = new Telerik.WinControls.UI.RadButton();
      this.radSeparator4 = new Telerik.WinControls.UI.RadSeparator();
      this.BxFileAdd = new Telerik.WinControls.UI.RadButton();
      this.radSeparator2 = new Telerik.WinControls.UI.RadSeparator();
      this.BxDownload = new Telerik.WinControls.UI.RadButton();
      this.radSeparator6 = new Telerik.WinControls.UI.RadSeparator();
      this.BxUpload = new Telerik.WinControls.UI.RadButton();
      this.PnGridBottom = new Telerik.WinControls.UI.RadPanel();
      this.SpMain = new System.Windows.Forms.Splitter();
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.PgStart = new Telerik.WinControls.UI.RadPageViewPage();
      ((System.ComponentModel.ISupportInitialize)(this.PnGrid)).BeginInit();
      this.PnGrid.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridCenter)).BeginInit();
      this.PnGridCenter.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.GxCard)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxCard.MasterTemplate)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridTop)).BeginInit();
      this.PnGridTop.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxFileSaveToDisk)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxFileDelete)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxFileAdd)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator6)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxUpload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridBottom)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PnGrid
      // 
      this.PnGrid.Controls.Add(this.PnGridCenter);
      this.PnGrid.Controls.Add(this.PnGridTop);
      this.PnGrid.Controls.Add(this.PnGridBottom);
      this.PnGrid.Dock = System.Windows.Forms.DockStyle.Left;
      this.PnGrid.Location = new System.Drawing.Point(0, 0);
      this.PnGrid.Name = "PnGrid";
      this.PnGrid.Size = new System.Drawing.Size(700, 470);
      this.PnGrid.TabIndex = 2;
      // 
      // PnGridCenter
      // 
      this.PnGridCenter.Controls.Add(this.GxCard);
      this.PnGridCenter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PnGridCenter.Location = new System.Drawing.Point(0, 36);
      this.PnGridCenter.Name = "PnGridCenter";
      this.PnGridCenter.Size = new System.Drawing.Size(700, 398);
      this.PnGridCenter.TabIndex = 2;
      // 
      // GxCard
      // 
      this.GxCard.Dock = System.Windows.Forms.DockStyle.Fill;
      this.GxCard.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.GxCard.Location = new System.Drawing.Point(0, 0);
      // 
      // 
      // 
      this.GxCard.MasterTemplate.AllowAddNewRow = false;
      this.GxCard.MasterTemplate.AllowColumnReorder = false;
      this.GxCard.MasterTemplate.AllowDeleteRow = false;
      this.GxCard.MasterTemplate.AllowEditRow = false;
      this.GxCard.MasterTemplate.AllowRowResize = false;
      gridViewTextBoxColumn1.FieldName = "IdRow";
      gridViewTextBoxColumn1.HeaderText = "CnIdRow";
      gridViewTextBoxColumn1.IsVisible = false;
      gridViewTextBoxColumn1.MaxLength = 50;
      gridViewTextBoxColumn1.Name = "CnIdRow";
      gridViewTextBoxColumn1.ReadOnly = true;
      gridViewTextBoxColumn2.FieldName = "IdParent";
      gridViewTextBoxColumn2.HeaderText = "CnIdParent";
      gridViewTextBoxColumn2.IsVisible = false;
      gridViewTextBoxColumn2.MaxLength = 50;
      gridViewTextBoxColumn2.Name = "CnIdParent";
      gridViewTextBoxColumn2.ReadOnly = true;
      gridViewTextBoxColumn3.FieldName = "IdProperty";
      gridViewTextBoxColumn3.HeaderText = "CnIdProperty";
      gridViewTextBoxColumn3.IsVisible = false;
      gridViewTextBoxColumn3.MaxLength = 250;
      gridViewTextBoxColumn3.Name = "CnIdProperty";
      gridViewTextBoxColumn3.ReadOnly = true;
      gridViewTextBoxColumn4.FieldName = "PropertyName";
      gridViewTextBoxColumn4.HeaderText = "CnName";
      gridViewTextBoxColumn4.MaxLength = 200;
      gridViewTextBoxColumn4.Name = "CnName";
      gridViewTextBoxColumn4.ReadOnly = true;
      gridViewTextBoxColumn4.Width = 200;
      gridViewTextBoxColumn5.FieldName = "DisplayValue";
      gridViewTextBoxColumn5.HeaderText = "CnValue";
      gridViewTextBoxColumn5.Name = "CnValue";
      gridViewTextBoxColumn5.ReadOnly = true;
      gridViewTextBoxColumn5.Width = 250;
      gridViewCheckBoxColumn1.FieldName = "ReadOnly";
      gridViewCheckBoxColumn1.HeaderText = "CnReadOnly";
      gridViewCheckBoxColumn1.IsVisible = false;
      gridViewCheckBoxColumn1.Name = "CnReadOnly";
      gridViewTextBoxColumn6.DataType = typeof(int);
      gridViewTextBoxColumn6.FieldName = "Type";
      gridViewTextBoxColumn6.HeaderText = "CnType";
      gridViewTextBoxColumn6.IsVisible = false;
      gridViewTextBoxColumn6.MaxLength = 50;
      gridViewTextBoxColumn6.Name = "CnType";
      gridViewTextBoxColumn7.FieldName = "JsonValue";
      gridViewTextBoxColumn7.HeaderText = "CnJson";
      gridViewTextBoxColumn7.IsVisible = false;
      gridViewTextBoxColumn7.MaxLength = 10000000;
      gridViewTextBoxColumn7.Name = "CnJson";
      this.GxCard.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
      this.GxCard.MasterTemplate.EnableGrouping = false;
      this.GxCard.MasterTemplate.EnableSorting = false;
      this.GxCard.MasterTemplate.ShowColumnHeaders = false;
      this.GxCard.MasterTemplate.ViewDefinition = tableViewDefinition1;
      this.GxCard.Name = "GxCard";
      this.GxCard.NewRowEnterKeyMode = Telerik.WinControls.UI.RadGridViewNewRowEnterKeyMode.None;
      this.GxCard.ReadOnly = true;
      this.GxCard.ShowGroupPanel = false;
      this.GxCard.ShowNoDataText = false;
      this.GxCard.Size = new System.Drawing.Size(700, 398);
      this.GxCard.TabIndex = 0;
      // 
      // PnGridTop
      // 
      this.PnGridTop.Controls.Add(this.radSeparator1);
      this.PnGridTop.Controls.Add(this.BxFileSaveToDisk);
      this.PnGridTop.Controls.Add(this.radSeparator5);
      this.PnGridTop.Controls.Add(this.BxFileDelete);
      this.PnGridTop.Controls.Add(this.radSeparator4);
      this.PnGridTop.Controls.Add(this.BxFileAdd);
      this.PnGridTop.Controls.Add(this.radSeparator2);
      this.PnGridTop.Controls.Add(this.BxDownload);
      this.PnGridTop.Controls.Add(this.radSeparator6);
      this.PnGridTop.Controls.Add(this.BxUpload);
      this.PnGridTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnGridTop.Location = new System.Drawing.Point(0, 0);
      this.PnGridTop.Name = "PnGridTop";
      this.PnGridTop.Padding = new System.Windows.Forms.Padding(5, 4, 0, 0);
      this.PnGridTop.Size = new System.Drawing.Size(700, 36);
      this.PnGridTop.TabIndex = 2;
      ((Telerik.WinControls.UI.RadPanelElement)(this.PnGridTop.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(5, 4, 0, 0);
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnGridTop.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      // 
      // radSeparator1
      // 
      this.radSeparator1.Dock = System.Windows.Forms.DockStyle.Left;
      this.radSeparator1.Location = new System.Drawing.Point(655, 4);
      this.radSeparator1.Name = "radSeparator1";
      this.radSeparator1.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.radSeparator1.Size = new System.Drawing.Size(20, 32);
      this.radSeparator1.TabIndex = 6;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator1.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator1.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator1.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxFileSaveToDisk
      // 
      this.BxFileSaveToDisk.AutoSize = true;
      this.BxFileSaveToDisk.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxFileSaveToDisk.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxFileSaveToDisk.Image = ((System.Drawing.Image)(resources.GetObject("BxFileSaveToDisk.Image")));
      this.BxFileSaveToDisk.Location = new System.Drawing.Point(532, 4);
      this.BxFileSaveToDisk.Name = "BxFileSaveToDisk";
      this.BxFileSaveToDisk.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxFileSaveToDisk.Size = new System.Drawing.Size(123, 32);
      this.BxFileSaveToDisk.TabIndex = 0;
      this.BxFileSaveToDisk.Text = "   Сохранить";
      this.BxFileSaveToDisk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // radSeparator5
      // 
      this.radSeparator5.Dock = System.Windows.Forms.DockStyle.Left;
      this.radSeparator5.Location = new System.Drawing.Point(512, 4);
      this.radSeparator5.Name = "radSeparator5";
      this.radSeparator5.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.radSeparator5.Size = new System.Drawing.Size(20, 32);
      this.radSeparator5.TabIndex = 9;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator5.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator5.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator5.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxFileDelete
      // 
      this.BxFileDelete.AutoSize = true;
      this.BxFileDelete.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxFileDelete.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxFileDelete.Image = ((System.Drawing.Image)(resources.GetObject("BxFileDelete.Image")));
      this.BxFileDelete.Location = new System.Drawing.Point(411, 4);
      this.BxFileDelete.Name = "BxFileDelete";
      this.BxFileDelete.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxFileDelete.Size = new System.Drawing.Size(101, 32);
      this.BxFileDelete.TabIndex = 0;
      this.BxFileDelete.Text = "   Удалить";
      this.BxFileDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // radSeparator4
      // 
      this.radSeparator4.Dock = System.Windows.Forms.DockStyle.Left;
      this.radSeparator4.Location = new System.Drawing.Point(391, 4);
      this.radSeparator4.Name = "radSeparator4";
      this.radSeparator4.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.radSeparator4.Size = new System.Drawing.Size(20, 32);
      this.radSeparator4.TabIndex = 8;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator4.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator4.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator4.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxFileAdd
      // 
      this.BxFileAdd.AutoSize = true;
      this.BxFileAdd.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxFileAdd.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxFileAdd.Image = ((System.Drawing.Image)(resources.GetObject("BxFileAdd.Image")));
      this.BxFileAdd.Location = new System.Drawing.Point(275, 4);
      this.BxFileAdd.Name = "BxFileAdd";
      this.BxFileAdd.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxFileAdd.Size = new System.Drawing.Size(116, 32);
      this.BxFileAdd.TabIndex = 0;
      this.BxFileAdd.Text = "   Добавить";
      this.BxFileAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // radSeparator2
      // 
      this.radSeparator2.Dock = System.Windows.Forms.DockStyle.Left;
      this.radSeparator2.Location = new System.Drawing.Point(255, 4);
      this.radSeparator2.Name = "radSeparator2";
      this.radSeparator2.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.radSeparator2.Size = new System.Drawing.Size(20, 32);
      this.radSeparator2.TabIndex = 5;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator2.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator2.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator2.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxDownload
      // 
      this.BxDownload.AutoSize = true;
      this.BxDownload.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxDownload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDownload.Image = ((System.Drawing.Image)(resources.GetObject("BxDownload.Image")));
      this.BxDownload.Location = new System.Drawing.Point(139, 4);
      this.BxDownload.Name = "BxDownload";
      this.BxDownload.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxDownload.Size = new System.Drawing.Size(116, 32);
      this.BxDownload.TabIndex = 0;
      this.BxDownload.Text = "   Получить";
      this.BxDownload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // radSeparator6
      // 
      this.radSeparator6.Dock = System.Windows.Forms.DockStyle.Left;
      this.radSeparator6.Location = new System.Drawing.Point(119, 4);
      this.radSeparator6.Name = "radSeparator6";
      this.radSeparator6.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.radSeparator6.Size = new System.Drawing.Size(20, 32);
      this.radSeparator6.TabIndex = 10;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator6.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator6.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.radSeparator6.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxUpload
      // 
      this.BxUpload.AutoSize = true;
      this.BxUpload.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxUpload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxUpload.Image = ((System.Drawing.Image)(resources.GetObject("BxUpload.Image")));
      this.BxUpload.Location = new System.Drawing.Point(5, 4);
      this.BxUpload.Name = "BxUpload";
      this.BxUpload.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxUpload.Size = new System.Drawing.Size(114, 32);
      this.BxUpload.TabIndex = 1;
      this.BxUpload.Text = "   Записать";
      this.BxUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // PnGridBottom
      // 
      this.PnGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnGridBottom.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnGridBottom.Location = new System.Drawing.Point(0, 434);
      this.PnGridBottom.Name = "PnGridBottom";
      this.PnGridBottom.Size = new System.Drawing.Size(700, 36);
      this.PnGridBottom.TabIndex = 2;
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnGridBottom.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      // 
      // SpMain
      // 
      this.SpMain.Location = new System.Drawing.Point(700, 0);
      this.SpMain.MinExtra = 15;
      this.SpMain.MinSize = 15;
      this.SpMain.Name = "SpMain";
      this.SpMain.Size = new System.Drawing.Size(5, 470);
      this.SpMain.TabIndex = 3;
      this.SpMain.TabStop = false;
      // 
      // PvMain
      // 
      this.PvMain.Controls.Add(this.PgStart);
      this.PvMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PvMain.Location = new System.Drawing.Point(705, 0);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.PgStart;
      this.PvMain.Size = new System.Drawing.Size(387, 470);
      this.PvMain.TabIndex = 4;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PvMain.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
      // 
      // PgStart
      // 
      this.PgStart.ItemSize = new System.Drawing.SizeF(40F, 28F);
      this.PgStart.Location = new System.Drawing.Point(10, 37);
      this.PgStart.Name = "PgStart";
      this.PgStart.Size = new System.Drawing.Size(366, 422);
      this.PgStart.Text = "Start";
      // 
      // FxCardFiles
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1092, 470);
      this.Controls.Add(this.PvMain);
      this.Controls.Add(this.SpMain);
      this.Controls.Add(this.PnGrid);
      this.Name = "FxCardFiles";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "";
      ((System.ComponentModel.ISupportInitialize)(this.PnGrid)).EndInit();
      this.PnGrid.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnGridCenter)).EndInit();
      this.PnGridCenter.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.GxCard.MasterTemplate)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxCard)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridTop)).EndInit();
      this.PnGridTop.ResumeLayout(false);
      this.PnGridTop.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxFileSaveToDisk)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxFileDelete)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxFileAdd)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radSeparator6)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxUpload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridBottom)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion
    private Telerik.WinControls.UI.RadPanel PnGridCenter;
    public Telerik.WinControls.UI.RadGridView GxCard;
    public Telerik.WinControls.UI.RadButton BxDownload;
    public Telerik.WinControls.UI.RadButton BxUpload;
    public Telerik.WinControls.UI.RadButton BxFileDelete;
    public System.Windows.Forms.Splitter SpMain;
    public Telerik.WinControls.UI.RadPageView PvMain;
    public Telerik.WinControls.UI.RadPageViewPage PgStart;
    public Telerik.WinControls.UI.RadButton BxFileAdd;
    public Telerik.WinControls.UI.RadButton BxFileSaveToDisk;
    public Telerik.WinControls.UI.RadPanel PnGridTop;
    public Telerik.WinControls.UI.RadPanel PnGridBottom;
    public Telerik.WinControls.UI.RadPanel PnGrid;
    public Telerik.WinControls.UI.RadSeparator radSeparator1;
    public Telerik.WinControls.UI.RadSeparator radSeparator5;
    public Telerik.WinControls.UI.RadSeparator radSeparator4;
    public Telerik.WinControls.UI.RadSeparator radSeparator2;
    public Telerik.WinControls.UI.RadSeparator radSeparator6;
  }
}
