﻿using System;
using System.IO;
using System.Linq;
using TJCard.Tools;
using System.Drawing;
using Telerik.WinControls;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TJCard
{
  public partial class FxCardFiles : RadForm, ICardForm
  {
    public string CurrentIdRow { get; private set; } = string.Empty;

    public string CurrentIdParent { get; private set; } = string.Empty;

    public CardProperty CurrentProperty { get; private set; } = null;

    public string CurrentFileName { get; private set; } = string.Empty;

    public Bitmap CurrentBitmap { get; private set; } = null;

    public string EntityName { get; internal set; } = string.Empty;

    public string GetIdObject { get => CardData.IdObject; }

    public string DirectoryToSaveFile { get; private set; } = string.Empty;

    public GridViewDataColumn CnName { get; set; } = null;

    public GridViewDataColumn CnValue { get; set; } = null;

    public DataOperatorCard CardData { get; private set; } = null;

    public Dictionary<CardProperty, RadPageViewPage> DcPage { get; } = new Dictionary<CardProperty, RadPageViewPage>();

    public int MaxFileSize { get; set; } = 10000000;

    public string StrFileNotSelected { get; set; } = "File is not selected";

    public string StrSelectFile { get; set; } = "You have to select a file";

    public string StrOverwriteFile { get; set; } = "Do you want to overwrite the file?";

    public string StrFileExists { get; set; } = "File already exists";

    public FxCardFiles()
    {
      InitializeComponent();
    }

    public void Configure()
    {
      SetProperties();
      SetGridProperties();
      SetEvents();
      CheckIdObject();
    }

    public void SetProperties()
    {
      BxFileDelete.Enabled = false;
      BxFileSaveToDisk.Enabled = false;
      foreach (var page in PvMain.Pages) page.Item.Visibility = ElementVisibility.Collapsed;
      BxDownload.Tag = this;
      BxUpload.Tag = this;
      BxFileAdd.Tag = this;
      BxFileDelete.Tag = this;
    }

    public void AdjustFormWidth()
    {
      PnGrid.Width = (this.Parent.Width * 59) / 100;
      CnName.Width = (PnGrid.Width * 62) / 100;
      CnValue.Width = (PnGrid.Width * 37) / 100;
    }

    public void SetGridProperties()
    {
      CardData = new DataOperatorCard();
      GxCard.ShowColumnHeaders = true;
      GxCard.TableElement.TableHeaderHeight = 25;
      GxCard.TableElement.RowHeight = 30;
      GxCard.ShowRowHeaderColumn = false;
      CnName = GxCard.Columns["CnName"];
      CnValue = GxCard.Columns["CnValue"];

      foreach (var column in GxCard.Columns) column.HeaderText = string.Empty;
      AdjustFormWidth();
      //GxCard.Relations.AddSelfReference(this.GxCard.MasterTemplate, "CnIdRow", "CnIdParent");

      GxCard.DataSource = CardData.DsList;
      GxCard.MasterTemplate.ExpandAll();
      GxCard.ZzGridClearSelection();
    }

    public void SetEvents()
    {
      BxFileAdd.Click += EventFileOpen;
      BxFileDelete.Click += EventFileDelete;
      BxFileSaveToDisk.Click += EventFileSaveToDisk;
      GxCard.SelectionChanged += EventSelectionChanged;
    }

    public void EventFileSaveToDisk(object sender, EventArgs e)
    {
      if ((CurrentProperty == null) || (CurrentProperty.StringValue == null))
      {
        RadMessageBox.Show(StrFileNotSelected, StrSelectFile, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        return;
      }

      FolderBrowserDialog folder = new FolderBrowserDialog();
      if (DirectoryToSaveFile.Length > 0)
        try { folder.SelectedPath = DirectoryToSaveFile; } catch { }

      DialogResult result = folder.ShowDialog();

      if (result == DialogResult.OK)
      {
        DirectoryToSaveFile = folder.SelectedPath;
        string FileName = Path.Combine(folder.SelectedPath, CurrentProperty.PropertyName);

        if (File.Exists(FileName))
        {
          string question =
            Path.GetDirectoryName(FileName) + "\r\n" +
            Path.GetFileName(FileName) + "\r\n" +
            StrOverwriteFile;

          DialogResult overwrite = RadMessageBox.Show(question, StrFileExists, MessageBoxButtons.YesNo, RadMessageIcon.Question);
          if (overwrite == DialogResult.No) return;
        }

        CardData.SaveBase64ToDisk(FileName, CurrentProperty.StringValue);
      }
    }

    public void EventUserSelectedOneRow() // Most important event that calls many other events //
    {
      CurrentIdRow = string.Empty; CurrentIdParent = CurrentIdRow; CurrentProperty = null;
      try
      {
        GridViewRowInfo row = GxCard.SelectedRows[0];
        if (row != null)
        {
          CurrentProperty = row.DataBoundItem as CardProperty;
          CurrentIdRow = CurrentProperty.IdRow;
          CurrentIdParent = CurrentProperty.IdParent ?? CurrentIdRow;
        }
      }
      catch { }

      if (CurrentProperty != null)
      {
        SetButtonsState();
        SelectPage();
      }
    }

    public void EventSelectionChanged(object sender, EventArgs e)
    {
      EventUserSelectedOneRow();
    }

    public void EventFileOpen(object sender, EventArgs e)
    {
      OpenFileDialog dialog = new OpenFileDialog();
      DialogResult result = dialog.ShowDialog();
      if (CardData.FileIsTooBig(result, dialog.FileName, MaxFileSize)) return;

      if (result == DialogResult.OK)
      {
        CurrentFileName = dialog.FileName;
        EventFileAdd(CurrentFileName);
      }
    }

    public void DeletePageFromDictionary(string IdRow)
    {
      CardProperty property = null;

      foreach (var item in DcPage)
        if (item.Key.IdRow == IdRow) { property = item.Key; break; }

      if (property == null) return;

      RadPageViewPage page = null;
      if (DcPage.TryGetValue(property, out RadPageViewPage PageFound)) page = PageFound;

      if (page == null) return;

      page.IsContentVisible = false;
      DcPage.Remove(property);
      PvMain.Pages.Remove(page);
      foreach (Control control in page.Controls) control.Dispose();
      page.Controls.Clear();
      page.Dispose();
    }

    public void TryToSelectRow(string IdRow)
    {
      foreach (var row in GxCard.Rows)
        if (row.Cells[0].Value.ToString() == IdRow)
        { GxCard.CurrentRow = row; break; }
    }

    public void DeleteAllFiles()
    {
      while (CardData.DsList.Count > 0)
      {
        CardProperty item = CardData.DsList.Last();
        EventFileDelete(item.IdRow);
      }
    }

    public void EventFileDelete(string IdRow)
    {
      bool deleted = CardData.DeleteProperty(IdRow);
      if (deleted) DeletePageFromDictionary(IdRow);
    }

    public async void EventFileDelete(object sender, EventArgs e)
    {
      BxFileDelete.Enabled = false;
      string IdRow = CurrentIdRow;
      string IdParent = CurrentIdParent;
      EventFileDelete(IdRow);
      TryToSelectRow(IdParent);
      await Task.Delay(200);
      SetButtonsState();
    }

    public void EventFileAdd(string FileName)
    {
      long FileLength = Standard.FileLength(FileName);
      string name = Path.GetFileName(FileName); ;
      string IdRow = KeyGenerator.GetNewIdRow();
      string IdProperty = Standard.HashAndSize(FileName, FileLength);
      string formattedFileLength = Standard.FormatInteger(FileLength) + " bytes";

      if (CardData.ThisFileAlreadyAdded(IdProperty, name)) return;

      string base64 = CxConvert.FileToBase64(FileName);
      CardData.AddFile(IdProperty, IdRow, name, formattedFileLength, base64);
      base64 = string.Empty;

      if (CardData.Exists(IdRow) == false) return;
      CurrentFileName = string.Empty;
    }

    public void SetButtonsState()
    {
      bool Flag =
        (CurrentProperty != null) &&
        (CurrentProperty.ReadOnly == false) &&
        (CardData.DsList.Count > 0);
      BxFileDelete.Enabled = Flag;
      BxFileSaveToDisk.Enabled = Flag;
    }

    public RadPageViewPage GetPage(CardProperty property)
    {
      RadPageViewPage result = null;
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page)) result = page;
      return result;
    }

    public void CreateNewPage()
    {
      RadPageViewPage page = new RadPageViewPage(CurrentIdRow);
      DcPage.Add(CurrentProperty, page);
      PvMain.Pages.Add(page);
      PvMain.SelectedPage = page;
      AddControlForFile(page, CurrentProperty, CurrentFileName);
      page.Item.Visibility = ElementVisibility.Collapsed;
    }

    public void TrySelectPage()
    {
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page))
      {
        PvMain.SelectedPage = page;
      }
    }

    public void SelectPage()
    {
      if (DcPage.ContainsKey(CurrentProperty))
      {
        TrySelectPage();
      }
      else
      {
        CreateNewPage();
      }
    }

    public void ShowChangedText(string value, bool RefreshGridRow = false)
    {
      CurrentProperty.DisplayValue = value;
      if (RefreshGridRow) GxCard.CurrentRow.InvalidateRow();
    }

    public Tuple<RadPanel, RadButton> CreateTopPanel(RadPageViewPage page, bool CreateClearButton)
    {
      RadPanel PnTop = new RadPanel();
      RadButton BxClear = CreateClearButton ? new RadButton() : null;

      page.Controls.Add(PnTop);

      PnTop.Dock = DockStyle.Top;
      PnTop.Font = Standard.MainFont;
      PnTop.Location = new Point(0, 0);
      PnTop.Name = "PnTop";
      PnTop.Size = new Size(431, 30);
      PnTop.TabIndex = 2;
      PnTop.Padding = new Padding(3, 3, 3, 3);

      if (BxClear != null)
      {
        BxClear.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
        BxClear.DisplayStyle = DisplayStyle.Image;
        BxClear.Image = PictureDepot.GetImageClear();
        BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
        BxClear.Location = new Point(400, 6);
        BxClear.Name = "BxClear";
        BxClear.Size = new Size(26, 24);
        BxClear.TabIndex = 0;
        BxClear.Text = "";
        BxClear.Dock = DockStyle.Right;
        BxClear.TabStop = false;
        PnTop.Controls.Add(BxClear);
      }

      PnTop.Text = "  " + CurrentProperty.PropertyName;

      return new Tuple<RadPanel, RadButton>(PnTop, BxClear);
    }

    public RadPanel CreateBottomPanel(RadPageViewPage page)
    {
      var PnBottom = new RadPanel();
      page.Controls.Add(PnBottom);

      PnBottom.Dock = DockStyle.Bottom;
      PnBottom.Font = Standard.MainFont;
      PnBottom.Location = new Point(0, 415);
      PnBottom.Name = "PnBottom";
      PnBottom.Size = new Size(431, 30);
      PnBottom.TabIndex = 2;

      return PnBottom;
    }

    public void AddControlForFile(RadPageViewPage page, CardProperty property, string fileName)
    {
      RadTextBoxControl TxFileName = new RadTextBoxControl();
      page.Controls.Add(TxFileName);
      page.Tag = TxFileName;

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, false);
      var BxClear = new RadButton();

      BottomPanel.Controls.Add(BxClear);
      BottomPanel.Padding = new Padding(5);

      TxFileName.Dock = DockStyle.Top;
      TxFileName.Font = Standard.MainFont;
      TxFileName.IsReadOnly = true;
      TxFileName.Location = new Point(0, 35);
      TxFileName.MaxLength = 2000;
      TxFileName.Name = "TxFileName";
      TxFileName.Size = new Size(374, 35);
      TxFileName.TabIndex = 3;
      TxFileName.Text = fileName;

      BxClear.Anchor = AnchorStyles.Right;
      BxClear.DisplayStyle = DisplayStyle.Image;
      BxClear.Image = PictureDepot.GetImageClear();
      BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
      BxClear.Location = new Point(340, 6);
      BxClear.Name = "BxClear";
      BxClear.Size = new Size(26, 26);
      BxClear.TabIndex = 1;
      BxClear.Text = "";
      BxClear.Dock = DockStyle.Right;
      
      BxClear.Click += EventDeleteFile;

      void EventDeleteFile(object sender, EventArgs e)
      {
        BxClear.Click -= EventDeleteFile;
        BxClear.Visible = false;
        EventFileDelete(sender, e);
      }
    }

    public void ClearIdObject()
    {
      DeleteAllFiles();
      SetIdObject(string.Empty, string.Empty);
    }

    public void SetIdObject(string idObject, string nameObject)
    {
      CardData.IdObject = idObject;
      CardData.NameObject = nameObject;
      CheckIdObject();
    }

    public void CheckIdObject()
    {
      bool IdObjectIsSet = CardData.IdObjectIsSet();
      PnGridBottom.Text = CardData.GetTextForSelectedObject();
      BxUpload.Enabled = IdObjectIsSet;
      BxFileAdd.Enabled = IdObjectIsSet;
      //BxFileDelete.Enabled = IdObjectIsSet;
      //BxFileSaveToDisk.Enabled = IdObjectIsSet;
      GxCard.Visible = IdObjectIsSet;
    }

    public BindingList<CardProperty> GetDataForUpload()
    {
      return CardData.DsList;
    }
  }
}
