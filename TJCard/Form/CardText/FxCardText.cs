﻿using System;
using System.Drawing;
using System.Diagnostics;
using Telerik.WinControls;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using System.Threading.Tasks;
using Telerik.WinControls.Data;
using System.Collections.Generic;

namespace TJCard
{
  public partial class FxCardText : RadForm, ICardForm
  {
    public string CurrentIdRow { get; private set; } = string.Empty;

    public string CurrentIdParent { get; private set; } = string.Empty;

    public CardProperty CurrentProperty { get; private set; } = null;

    public GridViewDataColumn CnName { get; set; } = null;

    public GridViewDataColumn CnValue { get; set; } = null;

    public DataOperatorCard CardData { get; private set; } = null;

    public RadGridView GxClassificator { get; private set; } = null;

    public string EntityName { get; internal set; } = string.Empty;

    public string GetIdObject { get => CardData.IdObject; }

    public Dictionary<CardProperty, RadPageViewPage> DcPage { get; } = new Dictionary<CardProperty, RadPageViewPage>();

    public string SpaceMargin = "  ";

    public string IdPropertyNew { get; private set; } = string.Empty;

    public string NamePropertyNew { get; private set; } = string.Empty;

    public FxCardText()
    {
      InitializeComponent();
    }

    public void Configure()
    {
      SetProperties();
      SetGridProperties();
      SetEvents();
      CheckIdObject();
    }

    public void SetProperties()
    {
      foreach (var page in PvMain.Pages) page.Item.Visibility = ElementVisibility.Collapsed;
      BxDownload.Tag = this;
      BxUpload.Tag = this;
      BxFileAdd.Tag = this;
      BxFileDelete.Tag = this;
    }

    public void AdjustFormWidth()
    {
      PnGrid.Width = (this.Parent.Width * 59) / 100;
      CnName.Width = (PnGrid.Width * 40) / 100;
      CnValue.Width = (PnGrid.Width * 59) / 100;
    }

    public void SetGridProperties()
    {
      CardData = new DataOperatorCard();
      GxCard.ShowColumnHeaders = true;
      GxCard.TableElement.TableHeaderHeight = 25;
      GxCard.TableElement.RowHeight = 26;
      GxCard.ShowRowHeaderColumn = false;
      CnName = GxCard.Columns["CnName"];
      CnValue = GxCard.Columns["CnValue"];

      foreach (var column in GxCard.Columns) column.HeaderText = string.Empty;
      AdjustFormWidth();
      GxCard.Relations.AddSelfReference(this.GxCard.MasterTemplate, "CnIdRow", "CnIdParent");

      GxCard.DataSource = CardData.DsList;
      GxCard.MasterTemplate.ExpandAll();
      GxCard.GridNavigator.ClearSelection();
      GxCard.CurrentRow = null;
      GxCard.EnableSorting = true;

      SortDescriptor sortIdProperty = new SortDescriptor();
      sortIdProperty.PropertyName = "CnIdProperty";
      sortIdProperty.Direction = ListSortDirection.Ascending;
      SortDescriptor sortIdRow = new SortDescriptor();
      sortIdRow.PropertyName = "CnIdRow";
      sortIdRow.Direction = ListSortDirection.Ascending;
      this.GxCard.SortDescriptors.Add(sortIdProperty);
      this.GxCard.SortDescriptors.Add(sortIdRow);

      GxCard.SortChanging += EventGridSortChanging;
    }

    private void EventGridSortChanging(object sender, GridViewCollectionChangingEventArgs e)
    {
      e.Cancel = true;
    }

    public void SetEvents()
    {
      BxFileDelete.Click += EventOneRowDelete;
      BxFileAdd.Click += EventAddNewTextProperty;
      GxCard.SelectionChanged += EventSelectionChanged;
    }

    public void EventUserSelectedOneRow() // Most important event that calls many other events //
    {
      CurrentIdRow = string.Empty; CurrentIdParent = CurrentIdRow; CurrentProperty = null;
      try
      {
        GridViewRowInfo row = GxCard.SelectedRows[0];
        if (row != null)
        {
          CurrentProperty = row.DataBoundItem as CardProperty;
          CurrentIdRow = CurrentProperty.IdRow;
          CurrentIdParent = CurrentProperty.IdParent ?? CurrentIdRow;
        }
      }
      catch { }

      if (CurrentProperty != null)
      {
        SetDeleteButtonState();
        SetAddButtonState();
        SelectPage();
      }
    }

    public void EventSelectionChanged(object sender, EventArgs e)
    {
      EventUserSelectedOneRow();
    }

    public void DeleteAllDataRows() 
    {
      for (int i = CardData.DsList.Count - 1; i > -1; i--)
        if (CardData.DsList[i].IdParent != null)
        {
          EventOneRowDelete(CardData.DsList[i].IdRow);
        }
    }

    public void DeletePageFromDictionary(string IdRow)
    {
      CardProperty property = null;

      foreach (var item in DcPage)
        if (item.Key.IdRow == IdRow) { property = item.Key; break; }

      if (property == null) return;

      RadPageViewPage page = null;
      if (DcPage.TryGetValue(property, out RadPageViewPage PageFound)) page = PageFound;

      if (page == null) return;

      page.IsContentVisible = false;
      DcPage.Remove(property);
      PvMain.Pages.Remove(page);
      foreach (Control control in page.Controls) control.Dispose();
      page.Controls.Clear();
      page.Dispose();
    }

    public void TryToSelectRow(string IdRow)
    {
      foreach (var row in GxCard.Rows)
        if (row.Cells[0].Value.ToString() == IdRow)
        { GxCard.CurrentRow = row; break; }
    }

    public void EventOneRowDelete(string IdRow)
    {
      bool deleted = CardData.DeleteProperty(IdRow);
      if (deleted) DeletePageFromDictionary(IdRow);
    }

    public async void EventOneRowDelete(object sender, EventArgs e)
    {
      BxFileDelete.Enabled = false;
      string IdRow = CurrentIdRow;
      string IdParent = CurrentIdParent;
      EventOneRowDelete(IdRow);
      TryToSelectRow(IdParent);
      await Task.Delay(100);
      SetDeleteButtonState();
    }

    public void SetDeleteButtonState()
    {
      BxFileDelete.Enabled = RowCanBeDeleted() && CardData.IdObjectIsSet();
    }

    public void SetAddButtonState()
    {
      BxFileAdd.Enabled = ( !RowCanBeDeleted() ) && CardData.IdObjectIsSet() ;
    }

    public bool RowCanBeDeleted() => (CurrentProperty != null) && (CurrentProperty.IdParent != null) && (CurrentProperty.ReadOnly == false);

    public RadPageViewPage GetPage(CardProperty property)
    {
      RadPageViewPage result = null;
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page)) result = page;
      return result;
    }

    public void CreateNewPage()
    {
      RadPageViewPage page = new RadPageViewPage(CurrentIdRow);
      DcPage.Add(CurrentProperty, page);
      PvMain.Pages.Add(page);
      PvMain.SelectedPage = page;

      if (CurrentProperty.IdParent==null)
        AddControlForGroupOfTypedText(page);
      else
        AddControlForText(page);

      page.Item.Visibility = ElementVisibility.Collapsed;
    }

    public void TrySelectPage()
    {
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page))
      {
        PvMain.SelectedPage = page;
      }
    }

    public void SelectPage()
    {
      if (DcPage.ContainsKey(CurrentProperty))
      {
        TrySelectPage();
      }
      else
      {
        CreateNewPage();
      }
    }

    public void ShowChangedText(string value, bool RefreshGridRow = false)
    {
      CurrentProperty.DisplayValue = value;
      if (RefreshGridRow) GxCard.CurrentRow.InvalidateRow();
    }

    public Tuple<RadPanel, RadButton> CreateTopPanel(RadPageViewPage page, bool CreateClearButton)
    {
      RadPanel PnTop = new RadPanel();
      RadButton BxClear = CreateClearButton ? new RadButton() : null;

      page.Controls.Add(PnTop);

      PnTop.Dock = DockStyle.Top;
      PnTop.Font = Standard.MainFont;
      PnTop.Location = new Point(0, 0);
      PnTop.Name = "PnTop";
      PnTop.Size = new Size(431, 30);
      PnTop.TabIndex = 2;
      PnTop.Padding = new Padding(3, 3, 3, 3);

      if (BxClear != null)
      {
        BxClear.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
        BxClear.DisplayStyle = DisplayStyle.Image;
        BxClear.Image = PictureDepot.GetImageClear();
        BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
        BxClear.Location = new Point(400, 6);
        BxClear.Name = "BxClear";
        BxClear.Size = new Size(26, 24);
        BxClear.TabIndex = 0;
        BxClear.Text = "";
        BxClear.Dock = DockStyle.Right;
        BxClear.TabStop = false;
        PnTop.Controls.Add(BxClear);
      }

      PnTop.Text = SpaceMargin + CurrentProperty.PropertyName;

      return new Tuple<RadPanel, RadButton>(PnTop, BxClear);
    }

    public RadPanel CreateBottomPanel(RadPageViewPage page)
    {
      var PnBottom = new RadPanel();
      page.Controls.Add(PnBottom);

      PnBottom.Dock = DockStyle.Bottom;
      PnBottom.Font = Standard.MainFont;
      PnBottom.Location = new Point(0, 415);
      PnBottom.Name = "PnBottom";
      PnBottom.Size = new Size(431, 30);
      PnBottom.TabIndex = 2;

      return PnBottom;
    }

    public void MakeSureNewRowIsExpanded()
    {
      bool flag1 = false; bool flag2 = false;
      foreach (var row in GxCard.Rows)
      {
        if (row.Cells[0].Value.ToString() == CurrentIdParent.ToString())
        {
          row.IsExpanded = true; flag1 = true; if (flag1 && flag2) break;
        }
        if (row.Cells[0].Value.ToString() == CurrentIdRow.ToString())
        {
          row.EnsureVisible(); flag2 = true; if (flag1 && flag2) break;
        }
      }
    }

    public void AddControlForText(RadPageViewPage page)
    {
      var TxText = new RadTextBoxControl();
      page.Controls.Add(TxText);
      page.Tag = TxText;
      // Порядок создания элементов имеет значение //
      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);
      //-------------------------------------------------------------------------------------------------------------------------
      TxText.Dock = DockStyle.Fill;
      TxText.Font = Standard.MainFont;
      TxText.Location = new Point(0, 35);
      TxText.MaxLength = 100000;
      TxText.Multiline = true;
      TxText.Name = "TJCardTxText";
      TxText.ShowClearButton = false;
      TxText.Size = new Size(431, 380);
      TxText.TabIndex = 0;

      BottomPanel.Visible = false;

      if (string.IsNullOrWhiteSpace(CurrentProperty.DisplayValue) == false) TxText.Text = CurrentProperty.DisplayValue;

      //-------------------------------------------------------------------------------------------------------------------------
      TxText.TextChanged += EventTextChanged;
      TxText.TextChanging += EventTextChanging;
      TxText.KeyUp += EventKeyUp;
      TopPanelAndClearButton.Item2.Click += EventClearText;
      //-------------------------------------------------------------------------------------------------------------------------
      void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(TxText.Text, true);
      void EventTextChanging(object sender, TextChangingEventArgs e) => ShowChangedText(TxText.Text, true);
      void EventTextChanged(object sender, EventArgs e) => ShowChangedText(TxText.Text);
      void EventClearText(object sender, EventArgs e)
      {
        if (TxText.Text.Length == 0) return;
        TxText.Clear();
        ShowChangedText(TxText.Text, true);
      }
    }

    public void AddControlForGroupOfTypedText(RadPageViewPage page)
    {
      if ((CardData.DsText == null) || (CardData.DsText.Count < 1)) return; 

      RadGridView grid = new RadGridView();
      page.Tag = grid;
      page.Controls.Add(grid);
    
      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);
      RadPanel PnTop = TopPanelAndClearButton.Item1;

      RadButton BxAdd = new RadButton();

      BottomPanel.Visible = false;
      PnTop.Visible = false;

      BxAdd.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
      BxAdd.DisplayStyle = DisplayStyle.ImageAndText;
      BxAdd.Image = PictureDepot.GetImageAdd();
      BxAdd.ImageAlignment = ContentAlignment.MiddleCenter;
      BxAdd.Location = new Point(6, 6);
      BxAdd.Name = "BxAdd";
      BxAdd.Size = new Size(26, 24);
      BxAdd.TabIndex = 0;
      BxAdd.Text = "";
      BxAdd.Dock = DockStyle.Left;
      BxAdd.Font = new Font("Verdana", 9F);

      TopPanelAndClearButton.Item2.Visible = false;
      TopPanelAndClearButton.Item2.Dispose();
      PnTop.Text = string.Empty;
      PnTop.Controls.Add(BxAdd);

      GridViewTextBoxColumn gridViewTextBoxColumn1 = new GridViewTextBoxColumn();
      GridViewTextBoxColumn gridViewTextBoxColumn2 = new GridViewTextBoxColumn();
      TableViewDefinition tableViewDefinition1 = new TableViewDefinition();

      grid.AutoScroll = true;
      grid.Dock = DockStyle.Fill;
      grid.EnableAnalytics = false;
      grid.EnableGestures = false;
      grid.Font = Standard.MainFont;
      grid.Location = new Point(0, 35);

      grid.MasterTemplate.AllowAddNewRow = false;
      grid.MasterTemplate.AllowCellContextMenu = false;
      grid.MasterTemplate.AllowColumnChooser = false;
      grid.MasterTemplate.AllowColumnHeaderContextMenu = false;
      grid.MasterTemplate.AllowColumnReorder = false;
      grid.MasterTemplate.AllowDeleteRow = false;
      grid.MasterTemplate.AllowDragToGroup = false;
      grid.MasterTemplate.AllowEditRow = false;
      grid.MasterTemplate.AllowRowHeaderContextMenu = false;
      grid.MasterTemplate.AllowRowReorder = true;
      grid.MasterTemplate.AllowRowResize = false;
      grid.MasterTemplate.AllowSearchRow = true;
      grid.MasterTemplate.AutoGenerateColumns = false;
      grid.MasterTemplate.ClipboardPasteMode = GridViewClipboardPasteMode.EnableWithNotifications;
      grid.MasterTemplate.ShowColumnHeaders = true;
      grid.MasterTemplate.ShowFilteringRow = true;
      grid.MasterTemplate.EnableFiltering = true;

      gridViewTextBoxColumn1.AllowHide = false;
      gridViewTextBoxColumn1.HeaderText = string.Empty;
      gridViewTextBoxColumn1.MaxLength = 500;
      gridViewTextBoxColumn1.Name = "CnIdObject";
      gridViewTextBoxColumn1.Width = 120;
      gridViewTextBoxColumn1.FieldName = nameof(HierarchicalClassifier.IdObject); // "Item1"; // Tuple Item1

      gridViewTextBoxColumn2.HeaderText = string.Empty; // CurrentProperty.PropertyName;
      gridViewTextBoxColumn2.MaxLength = 4000;
      gridViewTextBoxColumn2.Name = "CnNameObject";
      gridViewTextBoxColumn2.ReadOnly = true;
      gridViewTextBoxColumn2.Width = 600;
      gridViewTextBoxColumn2.FieldName = nameof(HierarchicalClassifier.NameObject); // "Item2"; // Tuple Item2

      grid.MasterTemplate.Columns.AddRange(new GridViewDataColumn[] { gridViewTextBoxColumn1, gridViewTextBoxColumn2 });
      grid.MasterTemplate.EnableGrouping = false;
    
      grid.MasterTemplate.ViewDefinition = tableViewDefinition1;
      grid.Name = "GxText";
      grid.ReadOnly = false;
      grid.ShowGroupPanel = false;
      grid.ShowGroupPanelScrollbars = false;
      grid.ShowNoDataText = false;
      grid.Size = new Size(465, 407);
      grid.TabIndex = 3;

      grid.MasterView.TableSearchRow.ShowCloseButton = false;
      grid.MasterView.TableSearchRow.ShowClearButton = true;

      grid.DataSource = CardData.DsText;
      grid.ZzGridClearSelection();

      grid.MasterView.TableSearchRow.AutomaticallySelectFirstResult = true;

      GxClassificator = grid;
      GxClassificator.Visible = false;

      grid.TableElement.TableHeaderHeight = 20;

      grid.CurrentRowChanged += EventCurrentRowChangedLocalMethod;
      BxAdd.Click += EventAddNewTextProperty;
   
      void EventCurrentRowChangedLocalMethod(object sender, CurrentRowChangedEventArgs e)
      {
        if (e.CurrentRow is GridViewDataRowInfo)
        {
          GridViewDataRowInfo row = e.CurrentRow as GridViewDataRowInfo;
          IdPropertyNew = row.Cells[0].Value.ToString();
          NamePropertyNew = row.Cells[1].Value.ToString();
        }
      }
    }

    public void EventAddNewTextProperty(object sender, EventArgs e) => AddTypedTextProperty(IdPropertyNew, null, NamePropertyNew, string.Empty);

    public void AddTypedTextProperty(string idProperty, string idRow, string nameProperty, string value)
    {
      if (string.IsNullOrWhiteSpace(idProperty)) return;
      if (string.IsNullOrWhiteSpace(nameProperty)) nameProperty = CardData.GetTextValueOfClassificator(idProperty);
      CardData.AddTypedTextProperty(idProperty, idRow, nameProperty.Trim(), value, false);

      MakeSureNewRowIsExpanded();
      if (GxClassificator.FilterDescriptors.Count > 0) GxClassificator.FilterDescriptors.Clear();     
    }

    public void ClearIdObject()
    {
      DeleteAllDataRows();
      SetIdObject(string.Empty, string.Empty);
    }

    public void SetIdObject(string idObject, string nameObject)
    {
      CardData.IdObject = idObject;
      CardData.NameObject = nameObject;
      CheckIdObject();
    }

    public void CheckIdObject()
    {
      bool IdObjectIsSet = CardData.IdObjectIsSet();
      PnGridBottom.Text = CardData.GetTextForSelectedObject();
      BxUpload.Enabled = IdObjectIsSet;
      BxFileAdd.Enabled = IdObjectIsSet;
      BxFileDelete.Enabled = IdObjectIsSet;         
      GxCard.Visible = IdObjectIsSet;
      if (GxClassificator != null) GxClassificator.Visible = IdObjectIsSet;
    }

    public void ExpandAllRows() => GxCard.MasterTemplate.ExpandAll();

    public BindingList<CardProperty> GetDataForUpload()
    {
      return CardData.DsList;
    }
  }
}
