﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJCard.Tools;

namespace TJCard
{
  public partial class FxStart : RadForm
  {
    ServiceCardForm SrvCardForm { get; } = ServiceCardForm.Create();

    FxCardMain Card { get; set; } = null;

    Random gen { get; set; } = new Random();

    public FxStart()
    {
      InitializeComponent();
      SetEvents();
    }

    private void SetEvents()
    {
      BxExit.Click += (s, e) => this.Close();
      BxCardFormInsert.Click += (s, e) => EventCardFormCreate(true);
      BxCardFormUpdate.Click += (s, e) => EventCardFormCreate(false);    
    }

    public void Print(string message)
    {
      TxMessage.AppendText(message + Environment.NewLine + Environment.NewLine);
    }

    public void Write(string message)
    {
      TxMessage.AppendText(message + " ");
    }

    public async Task Process()
    {
      for(int i = 1; i < 11; i++)
      {
        TxMessage.AppendText(i.ToString() + ", "); await Task.Delay(50);
      }
    }

    public void RemoveControls()
    {
      for (int i = PnMain.Controls.Count-1; i >= 0; i--)
      {
        var control = PnMain.Controls[i];
        control.Visible = false;
        PnMain.Controls.RemoveAt(i);
        control.Dispose();
      }
    }

    public void FillData(FxCardMain Form) 
    {
      DataOperatorCard dt = Form.CardData;

      if (dt.DsList.Count > 0) dt.DsList.Clear();

      BindingList<Tuple<string, string>> DsExample1 = new BindingList<Tuple<string, string>>();
      BindingList<Tuple<string, string>> DsExample2 = new BindingList<Tuple<string, string>>();
      TestData test = new TestData();
      test.FillTestData1(DsExample1);
      test.FillTestData2(DsExample2);

      //AddGroup("1000", "Числовые свойства");
      dt.Add(PropertyType.Int32, null, nameof(TestEntity.IdPerson), null, "Идентификатор Сотрудника");
      dt.Add(PropertyType.Int64, null, nameof(TestEntity.IdUniqueKey), null, "Идентификатор Экземпляра");
      dt.Add(PropertyType.Decimal, null, nameof(TestEntity.MyMoney), null, "Цена товара");

      //AddGroup("2000", "Текстовые свойства");
      dt.Add(PropertyType.Text, null, nameof(TestEntity.ItemName), null, "Название предмета");
      dt.Add(PropertyType.Text, null, nameof(TestEntity.InventoryNumber), null, "Инвентарный номер");
      dt.Add(PropertyType.Text, null, nameof(TestEntity.ArticleCode), null, "Артикул");
      dt.Add(PropertyType.Text, null, nameof(TestEntity.SerialNumber), null, "Серийный номер");

      //AddGroup("3000", "Дата/время");
      dt.Add(PropertyType.Datetime, null, nameof(TestEntity.StartDate), null, "Дата начала");
      dt.Add(PropertyType.Datetime, null, nameof(TestEntity.EndDate), null, "Дата окончания", DataOperatorCard.FlagDateOnly);

      //AddGroup("4000", "Password");
      dt.Add(PropertyType.Password, null, nameof(TestEntity.MyPassword), null, "Пароль пользователя");

      //AddGroup("5000", "Logic switcher");
      dt.Add(PropertyType.Boolean, null, nameof(TestEntity.Gto), null, "Сдал нормы ГТО");
      dt.Add(PropertyType.Boolean, null, nameof(TestEntity.GtaVcGame), null, "Прошёл GTA VC");

      //AddGroup("9000", "Ссылка на справочник", PropertyType.Classificator);
      //dt.AddClassificator(DsExample1, null, nameof(TestEntity.IdMeasure), null, "Единица измерения");
      //dt.AddClassificator(DsExample2, null, nameof(TestEntity.IdSupplier), null, "Изготовитель продукции");

      //AddGroup("11000", "Barcode and RFID", PropertyType.Unknown);
      dt.Add(PropertyType.Barcode, null, nameof(TestEntity.Barcode), null, "Barcode");
      dt.Add(PropertyType.RfidCode, null, nameof(TestEntity.Rfid), null, "RFID");
      
      /*
      BindingList<Tuple<string, string>> DsTxtProperties = new BindingList<Tuple<string, string>>();

      DsTxtProperties.Add(new Tuple<string, string>("234", "Почтовый адрес"));
      DsTxtProperties.Add(new Tuple<string, string>("345", "Номер партийного билета"));
      DsTxtProperties.Add(new Tuple<string, string>("456", "Водительское удостоверение"));
      DsTxtProperties.Add(new Tuple<string, string>("567", "Адрес сайта"));
      */
      //dt.AddGroupTextProperties(DsTxtProperties, "Текстовые свойства");
      //dt.AddGroupImages("Изображение");
      //dt.AddGroupFiles("Документ");
    }

    private void EventCardFormCreate(bool InsertMode)
    {
      RemoveControls();
      Card = SrvCardForm.AddCardForm(PnMain, FillData, InsertMode, TsDebug.Value);
      if (InsertMode == false)  Card.BxDownload.Click += EventDownload;
      Card.BxUpload.Click += EventUpload;
    }

    DateTime RandomDay()
    {
      DateTime start = new DateTime(2015, 1, 1,15,59,21);
      int range = (DateTime.Today - start).Days;
      return start.AddDays(gen.Next(range));
    }

    public async Task<TestEntity> GetTestEntity()
    {
      await Task.Delay(1000);
      TestEntity test = new TestEntity();
      test.IdPerson = 90023015;
      test.ArticleCode = "Test Article Code - 7770-9992-GHJE-0001";
      test.Barcode = KeyGenerator.GetUniqueKey(25).ToUpper(); // "738947583458934897538483760934859359";
      test.Rfid = KeyGenerator.GetSecondsBase36() + "-" + KeyGenerator.GetUniqueKey(20).ToUpper();
      test.StartDate = RandomDay();
      test.EndDate = RandomDay();
      test.GtaVcGame = true;
      test.Gto = false;
      test.IdMeasure = 11160241;
      test.IdSupplier = 221513;
      test.IdUniqueKey = 9111000888000333001;
      test.InventoryNumber = "Test INV Number - " + KeyGenerator.GetUniqueKey(8).ToUpper();
      test.ItemName = "Название предмета тестирование";
      test.MyMoney = 100203.95M;
      test.MyPassword = "MySyperSecr3tP@55w0rd";
      test.SerialNumber = "SERIAL NUMBER " + KeyGenerator.GetUniqueKey(4).ToUpper() + "-" + KeyGenerator.GetUniqueKey(4).ToUpper();
      return test;
    }

    public async void EventUpload(object sender, EventArgs e)
    {
      Card.BxUpload.Enabled = false;
      TestEntity test = await GetTestEntity();
      await EventUpload();
      await Task.Delay(500);
      Card.BxUpload.Enabled = true;
    }

    public async Task EventUpload() // NOTE: UPLOAD EXAMPLE //
    {
      TestEntity test = new TestEntity();
      DataOperatorCard dt = Card.CardData;

      test.ArticleCode = dt.GetValueText(nameof(TestEntity.ArticleCode));
      test.Barcode = dt.GetValueText(nameof(TestEntity.Barcode));
      test.Rfid = dt.GetValueText(nameof(TestEntity.Rfid));
      test.InventoryNumber = dt.GetValueText(nameof(TestEntity.InventoryNumber));
      test.ItemName = dt.GetValueText(nameof(TestEntity.ItemName));
      test.SerialNumber = dt.GetValueText(nameof(TestEntity.SerialNumber));
      test.MyPassword = dt.GetValuePassword(nameof(TestEntity.MyPassword));

      test.GtaVcGame = dt.GetValueBoolean(nameof(TestEntity.GtaVcGame));
      test.Gto = dt.GetValueBoolean(nameof(TestEntity.Gto));

      test.IdPerson = dt.GetValueInt32(nameof(TestEntity.IdPerson));
      test.IdUniqueKey = dt.GetValueInt64(nameof(TestEntity.IdUniqueKey));
      test.MyMoney = dt.GetValueDecimal(nameof(TestEntity.MyMoney));

      test.StartDate = dt.GetValueDatetime(nameof(TestEntity.StartDate));
      test.EndDate = dt.GetValueDatetime(nameof(TestEntity.EndDate));

      test.IdMeasure = dt.GetValueClassificatorAsInt32(nameof(TestEntity.IdMeasure));
      test.IdSupplier = dt.GetValueClassificatorAsInt32(nameof(TestEntity.IdSupplier));

      string json = CxConvert.ObjectToJson(test);
      Card.ClearAllValues();
      TxMessage.Clear();

      await Task.Delay(1000);

      Print(json);      
    }

    public async void EventDownload(object sender, EventArgs e)
    {
      Card.BxDownload.Enabled = false;
      TestEntity test = await GetTestEntity();
      await EventDownload(test);
      await Task.Delay(1500);
      Card.BxDownload.Enabled = true;
    }

    public async Task EventDownload(TestEntity test) // NOTE: DOWNLOAD EXAMPLE //
    {
      Card.ClearAllValues();

      Card.UpdateInt32(nameof(TestEntity.IdPerson), test.IdPerson);
      Card.UpdateInt64(nameof(TestEntity.IdUniqueKey), test.IdUniqueKey);
      Card.UpdateDecimal(nameof(TestEntity.MyMoney), test.MyMoney);
      Card.UpdateBoolean(nameof(TestEntity.GtaVcGame), test.GtaVcGame);
      Card.UpdateBoolean(nameof(TestEntity.Gto), test.Gto);
      Card.UpdatePassword(nameof(TestEntity.MyPassword), test.MyPassword);
      Card.UpdateBarcode(nameof(TestEntity.Barcode), test.Barcode);
      Card.UpdateRfid(nameof(TestEntity.Rfid), test.Rfid);
      Card.UpdateText(nameof(TestEntity.ArticleCode), test.ArticleCode);
      Card.UpdateText(nameof(TestEntity.InventoryNumber), test.InventoryNumber);
      Card.UpdateText(nameof(TestEntity.ItemName), test.ItemName);
      Card.UpdateText(nameof(TestEntity.SerialNumber), test.SerialNumber);
      Card.UpdateDatetime(nameof(TestEntity.StartDate), test.StartDate);
      Card.UpdateDatetime(nameof(TestEntity.EndDate), test.EndDate);
      Card.UpdateClassificator(nameof(TestEntity.IdMeasure), test.IdMeasure);
      Card.UpdateClassificator(nameof(TestEntity.IdSupplier), test.IdSupplier);

      Card.RefreshAllRows();
      await Task.Delay(100);
      Card.SetIdObject("123", "Desktop Computer HP G600 Intel Core i5 RAM 16 Gb");
    }
  }
}
