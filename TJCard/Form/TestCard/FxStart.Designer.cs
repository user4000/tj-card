﻿namespace TJCard
{
    partial class FxStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.PnMain = new System.Windows.Forms.Panel();
      this.BxCardFormInsert = new Telerik.WinControls.UI.RadButton();
      this.BxExit = new Telerik.WinControls.UI.RadButton();
      this.BxCardFormUpdate = new Telerik.WinControls.UI.RadButton();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
      this.TsDebug = new Telerik.WinControls.UI.RadToggleSwitch();
      this.TxMessage = new Telerik.WinControls.UI.RadTextBoxControl();
      this.BxUpload = new Telerik.WinControls.UI.RadButton();
      this.BxDownload = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.BxCardFormInsert)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxExit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxCardFormUpdate)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
      this.radPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TsDebug)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxUpload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PnMain
      // 
      this.PnMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnMain.Location = new System.Drawing.Point(0, 321);
      this.PnMain.Name = "PnMain";
      this.PnMain.Size = new System.Drawing.Size(976, 499);
      this.PnMain.TabIndex = 0;
      // 
      // BxCardFormInsert
      // 
      this.BxCardFormInsert.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxCardFormInsert.Location = new System.Drawing.Point(12, 12);
      this.BxCardFormInsert.Name = "BxCardFormInsert";
      this.BxCardFormInsert.Size = new System.Drawing.Size(172, 40);
      this.BxCardFormInsert.TabIndex = 1;
      this.BxCardFormInsert.Text = "Форма для создания";
      // 
      // BxExit
      // 
      this.BxExit.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxExit.Location = new System.Drawing.Point(792, 12);
      this.BxExit.Name = "BxExit";
      this.BxExit.Size = new System.Drawing.Size(172, 40);
      this.BxExit.TabIndex = 1;
      this.BxExit.Text = "Exit";
      // 
      // BxCardFormUpdate
      // 
      this.BxCardFormUpdate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxCardFormUpdate.Location = new System.Drawing.Point(201, 12);
      this.BxCardFormUpdate.Name = "BxCardFormUpdate";
      this.BxCardFormUpdate.Size = new System.Drawing.Size(172, 40);
      this.BxCardFormUpdate.TabIndex = 1;
      this.BxCardFormUpdate.Text = "Форма для изменения";
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 310);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(976, 11);
      this.splitter1.TabIndex = 2;
      this.splitter1.TabStop = false;
      // 
      // radPanel1
      // 
      this.radPanel1.Controls.Add(this.TsDebug);
      this.radPanel1.Controls.Add(this.TxMessage);
      this.radPanel1.Controls.Add(this.BxCardFormInsert);
      this.radPanel1.Controls.Add(this.BxUpload);
      this.radPanel1.Controls.Add(this.BxDownload);
      this.radPanel1.Controls.Add(this.BxCardFormUpdate);
      this.radPanel1.Controls.Add(this.BxExit);
      this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.radPanel1.Location = new System.Drawing.Point(0, 0);
      this.radPanel1.Name = "radPanel1";
      this.radPanel1.Size = new System.Drawing.Size(976, 310);
      this.radPanel1.TabIndex = 3;
      // 
      // TsDebug
      // 
      this.TsDebug.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TsDebug.Location = new System.Drawing.Point(425, 12);
      this.TsDebug.Name = "TsDebug";
      this.TsDebug.OffText = "Release mode";
      this.TsDebug.OnText = "Debug mode";
      this.TsDebug.Size = new System.Drawing.Size(183, 40);
      this.TsDebug.TabIndex = 3;
      ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.TsDebug.GetChildAt(0))).ThumbOffset = 163;
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsDebug.GetChildAt(0).GetChildAt(0))).Text = "Debug mode";
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsDebug.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsDebug.GetChildAt(0).GetChildAt(1))).Text = "Release mode";
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsDebug.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      // 
      // TxMessage
      // 
      this.TxMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxMessage.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxMessage.Location = new System.Drawing.Point(12, 74);
      this.TxMessage.MaxLength = 100000;
      this.TxMessage.Multiline = true;
      this.TxMessage.Name = "TxMessage";
      this.TxMessage.ShowClearButton = true;
      this.TxMessage.Size = new System.Drawing.Size(952, 119);
      this.TxMessage.TabIndex = 2;
      // 
      // BxUpload
      // 
      this.BxUpload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxUpload.Location = new System.Drawing.Point(12, 234);
      this.BxUpload.Name = "BxUpload";
      this.BxUpload.Size = new System.Drawing.Size(361, 29);
      this.BxUpload.TabIndex = 1;
      this.BxUpload.Text = "Записать данные на сервер";
      // 
      // BxDownload
      // 
      this.BxDownload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDownload.Location = new System.Drawing.Point(12, 199);
      this.BxDownload.Name = "BxDownload";
      this.BxDownload.Size = new System.Drawing.Size(361, 29);
      this.BxDownload.TabIndex = 1;
      this.BxDownload.Text = "Получить данные сервера";
      // 
      // FxStart
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(976, 820);
      this.Controls.Add(this.radPanel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.PnMain);
      this.Name = "FxStart";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "FxStart";
      ((System.ComponentModel.ISupportInitialize)(this.BxCardFormInsert)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxExit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxCardFormUpdate)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
      this.radPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TsDebug)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxUpload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Panel PnMain;
    public Telerik.WinControls.UI.RadButton BxCardFormInsert;
    public Telerik.WinControls.UI.RadButton BxExit;
    public Telerik.WinControls.UI.RadButton BxCardFormUpdate;
    private System.Windows.Forms.Splitter splitter1;
    private Telerik.WinControls.UI.RadPanel radPanel1;
    public Telerik.WinControls.UI.RadTextBoxControl TxMessage;
    public Telerik.WinControls.UI.RadButton BxUpload;
    public Telerik.WinControls.UI.RadButton BxDownload;
    public Telerik.WinControls.UI.RadToggleSwitch TsDebug;
  }
}
