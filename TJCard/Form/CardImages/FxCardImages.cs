﻿using System;
using System.IO;
using System.Linq;
using TJCard.Tools;
using System.Drawing;
using System.Diagnostics;
using Telerik.WinControls;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TJCard
{
  public partial class FxCardImages : RadForm, ICardForm
  {
    public string CurrentIdRow { get; private set; } = string.Empty;

    public string CurrentIdParent { get; private set; } = string.Empty;

    public CardProperty CurrentProperty { get; private set; } = null;

    public string CurrentFileName { get; private set; } = string.Empty;

    public string EntityName { get; internal set; } = string.Empty;

    public string GetIdObject { get => CardData.IdObject; }

    public string DirectoryToSaveFile { get; private set; } = string.Empty;

    public GridViewDataColumn CnName { get; private set; } = null;

    public GridViewDataColumn CnValue { get; set; } = null;

    public DataOperatorCard CardData { get; private set; } = null;

    public Dictionary<CardProperty, RadPageViewPage> DcPage { get; } = new Dictionary<CardProperty, RadPageViewPage>();

    public int MaxFileSize { get; set; } = 10000000;

    public string StrFileNotSelected { get; set; } = "File is not selected";

    public string StrSelectFile { get; set; } = "You have to select a file";

    public string StrOverwriteFile { get; set; } = "Do you want to overwrite the file?";

    public string StrFileExists { get; set; } = "File already exists";

    public string StrNotImage { get; set; } = "The file you specified is not an image";

    public string StrError { get; set; } = "Error!";

    public FxCardImages()
    {
      InitializeComponent();
    }

    public void Configure()
    {
      SetProperties();
      SetGridProperties();
      SetEvents();
      CheckIdObject();
    }

    public void SetProperties()
    {
      BxFileDelete.Enabled = false;
      BxFileSaveToDisk.Enabled = false;
      foreach (var page in PvMain.Pages) page.Item.Visibility = ElementVisibility.Collapsed;
      BxDownload.Tag = this;
      BxUpload.Tag = this;
      BxFileAdd.Tag = this;
      BxFileDelete.Tag = this;
    }

    public void AdjustFormWidth()
    {
      PnGrid.Width = (this.Parent.Width * 59) / 100;
      CnName.Width = (PnGrid.Width * 62) / 100;
      CnValue.Width = (PnGrid.Width * 37) / 100;
    }

    public void SetGridProperties()
    {
      CardData = new DataOperatorCard();
      GxCard.ShowColumnHeaders = true;
      GxCard.TableElement.TableHeaderHeight = 25;
      GxCard.TableElement.RowHeight = 30;
      GxCard.ShowRowHeaderColumn = false;
      CnName = GxCard.Columns["CnName"];
      CnValue = GxCard.Columns["CnValue"];

      foreach (var column in GxCard.Columns) column.HeaderText = string.Empty;
      AdjustFormWidth();
      //GxCard.Relations.AddSelfReference(this.GxCard.MasterTemplate, "CnIdRow", "CnIdParent");

      GxCard.DataSource = CardData.DsList;
      GxCard.MasterTemplate.ExpandAll();
      GxCard.ZzGridClearSelection();
    }

    public void SetEvents()
    {
      BxFileAdd.Click += EventFileOpen;
      BxFileDelete.Click += EventFileDelete;
      BxFileSaveToDisk.Click += EventFileSaveToDisk;
      GxCard.SelectionChanged += EventSelectionChanged;
    }

    public Bitmap GetBitmap(CardProperty property)
    {
      return (property?.InputField as RadImageEditor)?.CurrentBitmap;
    }

    public void EventFileSaveToDisk(object sender, EventArgs e)
    {
      //Bitmap bitmap = GetBitmap(CurrentProperty);
      //if ((CurrentProperty == null) || (bitmap==null))

      if (CurrentProperty == null)
      {
        RadMessageBox.Show(StrFileNotSelected, StrSelectFile, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        return;
      }

      FolderBrowserDialog folder = new FolderBrowserDialog();
      if (DirectoryToSaveFile.Length > 0)
        try { folder.SelectedPath = DirectoryToSaveFile; } catch { }

      DialogResult result = folder.ShowDialog();

      if (result == DialogResult.OK)
      {
        DirectoryToSaveFile = folder.SelectedPath;
        string FileName = Path.Combine(folder.SelectedPath, CurrentProperty.PropertyName);

        if (File.Exists(FileName))
        {
          string question =
            Path.GetDirectoryName(FileName) + "\r\n" +
            Path.GetFileName(FileName) + "\r\n" +
            StrOverwriteFile;

          DialogResult overwrite = RadMessageBox.Show(question, StrFileExists, MessageBoxButtons.YesNo, RadMessageIcon.Question);
          if (overwrite == DialogResult.No) return;
        }

        CardData.SaveBase64ToDisk(FileName, CurrentProperty.StringValue);
        //SaveBitmapToDisk(FileName, bitmap);
      }
    }

    public void EventUserSelectedOneRow() // Most important event that calls many other events //
    {
      CurrentIdRow = string.Empty; CurrentIdParent = CurrentIdRow; CurrentProperty = null;
      try
      {
        GridViewRowInfo row = GxCard.SelectedRows[0];
        if (row != null)
        {
          CurrentProperty = row.DataBoundItem as CardProperty;
          CurrentIdRow = CurrentProperty.IdRow;
          CurrentIdParent = CurrentProperty.IdParent ?? CurrentIdRow;
        }
      }
      catch { }

      if (CurrentProperty != null)
      {
        SetButtonsState();
        SelectPage();
      }
    }

    public void EventSelectionChanged(object sender, EventArgs e)
    {
      EventUserSelectedOneRow();
    }

    public void EventFileOpen(object sender, EventArgs e)
    {
      OpenFileDialog dialog = new OpenFileDialog();
      DialogResult result = dialog.ShowDialog();
      if (CardData.FileIsTooBig(result, dialog.FileName, MaxFileSize)) return;

      if (result == DialogResult.OK)
      {
        CurrentFileName = dialog.FileName;
        EventFileAdd(CurrentFileName);
      }
    }

    public void DeletePageFromDictionary(string IdRow)
    {
      CardProperty property = null;

      foreach (var item in DcPage)
        if (item.Key.IdRow == IdRow) { property = item.Key; break; }

      if (property == null) return;

      RadPageViewPage page = null;
      if (DcPage.TryGetValue(property, out RadPageViewPage PageFound)) page = PageFound;

      if (page == null) return;

      page.IsContentVisible = false;
      ClearImageAndDeleteImageEditor(page);
      DcPage.Remove(property);
      PvMain.Pages.Remove(page);
      foreach (Control control in page.Controls) control.Dispose();
      page.Controls.Clear();
      page.Dispose();
    }

    public void TryToSelectRow(string IdRow)
    {
      foreach (var row in GxCard.Rows)
        if (row.Cells[0].Value.ToString() == IdRow)
        { GxCard.CurrentRow = row; break; }
    }

    public void DeleteAllFiles()
    {
      while (CardData.DsList.Count > 0)
      {
        CardProperty item = CardData.DsList.Last();
        EventFileDelete(item.IdRow);
      }
    }

    public void EventFileDelete(string IdRow)
    {
      bool deleted = CardData.DeleteProperty(IdRow);
      if (deleted) DeletePageFromDictionary(IdRow);
    }

    public async void EventFileDelete(object sender, EventArgs e)
    {
      BxFileDelete.Enabled = false;
      string IdRow = CurrentIdRow;
      string IdParent = CurrentIdParent;
      EventFileDelete(IdRow);
      TryToSelectRow(IdParent);
      await Task.Delay(200);
      SetButtonsState();
    }

    public Bitmap CheckFileIsPicture(string FileName)
    {
      Bitmap result = null;
      try
      {
        ImgEditor.OpenImage(FileName);
        result = ImgEditor.CurrentBitmap;
      }
      catch
      {
        result = null;
        ImgEditor.CurrentBitmap = null;
      }
      return result;
    }

    public void EventFileAdd(string FileName)
    {
      Bitmap bitmap = CheckFileIsPicture(FileName);

      if (bitmap == null)
      {
        RadMessageBox.Show(StrNotImage, StrError, MessageBoxButtons.OK, RadMessageIcon.Error);
        return;
      }

      long FileLength = Standard.FileLength(FileName);
      string name = Path.GetFileName(FileName); ;
      string IdRow = KeyGenerator.GetNewIdRow();
      string IdProperty = Standard.HashAndSize(FileName, FileLength);
      string formattedFileLength = Standard.FormatInteger(FileLength) + " bytes";

      if (CardData.ThisFileAlreadyAdded(IdProperty, name)) return;

      string base64 = CxConvert.FileToBase64(FileName);
      CardData.AddImage(IdProperty, IdRow, name, formattedFileLength, base64, bitmap);
      base64 = string.Empty;

      if (CardData.Exists(IdRow) == false) return;

      CurrentFileName = string.Empty;
    }

    public void SetButtonsState()
    {
      bool Flag =
        (CurrentProperty != null) &&
        (CurrentProperty.ReadOnly == false) &&
        (CardData.DsList.Count > 0);
      BxFileDelete.Enabled = Flag;
      BxFileSaveToDisk.Enabled = Flag;
    }

    public RadPageViewPage GetPage(CardProperty property)
    {
      RadPageViewPage result = null;
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page)) result = page;
      return result;
    }

    public void CreateNewPage()
    {
      RadPageViewPage page = new RadPageViewPage(CurrentIdRow);
      DcPage.Add(CurrentProperty, page);
      PvMain.Pages.Add(page);
      PvMain.SelectedPage = page;
      AddControlForImage(page, CurrentProperty);
      page.Item.Visibility = ElementVisibility.Collapsed;
    }

    public void TrySelectPage()
    {
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page))
      {
        PvMain.SelectedPage = page;
      }
    }

    public void SelectPage()
    {
      if (DcPage.ContainsKey(CurrentProperty))
      {
        TrySelectPage();
      }
      else
      {
        CreateNewPage();
      }
    }

    public void ShowChangedText(string value, bool RefreshGridRow = false)
    {
      CurrentProperty.DisplayValue = value;
      if (RefreshGridRow) GxCard.CurrentRow.InvalidateRow();
    }

    public Tuple<RadPanel, RadButton> CreateTopPanel(RadPageViewPage page, bool CreateClearButton)
    {
      RadPanel PnTop = new RadPanel();
      RadButton BxClear = CreateClearButton ? new RadButton() : null;

      page.Controls.Add(PnTop);

      PnTop.Dock = DockStyle.Top;
      PnTop.Font = Standard.MainFont;
      PnTop.Location = new Point(0, 0);
      PnTop.Name = "PnTop";
      PnTop.Size = new Size(431, 30);
      PnTop.TabIndex = 2;
      PnTop.Padding = new Padding(3, 3, 3, 3);

      if (BxClear != null)
      {
        BxClear.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
        BxClear.DisplayStyle = DisplayStyle.Image;
        BxClear.Image = PictureDepot.GetImageClear();
        BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
        BxClear.Location = new Point(400, 6);
        BxClear.Name = "BxClear";
        BxClear.Size = new Size(26, 24);
        BxClear.TabIndex = 0;
        BxClear.Text = "";
        BxClear.Dock = DockStyle.Right;
        BxClear.TabStop = false;
        PnTop.Controls.Add(BxClear);
      }

      PnTop.Text = "  " + CurrentProperty.PropertyName;

      return new Tuple<RadPanel, RadButton>(PnTop, BxClear);
    }

    public RadPanel CreateBottomPanel(RadPageViewPage page)
    {
      var PnBottom = new RadPanel();
      page.Controls.Add(PnBottom);

      PnBottom.Dock = DockStyle.Bottom;
      PnBottom.Font = Standard.MainFont;
      PnBottom.Location = new Point(0, 415);
      PnBottom.Name = "PnBottom";
      PnBottom.Size = new Size(431, 30);
      PnBottom.TabIndex = 2;

      return PnBottom;
    }

    public void AddControlForImage(RadPageViewPage page, CardProperty property)
    {
      RadImageEditor EdImage = new RadImageEditor();

      ((ISupportInitialize)(EdImage)).BeginInit();
      page.Controls.Add(EdImage);
      page.Tag = EdImage;
      EdImage.AllowDrop = true;
      EdImage.Dock = DockStyle.Fill;
      EdImage.Font = Standard.MainFont;
      EdImage.Location = new Point(0, 0);
      EdImage.Name = page.Text;
      EdImage.TabIndex = 0;
      ((ISupportInitialize)(EdImage)).EndInit();
   
      // Hide [Open] button
      ((RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(0))).Enabled = false;
      ((RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(0))).Visibility = ElementVisibility.Hidden;

      ((Telerik.WinControls.UI.RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(0))).ToolTipText = string.Empty;
      ((Telerik.WinControls.UI.RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(1))).ToolTipText = string.Empty;
      ((Telerik.WinControls.UI.RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(2))).ToolTipText = string.Empty;
      ((Telerik.WinControls.UI.RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(3))).ToolTipText = string.Empty;

      ((Telerik.WinControls.UI.RadMenuHeaderItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(1))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(2))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(3))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(4))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(5))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(6))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(7))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(8))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(9))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(10))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(11))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(12))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(13))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(14))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuHeaderItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(15))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(16))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(17))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(18))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(19))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuHeaderItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(20))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(21))).Text = " ";
      ((Telerik.WinControls.UI.RadMenuItem)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(22))).Text = " ";
      ((Telerik.WinControls.UI.RadDropDownListElement)(EdImage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 10F);


      // Do not hide [Save current bitmap] button
      //((RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(1))).Enabled = false;
      //((RadButtonElement)(EdImage.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(1))).Visibility = ElementVisibility.Hidden;

      try
      {
        EdImage.OpenImage(property.ImageBitmap);
        property.InputField = EdImage;
        //property.ByteArray = CxConvert.FileToByteArray(CurrentFileName);
        //property.StringValue = CxConvert.FileToBase64(CurrentFileName);
      }
      catch
      {
        EdImage.CurrentBitmap = null;
        RadMessageBox.Show(StrNotImage, StrError, MessageBoxButtons.OK, RadMessageIcon.Error);
        CurrentProperty.DisplayValue = "error_no_image.bmp";
        CurrentProperty.PropertyName = CurrentProperty.DisplayValue;
        EdImage.BitmapPath = CurrentProperty.DisplayValue;
        EdImage.Visible = false;
        page.Controls.Remove(EdImage);
        page.Tag = null;
        EdImage.Dispose();
      }

      ImgEditor.CurrentBitmap = null;
      property.ImageBitmap = null;
    }

    public void ClearImageAndDeleteImageEditor(RadPageViewPage page)
    {
      if (page.Tag == null) return;
      RadImageEditor EdImage = page.Tag as RadImageEditor;
      if (EdImage == null) return;
      EdImage.Visible = false;
      EdImage.CurrentBitmap = null;
      EdImage.Dock = DockStyle.None;
      EdImage.Parent = null;
      EdImage.Dispose();
    }

    public void ClearIdObject()
    {
      DeleteAllFiles();
      SetIdObject(string.Empty, string.Empty);
    }

    public void SetIdObject(string idObject, string nameObject)
    {
      CardData.IdObject = idObject;
      CardData.NameObject = nameObject;
      CheckIdObject();
    }

    public void CheckIdObject()
    {
      bool IdObjectIsSet = CardData.IdObjectIsSet();
      PnGridBottom.Text = CardData.GetTextForSelectedObject();
      BxUpload.Enabled = IdObjectIsSet;
      BxFileAdd.Enabled = IdObjectIsSet;
      //BxFileDelete.Enabled = IdObjectIsSet;
      //BxFileSaveToDisk.Enabled = IdObjectIsSet;
      GxCard.Visible = IdObjectIsSet;
    }

    public BindingList<CardProperty> GetDataForUpload()
    {
      return CardData.DsList;
    }

    public void DebugSaveToFile(string FileName, string Data)
    {
      File.WriteAllText($@"D:\{FileName}", Data);
    }
  }
}

/*

      
if (EdImage != null) EdImage.ImageLoaded += (s, e) => EventEditorImageLoaded(EdImage, e);

void EventEditorImageLoaded(RadImageEditor sender, AsyncCompletedEventArgs e)
{
  CurrentProperty.DisplayValue = Path.GetFileName(sender.BitmapPath);
  CurrentProperty.PropertyName = CurrentProperty.DisplayValue;
  GxCard.CurrentRow.InvalidateRow();
}  



public BindingList<CardProperty> GetDataForUpload()
{
  
  Данная конструкция не применяется потому что все изображения в этой конструкции переводятся в BMP
  Например пользователь открыл файл 5 килобайтов в формате JPG
  После преобразования через CurrentBitmap файл станет размером 40-50 Kb и будет иметь формат BMP
  Поэтому для записи на сервер и в файл на диске используется свойство StringValue,
  в котором хранится исходный файл-картинка, закодированная в BASE 64

  Bitmap bitmap; byte[] array;
  foreach (var item in CardData.DsList)
  {
        
    bitmap = GetBitmap(item);
    if (bitmap != null)
    {
      array = CxConvert.ImageToByteArray(bitmap);
      item.StringValue = CxConvert.ByteArrayToBase64(array);
      //DebugSaveToFile(item.IdRow + ".txt", item.StringValue);
    }
        
    DebugSaveToFile(item.IdRow + ".txt", item.StringValue);
  }
  bitmap = null;
  array = null;
  
  return CardData.DsList;
}

*/