﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace TJCard
{
  public partial class FxTestCardFiles : RadForm
  {
    ServiceCardForm SrvCardForm { get; } = ServiceCardForm.Create();

    FxCardFiles Card { get; set; } = null;

    public FxTestCardFiles()
    {
      InitializeComponent();
      SetEvents();
    }

    private void SetEvents()
    {
      BxExit.Click += (s, e) => this.Close();
      BxCardFilesCreate.Click += (s, e) => EventCardFilesCreate();   
    }

    private void EventCardFilesCreate()
    {
      RemoveControls();
      Card = SrvCardForm.AddCardFilesForm(PnMain, "test");
      Card.BxFileAdd.Click += EventFileAdd;
      Card.BxDownload.Click += EventDownload;
      Card.BxUpload.Click += (s, e) =>
      {
        Card.DeleteAllFiles();
      };
      this.ZzSetMessageBoxFont();
    }

    private void EventFileAdd(object sender, EventArgs e)
    {
      //string json = CxConvert.ObjectToJson(Card.CardData.DsList);
      //TxMessage.Text = json;
    }

    public void Print(string message)
    {
      TxMessage.AppendText(message + Environment.NewLine + Environment.NewLine);
    }

    public void Write(string message)
    {
      TxMessage.AppendText(message + " ");
    }

    public async Task Process()
    {
      for (int i = 1; i < 11; i++)
      {
        TxMessage.AppendText(i.ToString() + ", "); await Task.Delay(50);
      }
    }

    public void RemoveControls()
    {
      for (int i = PnMain.Controls.Count - 1; i >= 0; i--)
      {
        var control = PnMain.Controls[i];
        control.Visible = false;
        PnMain.Controls.RemoveAt(i);
        control.Dispose();
      }
    }

    private async void EventDownload(object sender, EventArgs e)
    {
      await Task.Delay(100);
      Card.DeleteAllFiles();
      Card.SetIdObject(KeyGenerator.GetUniqueKey(10), "Системный блок компьютер HP G600 процессор Inter Core i5 RAM 16 Gb");
    }

    private async void EventGetFiles(object sender, EventArgs e)
    {
      TxMessage.Clear();
      RadControl control = (sender as RadControl);
      if (control != null) control.Visible = false;
      await Task.Delay(1000);
      Write("Getting FILES ...");
      await Process();
      Write("Done.");
      await Task.Delay(1000);
      if (control != null) control.Visible = true;
    }

    private async void EventSaveFiles(object sender, EventArgs e)
    {
      TxMessage.Clear();
      RadControl control = (sender as RadControl);
      if (control != null) control.Visible = false;
      await Task.Delay(1000);
      Write("SAVING FILES ...");
      await Process();
      Write("Done.");
      await Task.Delay(1000);
      if (control != null) control.Visible = true;
    }
  }
}
