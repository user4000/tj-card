﻿using System;
using System.Drawing;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace TJCard
{
  public partial class FxCardDelete : RadForm, ICardForm
  {
    public string IdObject { get; internal set; } = string.Empty;

    public string NameObject { get; internal set; } = string.Empty;

    public string EntityName { get; internal set; } = string.Empty;

    public FxCardDelete()
    {
      InitializeComponent();
    }

    public void SetProperties()
    {
      TswDelete.Tag = this;
      BxDelete.Tag = this;
      TswDelete.ToggleSwitchElement.Tag = this;
    }

    public void Configure()
    {
      SetProperties();
      SetEvents();
    }

    public void SetEvents()
    {
      
    }

    public bool IdObjectIsSet() => !string.IsNullOrWhiteSpace(IdObject);

    public void ClearIdObject() => SetIdObject(string.Empty, string.Empty);

    public void SetIdObject(string idObject, string nameObject)
    {
      IdObject = idObject;
      NameObject = nameObject;
      PnIdObject.Text = IdObject;
      PnNameObject.Text = NameObject;
      CheckIdObject();
    }

    public void CheckIdObject()
    {
      bool FlagIdObjectIsSet = IdObjectIsSet();
      TswDelete.Value = FlagIdObjectIsSet;
      BxDelete.Enabled = FlagIdObjectIsSet;
      BxDelete.Image = FlagIdObjectIsSet ? BxDeleteImage.Image : BxDeleteNoImage.Image;
      PnWarning.Visible = FlagIdObjectIsSet;
      PbDelete.Visible = FlagIdObjectIsSet;
    }
  }
}
