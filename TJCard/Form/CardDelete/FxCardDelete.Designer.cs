﻿namespace TJCard
{
    partial class FxCardDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FxCardDelete));
      this.PnMain = new Telerik.WinControls.UI.RadPanel();
      this.PbDelete = new System.Windows.Forms.PictureBox();
      this.PnNameObject = new Telerik.WinControls.UI.RadPanel();
      this.PnIdObject = new Telerik.WinControls.UI.RadPanel();
      this.BxDeleteNoImage = new Telerik.WinControls.UI.RadButton();
      this.BxDeleteImage = new Telerik.WinControls.UI.RadButton();
      this.BxDelete = new Telerik.WinControls.UI.RadButton();
      this.TswDelete = new Telerik.WinControls.UI.RadToggleSwitch();
      this.PnWarning = new Telerik.WinControls.UI.RadPanel();
      ((System.ComponentModel.ISupportInitialize)(this.PnMain)).BeginInit();
      this.PnMain.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PbDelete)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnNameObject)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnIdObject)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDeleteNoImage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDeleteImage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDelete)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TswDelete)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnWarning)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PnMain
      // 
      this.PnMain.Controls.Add(this.PbDelete);
      this.PnMain.Controls.Add(this.PnNameObject);
      this.PnMain.Controls.Add(this.PnIdObject);
      this.PnMain.Controls.Add(this.BxDeleteNoImage);
      this.PnMain.Controls.Add(this.BxDeleteImage);
      this.PnMain.Controls.Add(this.BxDelete);
      this.PnMain.Controls.Add(this.TswDelete);
      this.PnMain.Controls.Add(this.PnWarning);
      this.PnMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PnMain.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnMain.Location = new System.Drawing.Point(5, 5);
      this.PnMain.Name = "PnMain";
      this.PnMain.Padding = new System.Windows.Forms.Padding(5);
      this.PnMain.Size = new System.Drawing.Size(882, 260);
      this.PnMain.TabIndex = 0;
      // 
      // PbDelete
      // 
      this.PbDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.PbDelete.Image = ((System.Drawing.Image)(resources.GetObject("PbDelete.Image")));
      this.PbDelete.Location = new System.Drawing.Point(837, 50);
      this.PbDelete.Name = "PbDelete";
      this.PbDelete.Size = new System.Drawing.Size(36, 43);
      this.PbDelete.TabIndex = 4;
      this.PbDelete.TabStop = false;
      // 
      // PnNameObject
      // 
      this.PnNameObject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PnNameObject.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnNameObject.Location = new System.Drawing.Point(8, 139);
      this.PnNameObject.Name = "PnNameObject";
      this.PnNameObject.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
      this.PnNameObject.Size = new System.Drawing.Size(866, 34);
      this.PnNameObject.TabIndex = 1;
      // 
      // PnIdObject
      // 
      this.PnIdObject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PnIdObject.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnIdObject.Location = new System.Drawing.Point(8, 99);
      this.PnIdObject.Name = "PnIdObject";
      this.PnIdObject.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
      this.PnIdObject.Size = new System.Drawing.Size(866, 34);
      this.PnIdObject.TabIndex = 1;
      // 
      // BxDeleteNoImage
      // 
      this.BxDeleteNoImage.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDeleteNoImage.Location = new System.Drawing.Point(834, 200);
      this.BxDeleteNoImage.Name = "BxDeleteNoImage";
      this.BxDeleteNoImage.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
      this.BxDeleteNoImage.Size = new System.Drawing.Size(39, 28);
      this.BxDeleteNoImage.TabIndex = 3;
      this.BxDeleteNoImage.Visible = false;
      // 
      // BxDeleteImage
      // 
      this.BxDeleteImage.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDeleteImage.Image = ((System.Drawing.Image)(resources.GetObject("BxDeleteImage.Image")));
      this.BxDeleteImage.Location = new System.Drawing.Point(766, 200);
      this.BxDeleteImage.Name = "BxDeleteImage";
      this.BxDeleteImage.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
      this.BxDeleteImage.Size = new System.Drawing.Size(39, 28);
      this.BxDeleteImage.TabIndex = 3;
      this.BxDeleteImage.Visible = false;
      // 
      // BxDelete
      // 
      this.BxDelete.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDelete.Image = ((System.Drawing.Image)(resources.GetObject("BxDelete.Image")));
      this.BxDelete.Location = new System.Drawing.Point(8, 50);
      this.BxDelete.Name = "BxDelete";
      this.BxDelete.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
      this.BxDelete.Size = new System.Drawing.Size(200, 32);
      this.BxDelete.TabIndex = 3;
      this.BxDelete.Text = "Удалить";
      // 
      // TswDelete
      // 
      this.TswDelete.Location = new System.Drawing.Point(312, 50);
      this.TswDelete.Name = "TswDelete";
      this.TswDelete.OffText = "Удаление запрещено";
      this.TswDelete.OnText = "Удаление разрешено";
      this.TswDelete.Size = new System.Drawing.Size(200, 32);
      this.TswDelete.TabIndex = 2;
      this.TswDelete.Value = false;
      ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.TswDelete.GetChildAt(0))).ThumbOffset = 0;
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TswDelete.GetChildAt(0).GetChildAt(0))).Text = "Удаление разрешено";
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TswDelete.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TswDelete.GetChildAt(0).GetChildAt(1))).Text = "Удаление запрещено";
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TswDelete.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      // 
      // PnWarning
      // 
      this.PnWarning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PnWarning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
      this.PnWarning.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnWarning.Location = new System.Drawing.Point(8, 8);
      this.PnWarning.Name = "PnWarning";
      this.PnWarning.Size = new System.Drawing.Size(866, 30);
      this.PnWarning.TabIndex = 1;
      // 
      // FxCardDelete
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(892, 270);
      this.Controls.Add(this.PnMain);
      this.Name = "FxCardDelete";
      this.Padding = new System.Windows.Forms.Padding(5);
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "";
      ((System.ComponentModel.ISupportInitialize)(this.PnMain)).EndInit();
      this.PnMain.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PbDelete)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnNameObject)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnIdObject)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDeleteNoImage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDeleteImage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDelete)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TswDelete)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnWarning)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPanel PnMain;
    public Telerik.WinControls.UI.RadButton BxDelete;
    public Telerik.WinControls.UI.RadToggleSwitch TswDelete;
    public Telerik.WinControls.UI.RadPanel PnWarning;
    public Telerik.WinControls.UI.RadPanel PnNameObject;
    public Telerik.WinControls.UI.RadPanel PnIdObject;
    public System.Windows.Forms.PictureBox PbDelete;
    public Telerik.WinControls.UI.RadButton BxDeleteNoImage;
    public Telerik.WinControls.UI.RadButton BxDeleteImage;
  }
}
