﻿using Telerik.WinControls.UI;

namespace TJCard
{
  public partial class FxCardMain
  {
    private void ClearValueOfUserInputControl(RadTextBoxControl cn)
    {
      if (cn == null) return; cn.Clear();
    }

    private void ClearValueOfUserInputControl(RadButtonTextBox cn)
    {
      if (cn == null) return; cn.Clear();
    }

    private void ClearValueOfUserInputControl(RadDateTimePicker cn)
    {
      if (cn == null) return; cn.NullableValue = null;
    }

    private void ClearValueOfUserInputControl(RadToggleSwitch cn)
    {
      if (cn == null) return; cn.Value = false;
    }

    private void ClearValueClassificatorGrid(RadTextBox cn)
    {
      if (cn == null) return; // the Tag of page references to TextBox and the Tag of the TextBox references to Grid //
      RadGridView grid = cn.Tag as RadGridView;
      grid?.ZzGridClearSelection();
    }

    private void ClearValue(CardProperty item)
    {
      DcPage.TryGetValue(item, out RadPageViewPage page);
      if (page == null) return;
      switch (item.Type)
      {
        case PropertyType.Text: ClearValueOfUserInputControl(page.Tag as RadTextBoxControl); break;
        case PropertyType.Barcode: ClearValueOfUserInputControl(page.Tag as RadTextBoxControl); break;
        case PropertyType.RfidCode: ClearValueOfUserInputControl(page.Tag as RadTextBoxControl); break;
        case PropertyType.Int32: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.IntNullable32: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.Int64: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.IntNullable64: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.Decimal: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.DecimalNullable: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.Password: ClearValueOfUserInputControl(page.Tag as RadButtonTextBox); break;
        case PropertyType.Datetime: ClearValueOfUserInputControl(page.Tag as RadDateTimePicker); break;
        case PropertyType.Boolean: ClearValueOfUserInputControl(page.Tag as RadToggleSwitch); break;
        case PropertyType.Classificator: ClearValueClassificatorGrid(page.Tag as RadTextBox); break;

        default: break;
      }
    }

    public void ClearAllValues() // Очистим все значения свойств объекта //
    {
      GxCard.Enabled = false;

      foreach (var item in CardData.DsList)
      {
        ClearValue(item);
        if (item.Type == PropertyType.Classificator) item.IdProperty = string.Empty;
        item.StringValue = string.Empty;
      }

      foreach (var row in GxCard.Rows)
      {
        row.Cells[CnValue.Index].Value = null;
      }

      PvMain.SelectedPage = PgStart;
      GxCard.ZzGridClearSelection();

      GxCard.Enabled = true;
    }

    public void ClearAllValuesButOne(string IdRow) // Очистим все значения свойств объекта кроме одного - IdRow которого является аргументом данного метода //
    {
      GxCard.Enabled = false;

      foreach (var item in CardData.DsList)
        if (item.IdRow != IdRow)
        {
          ClearValue(item);
          if (item.Type == PropertyType.Classificator) item.IdProperty = string.Empty;
          item.StringValue = string.Empty;
        }

      foreach (var row in GxCard.Rows)
      {
        CardProperty property = row.DataBoundItem as CardProperty;
        if (property.IdRow != IdRow)
        {
          row.Cells[CnValue.Index].Value = null;
        }
      }

      PvMain.SelectedPage = PgStart;
      GxCard.ZzGridClearSelection();

      GxCard.Enabled = true;
    }
  }
}
