﻿using System;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using Telerik.WinControls;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.Collections.Generic;

namespace TJCard
{
  public partial class FxCardMain : RadForm, ICardForm
  {
    internal string CurrentIdRow { get; private set; } = string.Empty;

    internal string CurrentIdParent { get; private set; } = null;

    public CardProperty CurrentProperty { get; private set; } = null;

    public GridViewDataColumn CnValue { get; private set; } = null;

    public GridViewDataColumn CnName { get; private set; } = null;

    public GridViewDataColumn CnIdProperty { get; private set; } = null;

    public string GetIdObject { get => CardData.IdObject; }

    public bool ModeCreateNewObject { get; private set; } = true; // This form can work in two modes: [Create a New Object] and [Update an Existing Object] //

    public bool ModeDebug { get; private set; } = false;

    public DataOperatorCard CardData { get; private set; } = null;

    public Dictionary<CardProperty, RadPageViewPage> DcPage { get; } = new Dictionary<CardProperty, RadPageViewPage>();

    public DateTime LastTimeEnterKeyPressed { get; set; } = DateTime.Now;

    public char MinusSign { get; } = '-';

    public char DecimalPoint { get; set; } = '.';

    public char PasswordChar { get; set; } = '*';

    public string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss";

    public string DateOnlyFormat { get; set; } = "yyyy-MM-dd";

    public string SwitcherOnText { get; set; } = "Yes";

    public string SwitcherOffText { get; set; } = "No";

    public bool AllowMinusForInt32 { get; set; } = false;

    public RadButton BxBarcodeScan { get; private set; } = new RadButton();

    public RadTextBoxControl TxBarcode { get; private set; } = new RadTextBoxControl();

    public RadButton BxRfidScan { get; private set; } = new RadButton();

    public RadTextBoxControl TxRfidCode { get; private set; } = new RadTextBoxControl();

    private bool EnableShowChangedTextInMainGrid { get; set; } = true;

    public string ControlNameForUserInput { get; } = "ControlForUserInput";

    public string StrRfidScan { get; set; } = "RFID scan";

    public string StrBarcode { get; set; } = "Barcode";

    public event EventHandler UserSelectedOneRow;

    public FxCardMain()
    {
      InitializeComponent();
    }

    public void SetMode(bool SetModeCreateNewObject, bool SetModeDebug)
    {
      ModeCreateNewObject = SetModeCreateNewObject;
      BxDownload.Enabled = !SetModeCreateNewObject;
      BxDownload.Visible = !SetModeCreateNewObject;
      SeparatorOne.Visible = !SetModeCreateNewObject;

      BxSearch.Visible = false;

      /*if (SetModeCreateNewObject)
      {
        Point point = new Point() { X = BxClearAllFields.Location.X, Y = BxClearAllFields.Location.Y };        
        BxClearAllFields.Location = BxDownload.Location;
        BxSearch.Location = point;
      }*/

      ModeDebug = SetModeDebug;
      SetProperties();
      SetMainGridProperties();
      SetEvents();
      CheckIdObject();
    }

    private void SetProperties()
    {
      foreach (var page in PvMain.Pages) page.Item.Visibility = ElementVisibility.Collapsed;
      BxClearAllFields.Tag = this;
      BxDownload.Tag = this;
      BxUpload.Tag = this;
      BxClearAllFields.Tag = this;
    }

    public void AdjustFormWidth()
    {
      PnGrid.Width = (this.Parent.Width * 50) / 100;
      CnName.Width = (PnGrid.Width * 40) / 100;
      CnValue.Width = (PnGrid.Width * 59) / 100;
    }

    private void SetMainGridProperties()
    {
      CardData = new DataOperatorCard();
      GxCard.ShowColumnHeaders = true;
      GxCard.ShowRowHeaderColumn = false;
      GxCard.TableElement.TableHeaderHeight = 20;

      if (ModeDebug)
        foreach (var column in GxCard.Columns) column.IsVisible = true;
      else
        foreach (var column in GxCard.Columns) column.HeaderText = string.Empty;

      CnIdProperty = GxCard.Columns["CnIdProperty"];
      CnName = GxCard.Columns["CnName"];
      CnValue = GxCard.Columns["CnValue"];

      AdjustFormWidth();

      if (ModeDebug)
      {
        GxCard.Columns[0].Width = 150;
        GxCard.Columns[3].Width = 200;
        CnValue.Width = 100;
        GxCard.Columns[1].Width = 100;
        GxCard.Columns[2].Width = 130;
        GxCard.Columns[5].Width = 100;
        GxCard.Columns[6].Width = 70;
      }

      GxCard.Relations.AddSelfReference(this.GxCard.MasterTemplate, "CnIdRow", "CnIdParent");

      GxCard.DataSource = CardData.DsList;
      GxCard.MasterTemplate.ExpandAll();
      GxCard.ZzGridClearSelection();
    }

    private void SetEvents()
    {
      BxClearAllFields.Click += (s, e) => ClearAllValues();
      GxCard.SelectionChanged += EventSelectionChanged;
    }

    private void EventUserSelectedOneRow() // Most important event that calls many other events //
    {
      CurrentIdRow = string.Empty;
      CurrentIdParent = CurrentIdRow;
      CurrentProperty = null;

      if (GxCard.SelectedRows.Count > 0)
        try
        {
          GridViewRowInfo row = GxCard.SelectedRows[0];
          if (row != null)
          {
            CurrentProperty = row.DataBoundItem as CardProperty;
            CurrentIdRow = CurrentProperty.IdRow;
            CurrentIdParent = CurrentProperty.IdParent ?? CurrentIdRow;
          }
        }
        catch { }

      if (CurrentProperty != null)
      {
        SelectPage();
        CheckIfCurrentCellHasAnyValue(CurrentProperty);
        UserSelectedOneRow?.Invoke(CurrentProperty, new EventArgs());
      }
    }

    private void CheckIfClassificatorPropertyHasValueButGridHasNoAnyRowSelected(CardProperty property)
    {
      if (string.IsNullOrWhiteSpace(property.IdProperty)) return;
      RadGridView grid = property.InputField as RadGridView;
      if ((grid != null) && (grid.SelectedRows.Count < 1))
        foreach (var row in grid.Rows)
          if (row.Cells[0].Value.ToString() == property.IdProperty) // TODO: Здесь предполагается что столбец с индексом 0 содержит id объекта. Это нужно как-то более явно стандартизировать. //
          {
            grid.CurrentRow = row; break;
          }
    }

    private void CheckIfCurrentCellHasAnyValue(CardProperty property)
    {
      /*
      After the data has been received from the server and all the cells in the table have updated their values, 
      the data entry elements that are located to the right of the table can still have empty values. 
      It is necessary to coordinate the values from the table cells and add these values to the corresponding controls 
      intended for input, in the event that they now do not contain any values.
      */
      //if (ModeCreateNewObject) return; // This method works only for [Updating an existing object] mode //
      string value = property.DisplayValue;
      if (string.IsNullOrWhiteSpace(value) && (property.Type != PropertyType.Classificator)) return;

      // If the cell received the value from the server, and the text field is empty, we must set the value in the text field.//
      switch (property.Type)
      {
        case PropertyType.Decimal:
        case PropertyType.DecimalNullable:
        case PropertyType.IntNullable32:
        case PropertyType.IntNullable64:
        case PropertyType.Int32:
        case PropertyType.Int64:
          {
            RadButtonTextBox textbox = property.InputField as RadButtonTextBox;
            if (textbox == null) return;
            if (!string.IsNullOrWhiteSpace(textbox.Text)) return;
            EnableShowChangedTextInMainGrid = false;
            textbox.Text = value;
            EnableShowChangedTextInMainGrid = true;
          }
          break;
        case PropertyType.RfidCode:
        case PropertyType.Barcode:
        case PropertyType.Text:
          {
            RadTextBoxControl textbox = property.InputField as RadTextBoxControl;
            if (textbox == null) return;
            if (!string.IsNullOrWhiteSpace(textbox.Text)) return;
            EnableShowChangedTextInMainGrid = false;
            textbox.Text = value;
            EnableShowChangedTextInMainGrid = true;
          }
          break;
        case PropertyType.Boolean:
          {
            RadToggleSwitch control = property.InputField as RadToggleSwitch;
            if (control == null) return;
            EnableShowChangedTextInMainGrid = false;
            control.Value = (property.DisplayValue == CardData.SwitcherOnCellText);
            EnableShowChangedTextInMainGrid = true;
          }
          break;
        case PropertyType.Datetime:
          {
            RadDateTimePicker control = property.InputField as RadDateTimePicker;
            if (control == null) return;
            if (!string.IsNullOrWhiteSpace(control.Text)) return;
            EnableShowChangedTextInMainGrid = false;
            control.Text = value;
            //control.Value = DateTime.ParseExact(value, control.CustomFormat, CultureInfo.InvariantCulture);
            EnableShowChangedTextInMainGrid = true;
          }
          break;
        case PropertyType.Classificator:
          CheckIfClassificatorPropertyHasValueButGridHasNoAnyRowSelected(property);
          break;
        default: break;
      }
    }

    private void EventSelectionChanged(object sender, EventArgs e)
    {
      EventUserSelectedOneRow();
    }

    private RadPageViewPage GetPage(CardProperty property)
    {
      RadPageViewPage result = null;
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page)) result = page;
      return result;
    }

    private void TryToSetFocusOnUserInputControl(RadPageViewPage page)
    {
      RadControl control = page.Tag as RadControl;
      if (control == null) return;
      try { GxCard.Focus(); } catch { } // Это необходимо сделать чтобы убрать фокус с предыдущего поля ввода, иначе возможен неприятный визуальный эффект //
      Thread.Sleep(50);
      try { control.Focus(); } catch { }
      Thread.Sleep(50);
    }

    private void CreateNewPage()
    {
      RadPageViewPage page = new RadPageViewPage(CurrentIdRow);
      DcPage.Add(CurrentProperty, page);
      PvMain.Pages.Add(page);
      PvMain.SelectedPage = page;
      AddControlToPage(page, CurrentProperty);
      page.Item.Visibility = ElementVisibility.Collapsed;
      TryToSetFocusOnUserInputControl(page);
    }

    private void TrySelectPage()
    {
      if (DcPage.TryGetValue(CurrentProperty, out RadPageViewPage page))
      {
        PvMain.SelectedPage = page;
        TryToSetFocusOnUserInputControl(page);
      }
    }

    private void SelectPage()
    {
      if (DcPage.ContainsKey(CurrentProperty))
      {
        TrySelectPage();
      }
      else
      {
        CreateNewPage();
      }
    }

    private void TryToSelectRow(string IdRow)
    {
      foreach (var row in GxCard.Rows)
        if (row.Cells[0].Value.ToString() == IdRow)
        { GxCard.CurrentRow = row; break; }
    }

    private void MakeSureNewRowIsExpanded()
    {
      bool flag1 = false; bool flag2 = false;
      foreach (var row in GxCard.Rows)
      {
        if (row.Cells[0].Value.ToString() == CurrentIdParent.ToString())
        {
          row.IsExpanded = true; flag1 = true; if (flag1 && flag2) break;
        }
        if (row.Cells[0].Value.ToString() == CurrentIdRow.ToString())
        {
          row.EnsureVisible(); flag2 = true; if (flag1 && flag2) break;
        }
      }
    }

    private void DeletePageFromDictionary(string IdRow)
    {
      CardProperty property = null;

      foreach (var item in DcPage)
        if (item.Key.IdRow == IdRow) { property = item.Key; break; }

      if (property == null) return;

      RadPageViewPage page = null;
      if (DcPage.TryGetValue(property, out RadPageViewPage PageFound)) page = PageFound;

      if (page == null) return;

      page.IsContentVisible = false;

      DcPage.Remove(property);
      PvMain.Pages.Remove(page);
      foreach (Control control in page.Controls) control.Dispose();
      page.Controls.Clear();
      page.Dispose();
    }

    private void ShowChangedText(string value, bool RefreshGridRow = false)
    {
      if (!EnableShowChangedTextInMainGrid) return;
      if (CurrentProperty != null) CurrentProperty.DisplayValue = value;
      if (RefreshGridRow) GxCard.CurrentRow?.InvalidateRow();
    }

    private void ShowChangedText(KeyEventArgs e, string value, bool RefreshGridRow = false)
    {
      if (e.KeyCode == Keys.Enter) { GotoNextRow(); return; }
      ShowChangedText(value, RefreshGridRow);
    }

    private Tuple<RadPanel, RadButton> CreateTopPanel(RadPageViewPage page, bool CreateClearButton)
    {
      RadPanel PnTop = new RadPanel();
      RadButton BxClear = CreateClearButton ? new RadButton() : null;

      page.Controls.Add(PnTop);

      PnTop.Dock = DockStyle.Top;
      PnTop.Font = Standard.MainFont;
      PnTop.Location = new Point(0, 0);
      PnTop.Name = "PnTop";
      PnTop.Size = new Size(431, 30);
      PnTop.TabIndex = 2;
      PnTop.Padding = new Padding(3, 3, 3, 3);

      if (BxClear != null)
      {
        BxClear.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
        BxClear.DisplayStyle = DisplayStyle.Image;
        BxClear.Image = PictureDepot.GetImageClear();
        BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
        BxClear.Location = new Point(400, 6);
        BxClear.Name = "BxClear";
        BxClear.Size = new Size(26, 24);
        BxClear.TabIndex = 0;
        BxClear.Text = "";
        BxClear.Dock = DockStyle.Right;
        BxClear.TabStop = false;
        PnTop.Controls.Add(BxClear);
      }

      PnTop.Text = "  " + CurrentProperty.PropertyName;

      return new Tuple<RadPanel, RadButton>(PnTop, BxClear);
    }

    private RadPanel CreateBottomPanel(RadPageViewPage page)
    {
      var PnBottom = new RadPanel();
      page.Controls.Add(PnBottom);

      PnBottom.Dock = DockStyle.Bottom;
      PnBottom.Font = Standard.MainFont;
      PnBottom.Location = new Point(0, 415);
      PnBottom.Name = "PnBottom";
      PnBottom.Size = new Size(431, 30);
      PnBottom.TabIndex = 2;

      return PnBottom;
    }

    public void ClearIdObject()
    {
      SetIdObject(string.Empty, string.Empty);
      PvMain.SelectedPage = PgStart;
      if (ModeCreateNewObject) ClearAllValues();
    }

    public void SetIdObject(string idObject, string nameObject) // Устанавливаем id и имя редактируемого объекта //
    {
      if (ModeCreateNewObject) return;
      CardData.IdObject = idObject;
      CardData.NameObject = nameObject;
      CheckIdObject();
    }

    public void CheckIdObject() // Устанавливаем элементы формы в зависимости от того, задан или нет редактируемый объект //
    {
      if (ModeCreateNewObject) return;
      bool IdObjectIsSet = CardData.IdObjectIsSet();
      PnGridBottom.Text = CardData.GetTextForSelectedObject();
      BxUpload.Enabled = IdObjectIsSet;
      BxClearAllFields.Enabled = IdObjectIsSet;
      GxCard.Visible = IdObjectIsSet;
    }

    public void RefreshAllRows()
    {
      foreach (var row in GxCard.Rows) if (row.Cells[CnValue.Index].Value?.ToString()?.Length > 0) row.InvalidateRow();
    }

    private void GotoNextRow()
    {
      TimeSpan span = DateTime.Now - LastTimeEnterKeyPressed;
      int ms = (int)span.TotalMilliseconds;
      if (ms < 500) return;

      LastTimeEnterKeyPressed = DateTime.Now;
      if (GxCard.GridNavigator.SelectNextRow(1))
      {
        EventUserSelectedOneRow();
      }
    }
  }
}


/*
    private void EventGridCurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
    {
      if (e.CurrentRow is GridViewDataRowInfo)
      {
        // Current row is row containing data //
      }
    }
*/
