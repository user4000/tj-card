﻿using System;
using System.Globalization;

namespace TJCard
{
  public partial class FxCardMain
  {
    public void UpdateInt32(string IdRow, int value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value.ToString();
    }

    public void UpdateNullableInt32(string IdRow, int? value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value == null ? string.Empty : value.ToString();
    }

    public void UpdateInt64(string IdRow, long value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value.ToString();
    }

    public void UpdateNullableInt64(string IdRow, long? value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value == null ? string.Empty : value.ToString();
    }

    public void UpdateDecimal(string IdRow, decimal value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value.ToString("G", CultureInfo.InvariantCulture);
    }

    public void UpdateNullableDecimal(string IdRow, decimal? value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      decimal x = value ?? 0;
      property.DisplayValue = value == null ? string.Empty : x.ToString("G", CultureInfo.InvariantCulture);
    }

    public void UpdateText(string IdRow, string value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value == null ? string.Empty : value.ToString();
    }

    public void UpdatePassword(string IdRow, string password)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = Standard.HidePasswordValue(password);
    }

    public void UpdateBarcode(string IdRow, string value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value == null ? string.Empty : value.ToString();
    }

    public void UpdateRfid(string IdRow, string value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value == null ? string.Empty : value.ToString();
    }

    public void UpdateBoolean(string IdRow, bool value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = CardData.ConvertBooleanToString(value);
    }

    public void UpdateDatetime(string IdRow, DateTime value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.DisplayValue = value.ToString(property.ByteArray == null ? this.DateTimeFormat : this.DateOnlyFormat); // NOTE: DateOnly //
    }

    public void UpdateClassificator(string IdRow, int value) => UpdateClassificator(IdRow, value.ToString());

    public void UpdateClassificator(string IdRow, string value)
    {
      CardProperty property = CardData.GetProperty(IdRow);
      if (property == null) return;
      property.IdProperty = value ?? string.Empty; 
      property.DisplayValue = CardData.GetTextValueOfClassificator(property.DataSource, value ?? string.Empty);
    }
  }
}
