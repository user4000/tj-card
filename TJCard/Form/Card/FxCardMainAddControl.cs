﻿using System;
using System.Drawing;
using System.Diagnostics;
using Telerik.WinControls;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using System.Threading.Tasks;

namespace TJCard
{
  public partial class FxCardMain
  {
    private void AddControlToPage(RadPageViewPage page, CardProperty item)
    {
      switch (item.Type)
      {
        case PropertyType.Text: AddControlForText(page, item); break;
        case PropertyType.Int32: AddControlForInt32(page, item); break;
        case PropertyType.IntNullable32: AddControlForInt32(page, item); break;
        case PropertyType.Int64: AddControlForInt64(page, item); break;
        case PropertyType.IntNullable64: AddControlForInt64(page, item); break;
        case PropertyType.Barcode: AddControlForBarcode(page, item); break;
        case PropertyType.Boolean: AddControlForBoolean(page, item); break;
        case PropertyType.Decimal: AddControlForDecimal(page, item); break;
        case PropertyType.DecimalNullable: AddControlForDecimal(page, item); break;
        case PropertyType.RfidCode: AddControlForRfidCode(page, item); break;
        case PropertyType.Password: AddControlForPassword(page, item); break;
        case PropertyType.Datetime: AddControlForDatetime(page, item); break;
        case PropertyType.Classificator: AddControlForClassificator(page, item); break;
        default: break;
      }
    }

    private void SetEventCopyTextToClipboard(RadPanel panel, RadControl control)
    {
      panel.Cursor = Cursors.Hand;
      panel.Click += (s, e) =>
      {
        try { if (!string.IsNullOrWhiteSpace(control.Text)) Clipboard.SetText(control.Text); } catch { }
      };
    }

    private void AddControlForText(RadPageViewPage page, CardProperty item)
    {
      var TxText = new RadTextBoxControl();
      item.InputField = TxText;
      page.Controls.Add(TxText);
      page.Tag = TxText;
      // Порядок создания элементов имеет значение //
      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);
      var TopPanel = TopPanelAndClearButton.Item1;    
      //-------------------------------------------------------------------------------------------------------------------------
      TxText.Dock = DockStyle.Fill;
      TxText.Font = Standard.MainFont;
      TxText.Location = new Point(0, 35);
      TxText.MaxLength = 100000;
      TxText.Multiline = true;
      TxText.Name = ControlNameForUserInput;
      TxText.ShowClearButton = false;
      TxText.Size = new Size(431, 380);
      TxText.TabIndex = 0;
      //-------------------------------------------------------------------------------------------------------------------------
      TxText.TextChanged += EventTextChanged;
      TxText.TextChanging += EventTextChanging;
      TxText.KeyUp += EventKeyUp;
      TopPanelAndClearButton.Item2.Click += EventClearText;
      //-------------------------------------------------------------------------------------------------------------------------
      void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(e, TxText.Text, true);
      void EventTextChanging(object sender, TextChangingEventArgs e) => ShowChangedText(TxText.Text, true);
      void EventTextChanged(object sender, EventArgs e) => ShowChangedText(TxText.Text);
      void EventClearText(object sender, EventArgs e)
      {
        if (TxText.Text.Length == 0) return;
        TxText.Clear();
        ShowChangedText(TxText.Text, true);
      }
      SetEventCopyTextToClipboard(TopPanel, TxText);
    }

    private void AddControlForBarcode(RadPageViewPage page, CardProperty item)
    {
      //if ((BxBarcodeScan != null) || (TxBarcode != null)) return; // Only one barcode-control is allowed //

      var TxText = TxBarcode; // new RadTextBoxControl();
      item.InputField = TxText;
      page.Controls.Add(TxText);
      page.Tag = TxText;

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);
      var TopPanel = TopPanelAndClearButton.Item1;
      //-------------------------------------------------------------------------------------------------------------------------
      TxText.Dock = DockStyle.Fill;
      TxText.Font = Standard.MainFont;
      TxText.Location = new Point(0, 35);
      TxText.MaxLength = 10000;
      TxText.Multiline = true;
      TxText.Name = ControlNameForUserInput;
      TxText.ShowClearButton = false;
      TxText.Size = new Size(431, 380);
      TxText.TabIndex = 0;
      TxText.IsReadOnly = true;
      //-------------------------------------------------------------------------------------------------------------------------
      RadButton BxScan = BxBarcodeScan; //  new RadButton();
      RadPanel PnTop = TopPanelAndClearButton.Item1;

      BxScan.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
      BxScan.DisplayStyle = DisplayStyle.ImageAndText;
      BxScan.Image = ButtonImageBarcode.Image;
      BxScan.Location = new Point(6, 6);
      BxScan.Name = "BxScan";
      BxScan.Size = new Size(150, 24);
      BxScan.Padding = new Padding(10, 0, 0, 0);
      BxScan.TabIndex = 0;
      BxScan.Text = StrBarcode;
      BxScan.Dock = DockStyle.Left;
      BxScan.Font = TxText.Font;
      BxScan.TabStop = false;   

      PnTop.Controls.Add(BxScan);
      PnTop.Text = string.Empty;
      BxBarcodeScan.Tag = TxBarcode;
      TxBarcode.Tag = this;

      //-------------------------------------------------------------------------------------------------------------------------
      TxText.TextChanged += EventTextChanged;
      //TxText.TextChanging += EventTextChanging;
      //TxText.KeyUp += EventKeyUp;
      TopPanelAndClearButton.Item2.Click += EventClearText;
      //-------------------------------------------------------------------------------------------------------------------------
      //void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(TxText.Text, true);
      //void EventTextChanging(object sender, TextChangingEventArgs e) => ShowChangedText(TxText.Text, true);
      void EventTextChanged(object sender, EventArgs e) => ShowChangedText(TxText.Text, true);
      void EventClearText(object sender, EventArgs e)
      {
        if (TxText.Text.Length == 0) return;
        TxText.Clear();
        ShowChangedText(TxText.Text, true);
      }
      SetEventCopyTextToClipboard(TopPanel, TxText);
    }

    private void AddControlForRfidCode(RadPageViewPage page, CardProperty item)
    {
      //if ((BxRfidScan != null) || (TxRfidCode != null)) return; // Only one RFID-control is allowed //

      var TxText = TxRfidCode; // new RadTextBoxControl();
      item.InputField = TxText;
      page.Controls.Add(TxText);
      page.Tag = TxText;

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);
      var TopPanel = TopPanelAndClearButton.Item1;
      //-------------------------------------------------------------------------------------------------------------------------
      TxText.Dock = DockStyle.Fill;
      TxText.Font = Standard.MainFont;
      TxText.Location = new Point(0, 35);
      TxText.MaxLength = 10000;
      TxText.Multiline = true;
      TxText.Name = "TJCardTxRfid";
      TxText.ShowClearButton = false;
      TxText.Size = new Size(431, 380);
      TxText.TabIndex = 0;
      TxText.IsReadOnly = true;
      //-------------------------------------------------------------------------------------------------------------------------
      RadButton BxScan = BxRfidScan; // new RadButton();
      RadPanel PnTop = TopPanelAndClearButton.Item1;

      BxScan.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Right)));
      BxScan.DisplayStyle = DisplayStyle.ImageAndText;
      BxScan.Image = ButtonImageRfid.Image;
      //BxScan.ImageAlignment = ContentAlignment.MiddleCenter;
      BxScan.Location = new Point(6, 6);
      BxScan.Name = "BxScan";
      BxScan.Size = new Size(150, 24);
      BxScan.Padding = new Padding(10, 0, 0, 0);
      BxScan.TabIndex = 0;
      BxScan.Text = StrRfidScan;
      BxScan.Dock = DockStyle.Left;
      BxScan.Font = TxText.Font;
      BxScan.TabStop = false;

      PnTop.Controls.Add(BxScan);
      PnTop.Text = string.Empty;
      BxRfidScan.Tag = TxRfidCode;
      TxRfidCode.Tag = this;

      //-------------------------------------------------------------------------------------------------------------------------
      TxText.TextChanged += EventTextChanged;
      //TxText.TextChanging += EventTextChanging;
      //TxText.KeyUp += EventKeyUp;
      TopPanelAndClearButton.Item2.Click += EventClearText;
      //-------------------------------------------------------------------------------------------------------------------------
      //void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(TxText.Text, true);
      //void EventTextChanging(object sender, TextChangingEventArgs e) => ShowChangedText(TxText.Text, true);
      void EventTextChanged(object sender, EventArgs e) => ShowChangedText(TxText.Text, true);
      void EventClearText(object sender, EventArgs e)
      {
        if (TxText.Text.Length == 0) return;
        TxText.Clear();
        ShowChangedText(TxText.Text, true);
      }
      SetEventCopyTextToClipboard(TopPanel, TxText);
    }

    private void AddControlForPassword(RadPageViewPage page, CardProperty item)
    {
      var TxPassword = new RadButtonTextBox();
      var BxClear = new RadImageButtonElement();
      var BxKey = new RadImageButtonElement();

      item.InputField = TxPassword;
      page.Controls.Add(TxPassword);
      page.Tag = TxPassword;

      // Порядок создания элементов имеет значение //
      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, false);
      var TopPanel = TopPanelAndClearButton.Item1;

      TxPassword.AutoSize = false;
      TxPassword.Dock = DockStyle.Top;
      TxPassword.Font = Standard.MainFont;
      TxPassword.LeftButtonItems.AddRange(new RadItem[] { BxKey });
      TxPassword.Location = new Point(0, 35);
      TxPassword.MaxLength = 4096;
      TxPassword.Name = "TJCardTxPassword";
      TxPassword.PasswordChar = PasswordChar;
      TxPassword.RightButtonItems.AddRange(new RadItem[] { BxClear });
      TxPassword.Size = new Size(431, 35);
      TxPassword.TabIndex = 3;
      // 
      // BxClear
      // 
      BxClear.AutoSize = false;
      BxClear.Bounds = new Rectangle(0, 0, 17, 33);
      BxClear.Image = PictureDepot.GetImageClear();
      BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
      BxClear.Margin = new Padding(2, 0, 0, 0);
      BxClear.Name = "BxClear";
      BxClear.ShowBorder = false;
      BxClear.Text = "";
      // 
      // BxKey
      // 
      BxKey.AutoSize = false;
      BxKey.Bounds = new Rectangle(0, 0, 39, 30);
      BxKey.Image = PictureDepot.GetImagePasswordYellowKey();
      BxKey.ImageAlignment = ContentAlignment.MiddleLeft;
      BxKey.Margin = new Padding(2, 0, 0, 0);
      BxKey.Name = "BxKey";
      BxKey.ShowBorder = false;
      BxKey.Text = "";

      BxClear.Click += EventClearText;
      TxPassword.TextChanged += EventTextChanged;
      TxPassword.TextChanging += EventTextChanging;
      TxPassword.KeyUp += EventKeyUp;
      BxKey.MouseHover += EventShowPassword;
      BxKey.MouseLeave += EventHidePassword;

      string GetPassword() => Standard.HidePasswordValue(TxPassword.Text);

      void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(e, GetPassword(), true);

      void EventTextChanging(object sender, TextChangingEventArgs e) => ShowChangedText(GetPassword(), true);

      void EventTextChanged(object sender, EventArgs e)
      {
        ShowChangedText(GetPassword());
        item.StringValue = TxPassword.Text;
      }

      void EventClearText(object sender, EventArgs e)
      {
        if (TxPassword.Text.Length == 0) return;
        TxPassword.Clear();
        ShowChangedText(TxPassword.Text, true);
      }

      void EventShowPassword(object sender, EventArgs e)
      {
        TxPassword.PasswordChar = '\0';
      }

      void EventHidePassword(object sender, EventArgs e)
      {
        TxPassword.PasswordChar = PasswordChar;
      }

      SetEventCopyTextToClipboard(TopPanel, TxPassword);
    }

    private void AddControlForInt32(RadPageViewPage page, CardProperty item)
    {
      AddControlForNumericValue(page, item, 11, AllowMinusForInt32, false);
    }

    private void AddControlForInt64(RadPageViewPage page, CardProperty item)
    {
      AddControlForNumericValue(page, item, 20, true, false);
    }

    private void AddControlForDecimal(RadPageViewPage page, CardProperty item)
    {
      AddControlForNumericValue(page, item, 50, true, true);
    }

    private void AddControlForNumericValue(RadPageViewPage page, CardProperty item, int MaxLength, bool AllowMinus, bool AllowDecimalPoint)
    {
      var TxNumber = new RadButtonTextBox();
      var BxClear = new RadImageButtonElement();
      var BxKey = new RadImageButtonElement();

      item.InputField = TxNumber;
     
      page.Controls.Add(TxNumber);
      page.Tag = TxNumber;

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, false);
      var TopPanel = TopPanelAndClearButton.Item1;

      TxNumber.AutoSize = false;
      TxNumber.Dock = DockStyle.Top;
      TxNumber.Font = Standard.MainFont;
      TxNumber.Location = new Point(0, 35);
      TxNumber.MaxLength = MaxLength;
      TxNumber.Name = "TxNumber";
      TxNumber.RightButtonItems.AddRange(new RadItem[] {BxClear});
      TxNumber.Size = new Size(431, 35);
      TxNumber.TabIndex = 3;
      // 
      // BxClear
      // 
      BxClear.AutoSize = false;
      BxClear.Bounds = new Rectangle(0, 0, 17, 33);
      BxClear.Image = PictureDepot.GetImageClear();
      BxClear.ImageAlignment = ContentAlignment.MiddleCenter;
      BxClear.ImageIndexClicked = 0;
      BxClear.ImageIndexHovered = 0;
      BxClear.Margin = new Padding(2, 0, 0, 0);
      BxClear.Name = "BxClear";
      BxClear.ShowBorder = false;
      BxClear.Text = "";
      BxClear.UseCompatibleTextRendering = false;

      BxClear.Click += EventClearText;
      TxNumber.KeyPress += EventNumberKeyPress;
      TxNumber.TextChanged += EventTextChanged;
      TxNumber.TextChanging += EventTextChanging;
      TxNumber.KeyUp += EventKeyUp;

      void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(e, TxNumber.Text, true);
      void EventTextChanging(object sender, TextChangingEventArgs e) => ShowChangedText(TxNumber.Text, true);
      void EventTextChanged(object sender, EventArgs e) => ShowChangedText(TxNumber.Text);
      void EventClearText(object sender, EventArgs e)
      {
        if (TxNumber.Text.Length == 0) return;
        TxNumber.Clear();
        ShowChangedText(TxNumber.Text, true);
      }

      void EventNumberKeyPress(object sender, KeyPressEventArgs e)
      {
        if // https://stackoverflow.com/questions/13158226/allow-only-numbers-and-negative-values //
          (
            (!char.IsControl(e.KeyChar)) &&
            (!char.IsDigit(e.KeyChar)) &&
            (e.KeyChar != DecimalPoint) &&
            (e.KeyChar != MinusSign)
          )
          e.Handled = true;

        if ((!AllowMinus) && (e.KeyChar == MinusSign))
          e.Handled = true;

        if ((!AllowDecimalPoint) && (e.KeyChar == DecimalPoint))
          e.Handled = true;

        // only allow one decimal point
        if (e.KeyChar == DecimalPoint && TxNumber.Text.IndexOf(DecimalPoint) > -1)
          e.Handled = true;

        // This is not very good and we have refactored this block //
        // only allow minus sign at the beginning
        /*if ((e.KeyChar == MinusSign) && TxNumber.Text.Length > 0)
          e.Handled = true;  */

        if ((e.KeyChar == MinusSign) && TxNumber.SelectionStart > 0)
          e.Handled = true;
      }

      SetEventCopyTextToClipboard(TopPanel, TxNumber);
    }

    private void AddControlForDatetime(RadPageViewPage page, CardProperty item)
    {
      RadDateTimePicker TxDate = new RadDateTimePicker();

      item.InputField = TxDate;
      page.Controls.Add(TxDate);
      page.Tag = TxDate;

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);
      var TopPanel = TopPanelAndClearButton.Item1;
      TopPanel.Cursor = Cursors.Hand;

      TxDate.AutoSize = false;
      TxDate.CustomFormat = "";
      TxDate.Dock = DockStyle.Top;
      TxDate.Font = Standard.MainFont;
      TxDate.Location = new Point(0, 35);
      TxDate.Name = "TxDate";
      TxDate.ShowItemToolTips = false;
      TxDate.Size = new Size(374, 35);
      TxDate.TabIndex = 3;
      TxDate.TabStop = false;
      //------------------------------------------------------------------------------------------------------------------------

      bool ShowTime = item.ByteArray == null;

      TxDate.DateTimePickerElement.ShowTimePicker = ShowTime;

      TxDate.Format = DateTimePickerFormat.Custom;
      TxDate.CustomFormat = ShowTime ? DateTimeFormat : DateOnlyFormat;
      //------------------------------------------------------------------------------------------------------------------------
      (TxDate.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMinSize = new Size(350, 350);
      TxDate.NullableValue = null;

      TopPanelAndClearButton.Item2.Click += EventClearValue;
      TxDate.ValueChanging += EventValueChanging;
      TxDate.ValueChanged += EventValueChanged;
      TxDate.KeyUp += EventKeyUp;

      void EventKeyUp(object sender, KeyEventArgs e) => ShowChangedText(e, TxDate.Text, true);
      void EventValueChanging(object sender, ValueChangingEventArgs e) => ShowChangedText(TxDate.Text, true);
      void EventValueChanged(object sender, EventArgs e) => ShowChangedText(TxDate.Text, true);
      void EventClearValue(object sender, EventArgs e)
      {
        TxDate.NullableValue = null; ShowChangedText(TxDate.Text, true);
      }

      SetEventCopyTextToClipboard(TopPanel, TxDate);
    }

    private void AddControlForBoolean(RadPageViewPage page, CardProperty item)
    {
      RadToggleSwitch TsBoolean = new RadToggleSwitch();
      item.InputField = TsBoolean;
      page.Controls.Add(TsBoolean);
      page.Tag = TsBoolean;

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, false);

      TsBoolean.Font = Standard.MainFont;
      TsBoolean.Location = new Point(2, 50);
      TsBoolean.Name = "TsBoolean";
      TsBoolean.Size = new Size(150, 35);
      TsBoolean.TabIndex = 3;
      TsBoolean.Value = false;
      ((ToggleSwitchPartElement)(TsBoolean.GetChildAt(0).GetChildAt(0))).Font = new Font("Verdana", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
      ((ToggleSwitchPartElement)(TsBoolean.GetChildAt(0).GetChildAt(1))).ForeColor = Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(107)))), ((int)(((byte)(168)))));
      ((ToggleSwitchPartElement)(TsBoolean.GetChildAt(0).GetChildAt(1))).Font = new Font("Verdana", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));

      TsBoolean.OnElement.Text = SwitcherOnText;
      TsBoolean.OffElement.Text = SwitcherOffText;

      TsBoolean.ValueChanged += EventValueChanged;
      TsBoolean.ValueChanging += EventValueChanging;

      void EventValueChanged(object sender, EventArgs e) => ShowChangedText(GetCellText(), true);
      void EventValueChanging(object sender, ValueChangingEventArgs e) => ShowChangedText(GetCellText(), true);
      string GetCellText() => CardData.ConvertBooleanToString(TsBoolean.Value);
    }

    private void AddControlForClassificator(GridConfig config, RadPageViewPage page, CardProperty item) // Формируем grid-справочник на основе объекта типа GridConfig. Здесь иерархия не применяется //
    {
      if (config == null) return;
      RadGridView GxClassificator = config.GridClassificator;

      RadTextBox TxIdProperty = new RadTextBox();

      //item.Grid = GxClassificator;
      item.InputField = GxClassificator;

      page.Tag = TxIdProperty;
      TxIdProperty.Tag = GxClassificator;
      page.Controls.Add(GxClassificator);

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);


      BottomPanel.Visible = false;

      TopPanelAndClearButton.Item1.Controls.Add(TxIdProperty);

      TxIdProperty.AutoSize = false;
      TxIdProperty.Location = new Point(101, 5);
      TxIdProperty.MaxLength = 1000;
      TxIdProperty.Name = "TxValue";
      TxIdProperty.ReadOnly = true;
      TxIdProperty.Size = new Size(248, 24);
      TxIdProperty.TabIndex = 0;
      TxIdProperty.Visible = false;
      TxIdProperty.WordWrap = false;

      TableViewDefinition tableViewDefinition1 = new TableViewDefinition();

      GxClassificator.AutoScroll = true;
      GxClassificator.Dock = DockStyle.Fill;
      GxClassificator.EnableAnalytics = false;
      GxClassificator.EnableGestures = false;
      //GxClassificator.Font = Standard.MainFont;
      GxClassificator.Location = new Point(0, 35);
      GxClassificator.MasterTemplate.EnableGrouping = false;
      GxClassificator.MasterTemplate.ViewDefinition = tableViewDefinition1;
      GxClassificator.Name = "GxClassificator";
      GxClassificator.ReadOnly = false;
      GxClassificator.ShowGroupPanel = false;
      GxClassificator.ShowGroupPanelScrollbars = false;
      GxClassificator.ShowNoDataText = false;
      GxClassificator.Size = new Size(465, 407);
      GxClassificator.TabIndex = 3;
      GxClassificator.MasterView.TableSearchRow.ShowCloseButton = false;
      GxClassificator.MasterView.TableSearchRow.ShowClearButton = true;
      //GxClassificator.TableElement.TableHeaderHeight = 20;
      GxClassificator.MasterView.TableSearchRow.AutomaticallySelectFirstResult = true;

      EventClearValue(false); // NOTE: Важная строка. При первом входе в поле без этой строки будет рассинхронизация значений //

      TopPanelAndClearButton.Item2.Click += (s, e) => EventClearValue(true);
      GxClassificator.CurrentRowChanged += EventCurrentRowChangedLocalMethod;
      GxClassificator.KeyUp += EventKeyUp;

      void EventKeyUp(object sender, KeyEventArgs e) // If user presses the Enter key then the Main Grid changes current row to the next one //
      {
        if (e.KeyCode == Keys.Enter) GotoNextRow();
      }

      void EventClearValue(bool ClearIdProperty)
      {
        TxIdProperty.Clear();
        item.DisplayValue = string.Empty;
        if (ClearIdProperty) item.IdProperty = string.Empty;
        GxClassificator.ZzGridClearSelection();
        ShowChangedText(TxIdProperty.Text, true);
      }

      void EventCurrentRowChangedLocalMethod(object sender, CurrentRowChangedEventArgs e)
      {
        if ((CurrentProperty == null) || (CurrentProperty != item)) return;
        if (CurrentProperty.Grid != GxClassificator) return; // Если вдруг текущая строка не соответсвтует гриду справа, то не должгно быть влияние на значение //
        if (e.CurrentRow is GridViewDataRowInfo)
        {
          GridViewDataRowInfo row = e.CurrentRow as GridViewDataRowInfo;
          item.IdProperty = config.GetIdObject(row);
          TxIdProperty.Text = item.IdProperty;
          ShowChangedText(config.GetNameObject(row), true);
        }
      }
    }

    private void AddControlForClassificator(RadPageViewPage page, CardProperty item) // Формируем grid-справочник на основе объекта типа BindingList<HierarchyClassifier> //
    {
      if (item.DataSource == null) return;
      if (item.DataSource is GridConfig)
      {
        AddControlForClassificator(item.DataSource as GridConfig, page, item);
        return;
      }

      RadGridView GxClassificator = item.Grid; //= new RadGridView();
      RadTextBox TxValue = new RadTextBox();


      item.InputField = GxClassificator;    
      page.Tag = TxValue;

      TxValue.Tag = GxClassificator;
      page.Controls.Add(GxClassificator);

      var BottomPanel = CreateBottomPanel(page);
      var TopPanelAndClearButton = CreateTopPanel(page, true);


      // --------------------------------------------------------------------------------------------------------------------
      RadButton BxExpand = new RadButton();
      RadButton BxCollapse = new RadButton();
      RadButton BxClose = TopPanelAndClearButton.Item2;

      BxExpand.Text = "+";
      BxCollapse.Text = "-";

      BxExpand.Font =  new Font( GxClassificator.Font.FontFamily, GxClassificator.Font.Size + 4);
      BxCollapse.Font = BxExpand.Font;

      BxExpand.Size = BxClose.Size;
      BxCollapse.Size = BxClose.Size;

      TopPanelAndClearButton.Item1.Controls.Add(BxExpand);
      TopPanelAndClearButton.Item1.Controls.Add(BxCollapse);

      BxCollapse.Location = new Point(BxClose.Location.X - BxClose.Width - 30, BxClose.Location.Y);
      BxExpand.Location = new Point(BxClose.Location.X - 2*BxClose.Width - 30 - 10, BxClose.Location.Y);

      BxExpand.Anchor = AnchorStyles.Right;
      BxCollapse.Anchor = AnchorStyles.Right;
      // --------------------------------------------------------------------------------------------------------------------


      BottomPanel.Visible = false;

      TopPanelAndClearButton.Item1.Controls.Add(TxValue);

      TxValue.AutoSize = false;
      TxValue.Location = new Point(101, 5);
      TxValue.MaxLength = 1000;
      TxValue.Name = "TxValue";
      TxValue.ReadOnly = true;
      TxValue.Size = new Size(248, 24);
      TxValue.TabIndex = 0;
      TxValue.Visible = false;
      TxValue.WordWrap = false;

      //((ISupportInitialize)(GxClassificator)).BeginInit();
      //((ISupportInitialize)(GxClassificator.MasterTemplate)).BeginInit();

      GridViewTextBoxColumn cmnIdObject = new GridViewTextBoxColumn();
      GridViewTextBoxColumn cmdNameObject = new GridViewTextBoxColumn();
      GridViewTextBoxColumn cmdIdParent = new GridViewTextBoxColumn();

      GridViewTextBoxColumn cmdNameShort = new GridViewTextBoxColumn();
      GridViewTextBoxColumn cmnNoteObject = new GridViewTextBoxColumn();
      GridViewTextBoxColumn cmdCodeObject = new GridViewTextBoxColumn();

      TableViewDefinition tableViewDefinition1 = new TableViewDefinition();

      GxClassificator.AutoScroll = true;
      GxClassificator.Dock = DockStyle.Fill;
      GxClassificator.EnableAnalytics = false;
      GxClassificator.EnableGestures = false;
      GxClassificator.Font = Standard.MainFont;
      GxClassificator.Location = new Point(0, 35);

      GxClassificator.MasterTemplate.AllowAddNewRow = false;
      GxClassificator.MasterTemplate.AllowCellContextMenu = false;
      GxClassificator.MasterTemplate.AllowColumnChooser = false;
      GxClassificator.MasterTemplate.AllowColumnHeaderContextMenu = false;
      GxClassificator.MasterTemplate.AllowColumnReorder = false;
      GxClassificator.MasterTemplate.AllowDeleteRow = false;
      GxClassificator.MasterTemplate.AllowDragToGroup = false;
      GxClassificator.MasterTemplate.AllowEditRow = false;
      GxClassificator.MasterTemplate.AllowRowHeaderContextMenu = false;
      GxClassificator.MasterTemplate.AllowRowReorder = true;
      GxClassificator.MasterTemplate.AllowRowResize = false;
      GxClassificator.MasterTemplate.AllowSearchRow = true;
      GxClassificator.MasterTemplate.AutoGenerateColumns = false;
      GxClassificator.MasterTemplate.ClipboardPasteMode = GridViewClipboardPasteMode.EnableWithNotifications;
      GxClassificator.MasterTemplate.ShowColumnHeaders = true;
      GxClassificator.MasterTemplate.ShowFilteringRow = true;
      GxClassificator.MasterTemplate.EnableFiltering = true;
      //GxClassificator.MasterTemplate.ShowColumnHeaders = false;

      cmnIdObject.AllowHide = false;
      //cmnIdObject.HeaderText = "id"; //  string.Empty;
      cmnIdObject.MaxLength = 500;
      cmnIdObject.Name = "CnIdObject";
      cmnIdObject.Width = 120;
      cmnIdObject.FieldName = nameof(HierarchicalClassifier.IdObject); // "Item1"; // Tuple Item1

      //cmdNameObject.HeaderText = "name"; // string.Empty; // CurrentProperty.PropertyName;
      cmdNameObject.MaxLength = 4000;
      cmdNameObject.Name = "CnNameObject";
      cmdNameObject.ReadOnly = true;
      cmdNameObject.Width = 600;
      cmdNameObject.FieldName = nameof(HierarchicalClassifier.NameObject); // "Item2"; // Tuple Item2

      cmdIdParent.HeaderText = string.Empty; 
      cmdIdParent.MaxLength = 100;
      cmdIdParent.Name = "CnIdParent";
      cmdIdParent.ReadOnly = true;
      cmdIdParent.Width = 0;
      cmdIdParent.FieldName = nameof(HierarchicalClassifier.IdParent);
      cmdIdParent.IsVisible = false;

      //cmdNameShort.HeaderText = "short name"; // string.Empty; 
      cmdNameShort.MaxLength = 1000;
      cmdNameShort.Name = "CnNameShort";
      cmdNameShort.ReadOnly = true;
      cmdNameShort.Width = 300;
      cmdNameShort.FieldName = nameof(HierarchicalClassifier.NameShort);

      //cmnNoteObject.HeaderText = "note"; // string.Empty;
      cmnNoteObject.MaxLength = 1000;
      cmnNoteObject.Name = "CnNoteObject";
      cmnNoteObject.ReadOnly = true;
      cmnNoteObject.Width = 500;
      cmnNoteObject.FieldName = nameof(HierarchicalClassifier.NoteObject);

      //cmdCodeObject.HeaderText = "code"; // string.Empty;
      cmdCodeObject.MaxLength = 1000;
      cmdCodeObject.Name = "CnCodeObject";
      cmdCodeObject.ReadOnly = true;
      cmdCodeObject.Width = 300;
      cmdCodeObject.FieldName = nameof(HierarchicalClassifier.CodeObject);

      if (item.ByteArray != null)
      {
        GxClassificator.Relations.AddSelfReference(GxClassificator.MasterTemplate, cmnIdObject.Name, cmdIdParent.Name);
        GxClassificator.MasterTemplate.SelfReferenceExpanderColumn = cmdNameObject;
      }

      GxClassificator.MasterTemplate.Columns.AddRange(new GridViewDataColumn[] { cmnIdObject, cmdNameObject, cmdIdParent, cmdNameShort, cmnNoteObject, cmdCodeObject });
      GxClassificator.MasterTemplate.EnableGrouping = false;

      GxClassificator.MasterTemplate.ViewDefinition = tableViewDefinition1;
      GxClassificator.Name = "GxClassificator";
      GxClassificator.ReadOnly = false;
      GxClassificator.ShowGroupPanel = false;
      GxClassificator.ShowGroupPanelScrollbars = false;
      GxClassificator.ShowNoDataText = false;
      GxClassificator.Size = new Size(465, 407);
      GxClassificator.TabIndex = 3;

      GxClassificator.MasterView.TableSearchRow.ShowCloseButton = false;
      GxClassificator.MasterView.TableSearchRow.ShowClearButton = true;
      GxClassificator.TableElement.TableHeaderHeight = 20;

      //((ISupportInitialize)(GxClassificator.MasterTemplate)).EndInit();
      //((ISupportInitialize)(GxClassificator)).EndInit();

      GxClassificator.MasterView.TableSearchRow.AutomaticallySelectFirstResult = true;





      GxClassificator.BeginUpdate();

      GxClassificator.DataSource = item.DataSource;

      if (item.ByteArray != null) // Поддерживается иерархия справочника //
      {
        //GxClassificator.MasterTemplate.ExpandAll();
        foreach (var row in GxClassificator.Rows)
          if ((row.IsExpanded == false) && (row.HierarchyLevel < 2))
            row.IsExpanded = true;
      }

      GxClassificator.EndUpdate();







      EventClearValue(false); // NOTE: Важная строка. При первом входе в поле без этой строки будет рассинхронизация значений //

      TopPanelAndClearButton.Item2.Click += (s, e) => EventClearValue(true);
      GxClassificator.CurrentRowChanged += EventCurrentRowChangedLocalMethod;
      GxClassificator.KeyUp += EventKeyUp;

      if (item.ByteArray != null) // Поддерживается иерархия справочника //
      {
        BxExpand.Click += EventExpandAllRows;
        BxCollapse.Click += EventCollapseAllRows;
      }
      else
      {
        BxCollapse.Visible = false;
        BxExpand.Visible = false;
      }

      async void EventExpandAllRows(object sender, EventArgs e)
      {
        BxExpand.Enabled = false;
        foreach (var row in GxClassificator.Rows) if (row.IsExpanded == false) row.IsExpanded = true;
        await Task.Delay(1000);
        BxExpand.Enabled = true;
      }

      async void EventCollapseAllRows(object sender, EventArgs e)
      {
        BxCollapse.Enabled = false;
        foreach (var row in GxClassificator.Rows) if (row.IsExpanded) row.IsExpanded = false;
        await Task.Delay(1000);
        BxCollapse.Enabled = true;
      }


      void EventKeyUp(object sender, KeyEventArgs e) // If user presses the Enter key then the Main Grid changes current row to the next one //
      {
        if (e.KeyCode == Keys.Enter) GotoNextRow();
      }

      void EventClearValue(bool ClearIdProperty)
      {       
        TxValue.Clear();
        item.DisplayValue = string.Empty;
        if (ClearIdProperty) item.IdProperty = string.Empty;
        GxClassificator.ZzGridClearSelection();
        ShowChangedText(TxValue.Text, true);
      }

      void EventCurrentRowChangedLocalMethod(object sender, CurrentRowChangedEventArgs e)
      {
        if ((CurrentProperty == null) || (CurrentProperty != item)) return;
        if (CurrentProperty.Grid != GxClassificator) return;  // Если вдруг текущая строка не соответсвтует гриду справа, то не должгно быть влияние на значение //
        if (e.CurrentRow is GridViewDataRowInfo)
        {
          GridViewDataRowInfo row = e.CurrentRow as GridViewDataRowInfo;
          item.IdProperty = row.Cells[0].Value.ToString();
          TxValue.Text = item.IdProperty;
          ShowChangedText(row.Cells[1].Value.ToString(), true);
        }
      }
    }
  }
}
