﻿namespace TJCard
{
    partial class FxCardMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FxCardMain));
      this.PnGrid = new Telerik.WinControls.UI.RadPanel();
      this.PnGridCenter = new Telerik.WinControls.UI.RadPanel();
      this.GxCard = new Telerik.WinControls.UI.RadGridView();
      this.PnGridTop = new Telerik.WinControls.UI.RadPanel();
      this.SeparatorFour = new Telerik.WinControls.UI.RadSeparator();
      this.BxSearch = new Telerik.WinControls.UI.RadButton();
      this.SeparatorThree = new Telerik.WinControls.UI.RadSeparator();
      this.BxClearAllFields = new Telerik.WinControls.UI.RadButton();
      this.SeparatorTwo = new Telerik.WinControls.UI.RadSeparator();
      this.BxDownload = new Telerik.WinControls.UI.RadButton();
      this.SeparatorOne = new Telerik.WinControls.UI.RadSeparator();
      this.BxUpload = new Telerik.WinControls.UI.RadButton();
      this.PnGridBottom = new Telerik.WinControls.UI.RadPanel();
      this.ButtonImageRfid = new Telerik.WinControls.UI.RadButton();
      this.ButtonImageBarcode = new Telerik.WinControls.UI.RadButton();
      this.SpMain = new System.Windows.Forms.Splitter();
      this.PnCard = new Telerik.WinControls.UI.RadPanel();
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.PgStart = new Telerik.WinControls.UI.RadPageViewPage();
      ((System.ComponentModel.ISupportInitialize)(this.PnGrid)).BeginInit();
      this.PnGrid.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridCenter)).BeginInit();
      this.PnGridCenter.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.GxCard)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxCard.MasterTemplate)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridTop)).BeginInit();
      this.PnGridTop.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorFour)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxSearch)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorThree)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxClearAllFields)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorTwo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorOne)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxUpload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridBottom)).BeginInit();
      this.PnGridBottom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ButtonImageRfid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ButtonImageBarcode)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnCard)).BeginInit();
      this.PnCard.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PnGrid
      // 
      this.PnGrid.Controls.Add(this.PnGridCenter);
      this.PnGrid.Controls.Add(this.PnGridTop);
      this.PnGrid.Controls.Add(this.PnGridBottom);
      this.PnGrid.Dock = System.Windows.Forms.DockStyle.Left;
      this.PnGrid.Location = new System.Drawing.Point(0, 0);
      this.PnGrid.Name = "PnGrid";
      this.PnGrid.Size = new System.Drawing.Size(700, 470);
      this.PnGrid.TabIndex = 1;
      // 
      // PnGridCenter
      // 
      this.PnGridCenter.Controls.Add(this.GxCard);
      this.PnGridCenter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PnGridCenter.Location = new System.Drawing.Point(0, 36);
      this.PnGridCenter.Name = "PnGridCenter";
      this.PnGridCenter.Size = new System.Drawing.Size(700, 398);
      this.PnGridCenter.TabIndex = 2;
      // 
      // GxCard
      // 
      this.GxCard.Dock = System.Windows.Forms.DockStyle.Fill;
      this.GxCard.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.GxCard.Location = new System.Drawing.Point(0, 0);
      // 
      // 
      // 
      this.GxCard.MasterTemplate.AllowAddNewRow = false;
      this.GxCard.MasterTemplate.AllowColumnReorder = false;
      this.GxCard.MasterTemplate.AllowDeleteRow = false;
      this.GxCard.MasterTemplate.AllowEditRow = false;
      this.GxCard.MasterTemplate.AllowRowResize = false;
      gridViewTextBoxColumn1.FieldName = "IdRow";
      gridViewTextBoxColumn1.HeaderText = "CnIdRow";
      gridViewTextBoxColumn1.IsVisible = false;
      gridViewTextBoxColumn1.MaxLength = 50;
      gridViewTextBoxColumn1.Name = "CnIdRow";
      gridViewTextBoxColumn1.ReadOnly = true;
      gridViewTextBoxColumn2.FieldName = "IdParent";
      gridViewTextBoxColumn2.HeaderText = "CnIdParent";
      gridViewTextBoxColumn2.IsVisible = false;
      gridViewTextBoxColumn2.MaxLength = 50;
      gridViewTextBoxColumn2.Name = "CnIdParent";
      gridViewTextBoxColumn2.ReadOnly = true;
      gridViewTextBoxColumn3.FieldName = "IdProperty";
      gridViewTextBoxColumn3.HeaderText = "CnIdProperty";
      gridViewTextBoxColumn3.IsVisible = false;
      gridViewTextBoxColumn3.MaxLength = 250;
      gridViewTextBoxColumn3.Name = "CnIdProperty";
      gridViewTextBoxColumn3.ReadOnly = true;
      gridViewTextBoxColumn4.FieldName = "PropertyName";
      gridViewTextBoxColumn4.HeaderText = "CnName";
      gridViewTextBoxColumn4.MaxLength = 200;
      gridViewTextBoxColumn4.Name = "CnName";
      gridViewTextBoxColumn4.ReadOnly = true;
      gridViewTextBoxColumn4.Width = 200;
      gridViewTextBoxColumn5.FieldName = "DisplayValue";
      gridViewTextBoxColumn5.HeaderText = "CnValue";
      gridViewTextBoxColumn5.Name = "CnValue";
      gridViewTextBoxColumn5.ReadOnly = true;
      gridViewTextBoxColumn5.Width = 250;
      gridViewCheckBoxColumn1.FieldName = "ReadOnly";
      gridViewCheckBoxColumn1.HeaderText = "CnReadOnly";
      gridViewCheckBoxColumn1.IsVisible = false;
      gridViewCheckBoxColumn1.Name = "CnReadOnly";
      gridViewTextBoxColumn6.DataType = typeof(int);
      gridViewTextBoxColumn6.FieldName = "Type";
      gridViewTextBoxColumn6.HeaderText = "CnType";
      gridViewTextBoxColumn6.IsVisible = false;
      gridViewTextBoxColumn6.MaxLength = 50;
      gridViewTextBoxColumn6.Name = "CnType";
      gridViewTextBoxColumn7.FieldName = "JsonValue";
      gridViewTextBoxColumn7.HeaderText = "CnJson";
      gridViewTextBoxColumn7.IsVisible = false;
      gridViewTextBoxColumn7.MaxLength = 10000;
      gridViewTextBoxColumn7.Name = "CnJson";
      gridViewTextBoxColumn8.FieldName = "StringValue";
      gridViewTextBoxColumn8.HeaderText = "CnStringValue";
      gridViewTextBoxColumn8.IsVisible = false;
      gridViewTextBoxColumn8.Name = "CnStringValue";
      gridViewTextBoxColumn8.Width = 200;
      this.GxCard.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
      this.GxCard.MasterTemplate.EnableGrouping = false;
      this.GxCard.MasterTemplate.EnableSorting = false;
      this.GxCard.MasterTemplate.ShowColumnHeaders = false;
      this.GxCard.MasterTemplate.ViewDefinition = tableViewDefinition1;
      this.GxCard.Name = "GxCard";
      this.GxCard.NewRowEnterKeyMode = Telerik.WinControls.UI.RadGridViewNewRowEnterKeyMode.None;
      this.GxCard.ReadOnly = true;
      this.GxCard.ShowGroupPanel = false;
      this.GxCard.ShowNoDataText = false;
      this.GxCard.Size = new System.Drawing.Size(700, 398);
      this.GxCard.TabIndex = 0;
      // 
      // PnGridTop
      // 
      this.PnGridTop.Controls.Add(this.SeparatorFour);
      this.PnGridTop.Controls.Add(this.BxSearch);
      this.PnGridTop.Controls.Add(this.SeparatorThree);
      this.PnGridTop.Controls.Add(this.BxClearAllFields);
      this.PnGridTop.Controls.Add(this.SeparatorTwo);
      this.PnGridTop.Controls.Add(this.BxDownload);
      this.PnGridTop.Controls.Add(this.SeparatorOne);
      this.PnGridTop.Controls.Add(this.BxUpload);
      this.PnGridTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnGridTop.Location = new System.Drawing.Point(0, 0);
      this.PnGridTop.Name = "PnGridTop";
      this.PnGridTop.Padding = new System.Windows.Forms.Padding(5, 4, 0, 0);
      this.PnGridTop.Size = new System.Drawing.Size(700, 36);
      this.PnGridTop.TabIndex = 2;
      ((Telerik.WinControls.UI.RadPanelElement)(this.PnGridTop.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(5, 4, 0, 0);
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnGridTop.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      // 
      // SeparatorFour
      // 
      this.SeparatorFour.Dock = System.Windows.Forms.DockStyle.Left;
      this.SeparatorFour.Location = new System.Drawing.Point(634, 4);
      this.SeparatorFour.Name = "SeparatorFour";
      this.SeparatorFour.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.SeparatorFour.Size = new System.Drawing.Size(20, 32);
      this.SeparatorFour.TabIndex = 6;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorFour.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorFour.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorFour.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxSearch
      // 
      this.BxSearch.AutoSize = true;
      this.BxSearch.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxSearch.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxSearch.Image = ((System.Drawing.Image)(resources.GetObject("BxSearch.Image")));
      this.BxSearch.Location = new System.Drawing.Point(527, 4);
      this.BxSearch.Name = "BxSearch";
      this.BxSearch.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxSearch.Size = new System.Drawing.Size(107, 32);
      this.BxSearch.TabIndex = 2;
      this.BxSearch.Text = "     Поиск ";
      this.BxSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.BxSearch.Visible = false;
      // 
      // SeparatorThree
      // 
      this.SeparatorThree.Dock = System.Windows.Forms.DockStyle.Left;
      this.SeparatorThree.Location = new System.Drawing.Point(507, 4);
      this.SeparatorThree.Name = "SeparatorThree";
      this.SeparatorThree.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.SeparatorThree.Size = new System.Drawing.Size(20, 32);
      this.SeparatorThree.TabIndex = 5;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorThree.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorThree.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorThree.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxClearAllFields
      // 
      this.BxClearAllFields.AutoSize = true;
      this.BxClearAllFields.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxClearAllFields.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxClearAllFields.Image = ((System.Drawing.Image)(resources.GetObject("BxClearAllFields.Image")));
      this.BxClearAllFields.Location = new System.Drawing.Point(323, 4);
      this.BxClearAllFields.Name = "BxClearAllFields";
      this.BxClearAllFields.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxClearAllFields.Size = new System.Drawing.Size(184, 32);
      this.BxClearAllFields.TabIndex = 0;
      this.BxClearAllFields.Text = "     Очистить все поля";
      this.BxClearAllFields.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // SeparatorTwo
      // 
      this.SeparatorTwo.Dock = System.Windows.Forms.DockStyle.Left;
      this.SeparatorTwo.Location = new System.Drawing.Point(303, 4);
      this.SeparatorTwo.Name = "SeparatorTwo";
      this.SeparatorTwo.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.SeparatorTwo.Size = new System.Drawing.Size(20, 32);
      this.SeparatorTwo.TabIndex = 3;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorTwo.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorTwo.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorTwo.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxDownload
      // 
      this.BxDownload.AutoSize = true;
      this.BxDownload.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxDownload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDownload.Image = ((System.Drawing.Image)(resources.GetObject("BxDownload.Image")));
      this.BxDownload.Location = new System.Drawing.Point(126, 4);
      this.BxDownload.Name = "BxDownload";
      this.BxDownload.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxDownload.Size = new System.Drawing.Size(177, 32);
      this.BxDownload.TabIndex = 0;
      this.BxDownload.Text = "     Получить данные";
      this.BxDownload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // SeparatorOne
      // 
      this.SeparatorOne.Dock = System.Windows.Forms.DockStyle.Left;
      this.SeparatorOne.Location = new System.Drawing.Point(106, 4);
      this.SeparatorOne.Name = "SeparatorOne";
      this.SeparatorOne.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.SeparatorOne.Size = new System.Drawing.Size(20, 32);
      this.SeparatorOne.TabIndex = 4;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorOne.GetChildAt(0))).Orientation = System.Windows.Forms.Orientation.Vertical;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorOne.GetChildAt(0))).ShowShadow = true;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorOne.GetChildAt(0))).ShowHorizontalLine = false;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorOne.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
      ((Telerik.WinControls.UI.SeparatorElement)(this.SeparatorOne.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxUpload
      // 
      this.BxUpload.AutoSize = true;
      this.BxUpload.Dock = System.Windows.Forms.DockStyle.Left;
      this.BxUpload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxUpload.Image = ((System.Drawing.Image)(resources.GetObject("BxUpload.Image")));
      this.BxUpload.Location = new System.Drawing.Point(5, 4);
      this.BxUpload.Name = "BxUpload";
      this.BxUpload.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
      this.BxUpload.Size = new System.Drawing.Size(101, 32);
      this.BxUpload.TabIndex = 1;
      this.BxUpload.Text = "Записать";
      this.BxUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      // 
      // PnGridBottom
      // 
      this.PnGridBottom.Controls.Add(this.ButtonImageRfid);
      this.PnGridBottom.Controls.Add(this.ButtonImageBarcode);
      this.PnGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnGridBottom.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnGridBottom.Location = new System.Drawing.Point(0, 434);
      this.PnGridBottom.Name = "PnGridBottom";
      this.PnGridBottom.Size = new System.Drawing.Size(700, 36);
      this.PnGridBottom.TabIndex = 2;
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnGridBottom.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
      // 
      // ButtonImageRfid
      // 
      this.ButtonImageRfid.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.ButtonImageRfid.Image = ((System.Drawing.Image)(resources.GetObject("ButtonImageRfid.Image")));
      this.ButtonImageRfid.Location = new System.Drawing.Point(87, 6);
      this.ButtonImageRfid.Name = "ButtonImageRfid";
      this.ButtonImageRfid.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
      this.ButtonImageRfid.Size = new System.Drawing.Size(50, 25);
      this.ButtonImageRfid.TabIndex = 0;
      this.ButtonImageRfid.Visible = false;
      // 
      // ButtonImageBarcode
      // 
      this.ButtonImageBarcode.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.ButtonImageBarcode.Image = ((System.Drawing.Image)(resources.GetObject("ButtonImageBarcode.Image")));
      this.ButtonImageBarcode.Location = new System.Drawing.Point(12, 6);
      this.ButtonImageBarcode.Name = "ButtonImageBarcode";
      this.ButtonImageBarcode.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
      this.ButtonImageBarcode.Size = new System.Drawing.Size(50, 25);
      this.ButtonImageBarcode.TabIndex = 0;
      this.ButtonImageBarcode.Visible = false;
      // 
      // SpMain
      // 
      this.SpMain.Location = new System.Drawing.Point(700, 0);
      this.SpMain.MinExtra = 15;
      this.SpMain.MinSize = 15;
      this.SpMain.Name = "SpMain";
      this.SpMain.Size = new System.Drawing.Size(5, 470);
      this.SpMain.TabIndex = 2;
      this.SpMain.TabStop = false;
      // 
      // PnCard
      // 
      this.PnCard.Controls.Add(this.PvMain);
      this.PnCard.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PnCard.Location = new System.Drawing.Point(705, 0);
      this.PnCard.Name = "PnCard";
      this.PnCard.Size = new System.Drawing.Size(487, 470);
      this.PnCard.TabIndex = 3;
      // 
      // PvMain
      // 
      this.PvMain.Controls.Add(this.PgStart);
      this.PvMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PvMain.Location = new System.Drawing.Point(0, 0);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.PgStart;
      this.PvMain.Size = new System.Drawing.Size(487, 470);
      this.PvMain.TabIndex = 0;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PvMain.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
      // 
      // PgStart
      // 
      this.PgStart.ItemSize = new System.Drawing.SizeF(40F, 28F);
      this.PgStart.Location = new System.Drawing.Point(10, 37);
      this.PgStart.Name = "PgStart";
      this.PgStart.Size = new System.Drawing.Size(466, 422);
      this.PgStart.Text = "Start";
      // 
      // FxCardMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1192, 470);
      this.Controls.Add(this.PnCard);
      this.Controls.Add(this.SpMain);
      this.Controls.Add(this.PnGrid);
      this.Name = "FxCardMain";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "";
      ((System.ComponentModel.ISupportInitialize)(this.PnGrid)).EndInit();
      this.PnGrid.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnGridCenter)).EndInit();
      this.PnGridCenter.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.GxCard.MasterTemplate)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxCard)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridTop)).EndInit();
      this.PnGridTop.ResumeLayout(false);
      this.PnGridTop.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorFour)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxSearch)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorThree)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxClearAllFields)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorTwo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.SeparatorOne)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxUpload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnGridBottom)).EndInit();
      this.PnGridBottom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.ButtonImageRfid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ButtonImageBarcode)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnCard)).EndInit();
      this.PnCard.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPanel PnGrid;
    public Telerik.WinControls.UI.RadPanel PnGridCenter;
    public Telerik.WinControls.UI.RadGridView GxCard;
    public Telerik.WinControls.UI.RadPanel PnGridTop;
    public Telerik.WinControls.UI.RadButton BxClearAllFields;
    public Telerik.WinControls.UI.RadButton BxDownload;
    public Telerik.WinControls.UI.RadButton BxUpload;
    public Telerik.WinControls.UI.RadPanel PnGridBottom;
    internal Telerik.WinControls.UI.RadButton ButtonImageRfid;
    internal Telerik.WinControls.UI.RadButton ButtonImageBarcode;
    public System.Windows.Forms.Splitter SpMain;
    public Telerik.WinControls.UI.RadPanel PnCard;
    public Telerik.WinControls.UI.RadPageView PvMain;
    public Telerik.WinControls.UI.RadPageViewPage PgStart;
    public Telerik.WinControls.UI.RadButton BxSearch;
    public Telerik.WinControls.UI.RadSeparator SeparatorThree;
    public Telerik.WinControls.UI.RadSeparator SeparatorTwo;
    public Telerik.WinControls.UI.RadSeparator SeparatorOne;
    public Telerik.WinControls.UI.RadSeparator SeparatorFour;
  }
}
