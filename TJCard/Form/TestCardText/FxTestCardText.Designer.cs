﻿namespace TJCard
{
    partial class FxTestCardText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.PnMain = new System.Windows.Forms.Panel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
      this.TxMessage = new Telerik.WinControls.UI.RadTextBoxControl();
      this.BxCardFilesCreate = new Telerik.WinControls.UI.RadButton();
      this.BxExit = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
      this.radPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxCardFilesCreate)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxExit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PnMain
      // 
      this.PnMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnMain.Location = new System.Drawing.Point(0, 334);
      this.PnMain.Name = "PnMain";
      this.PnMain.Size = new System.Drawing.Size(1274, 490);
      this.PnMain.TabIndex = 3;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 323);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(1274, 11);
      this.splitter1.TabIndex = 5;
      this.splitter1.TabStop = false;
      // 
      // radPanel1
      // 
      this.radPanel1.Controls.Add(this.TxMessage);
      this.radPanel1.Controls.Add(this.BxCardFilesCreate);
      this.radPanel1.Controls.Add(this.BxExit);
      this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.radPanel1.Location = new System.Drawing.Point(0, 0);
      this.radPanel1.Name = "radPanel1";
      this.radPanel1.Size = new System.Drawing.Size(1274, 323);
      this.radPanel1.TabIndex = 6;
      // 
      // TxMessage
      // 
      this.TxMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxMessage.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxMessage.Location = new System.Drawing.Point(12, 74);
      this.TxMessage.MaxLength = 100000;
      this.TxMessage.Multiline = true;
      this.TxMessage.Name = "TxMessage";
      this.TxMessage.ShowClearButton = true;
      this.TxMessage.Size = new System.Drawing.Size(1250, 119);
      this.TxMessage.TabIndex = 2;
      // 
      // BxCardFilesCreate
      // 
      this.BxCardFilesCreate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxCardFilesCreate.Location = new System.Drawing.Point(12, 12);
      this.BxCardFilesCreate.Name = "BxCardFilesCreate";
      this.BxCardFilesCreate.Size = new System.Drawing.Size(172, 40);
      this.BxCardFilesCreate.TabIndex = 1;
      this.BxCardFilesCreate.Text = "Create a form";
      // 
      // BxExit
      // 
      this.BxExit.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxExit.Location = new System.Drawing.Point(393, 12);
      this.BxExit.Name = "BxExit";
      this.BxExit.Size = new System.Drawing.Size(172, 40);
      this.BxExit.TabIndex = 1;
      this.BxExit.Text = "Exit";
      // 
      // FxTestCardText
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1274, 824);
      this.Controls.Add(this.radPanel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.PnMain);
      this.Name = "FxTestCardText";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "FxTestCardText";
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
      this.radPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxCardFilesCreate)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxExit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Panel PnMain;
    private System.Windows.Forms.Splitter splitter1;
    private Telerik.WinControls.UI.RadPanel radPanel1;
    public Telerik.WinControls.UI.RadTextBoxControl TxMessage;
    public Telerik.WinControls.UI.RadButton BxCardFilesCreate;
    public Telerik.WinControls.UI.RadButton BxExit;
  }
}
