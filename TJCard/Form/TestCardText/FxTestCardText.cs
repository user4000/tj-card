﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace TJCard
{
  public partial class FxTestCardText : RadForm
  {
    ServiceCardForm SrvCardForm { get; } = ServiceCardForm.Create();

    FxCardText Card { get; set; } = null;

    public BindingList<HierarchicalClassifier> DsText { get; private set; } = new BindingList<HierarchicalClassifier>();

    public FxTestCardText()
    {
      InitializeComponent();
      SetEvents();
      FillTestData();
    }

    private void FillTestData()
    {
      DsText.Add(new HierarchicalClassifier() { IdObject= "109", NameObject= "Контактная информация: Адрес проживания", IdParent=null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "127", NameObject = "Контактная информация: Юридический адрес", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "128", NameObject = "Личный мобильный телефон", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "129", NameObject = "Служебный мобильный телефон", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "130", NameObject = "Домашний телефон", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "131", NameObject = "Корпоративный адрес EMail", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "229", NameObject = "Личный адрес EMail", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "230", NameObject = "Адрес сайта", IdParent = null });
      DsText.Add(new HierarchicalClassifier() { IdObject = "231", NameObject = "Адрес профиля в системе контроля версий", IdParent = null });
    }

    private void SetEvents()
    {
      BxExit.Click += (s, e) => this.Close();
      BxCardFilesCreate.Click += (s, e) => EventCardTextCreate();
    }

    private void EventCardTextCreate()
    {
      RemoveControls();
      Card = SrvCardForm.AddCardTextForm(PnMain, DsText, "Дополнительные свойства", "test");
      Card.BxDownload.Click += EventDownload;
      Card.BxUpload.Click += (s, e) =>
      {
        Card.DeleteAllDataRows();
      };
    }

    private async void EventDownload(object sender, EventArgs e)
    {
      await Task.Delay(100);
      Card.DeleteAllDataRows();
      Card.SetIdObject(KeyGenerator.GetUniqueKey(10), "Системный блок компьютер HP G600 процессор Inter Core i5 RAM 16 Gb");
    }

    public void Print(string message)
    {
      TxMessage.AppendText(message + Environment.NewLine + Environment.NewLine);
    }

    public void Write(string message)
    {
      TxMessage.AppendText(message + " ");
    }

    public async Task Process()
    {
      for (int i = 1; i < 11; i++)
      {
        TxMessage.AppendText(i.ToString() + ", "); await Task.Delay(50);
      }
    }

    public void RemoveControls()
    {
      for (int i = PnMain.Controls.Count - 1; i >= 0; i--)
      {
        var control = PnMain.Controls[i];
        control.Visible = false;
        PnMain.Controls.RemoveAt(i);
        control.Dispose();
      }
    }

    private async void EventGetFiles(object sender, EventArgs e)
    {
      TxMessage.Clear();
      RadControl control = (sender as RadControl);
      if (control != null) control.Visible = false;
      await Task.Delay(1000);
      Write("Getting FILES ...");
      await Process();
      Write("Done.");
      await Task.Delay(1000);
      if (control != null) control.Visible = true;
    }

    private async void EventSaveFiles(object sender, EventArgs e)
    {
      TxMessage.Clear();
      RadControl control = (sender as RadControl);
      if (control != null) control.Visible = false;
      await Task.Delay(1000);
      Write("SAVING FILES ...");
      await Process();
      Write("Done.");
      await Task.Delay(1000);
      if (control != null) control.Visible = true;
    }
  }
}
