﻿using System;

namespace TJCard
{
  [Serializable]
  public class HierarchicalClassifier
  {
    public string IdObject { get; set; } = string.Empty;

    public string NameObject { get; set; } = string.Empty;

    public string CodeObject { get; set; } = string.Empty;

    public string NameShort { get; set; } = string.Empty;

    public string NoteObject { get; set; } = string.Empty;

    public string IdParent { get; set; } = null;
  }
}


