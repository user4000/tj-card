﻿using System;

namespace TJCard
{
  public class TestEntity
  {
    public int IdPerson { get; set; }
    public long IdUniqueKey { get; set; }
    public decimal MyMoney { get; set; }
    public string ItemName { get; set; }
    public string InventoryNumber { get; set; }
    public string ArticleCode { get; set; }
    public string SerialNumber { get; set; }   
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string MyPassword { get; set; }    
    public bool Gto { get; set; }
    public bool GtaVcGame { get; set; }
    public int IdMeasure { get; set; }
    public int IdSupplier { get; set; }
    public string Barcode { get; set; }
    public string Rfid { get; set; }
  }
}