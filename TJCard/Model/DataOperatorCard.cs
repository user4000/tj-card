﻿using System;
using System.IO;
using System.Linq;
using TJCard.Tools;
using System.Drawing;
using Telerik.WinControls;
using System.Globalization;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;

namespace TJCard
{
  public class DataOperatorCard
  {
    public string IdObject { get; internal set; } = string.Empty; // Применяется только для форм в режиме изменения данных существующего объекта //

    public string NameObject { get; internal set; } = string.Empty; // Применяется только для форм в режиме изменения данных существующего объекта //

    public int MinusOne { get; } = -1;

    public string GroupTextUniqueName { get; } = "group_text";

    public string SwitcherOnCellText { get; set; } = "   V";

    public string SwitcherOffCellText { get; set; } = string.Empty;

    public BindingList<CardProperty> DsList { get; } = new BindingList<CardProperty>();
  
    public BindingList<HierarchicalClassifier> DsText { get; private set; } = new BindingList<HierarchicalClassifier>(); // for CardText only //

    public CardProperty GroupTextProperties { get; private set; } = null;

    public static string FlagDateOnly { get; } = " "; // It must be spaces only //

    public int GetCount(string IdParent) => (from x in DsList where x.IdParent == IdParent select x).Count();

    public bool Exists(string IdRow) => (from x in DsList where x.IdRow == IdRow select x).Count() > 0;

    public CardProperty GetProperty(string IdRow) => DsList.FirstOrDefault(p => p.IdRow == IdRow);

    public int CountByIdProperty(string IdProperty) => DsList.Count(p => p.IdProperty == IdProperty);

    public bool IdObjectIsSet() => !string.IsNullOrWhiteSpace(IdObject);

    public string StrObject { get; set; } = "Изменение объекта:";

    public string StrNoObject { get; set; } = "Объект не выбран. Нажмите кнопку [Получить данные] для выбора объекта";

    public string StrFileNotSaved { get; set; } = "Failed to save the specified file";

    public string StrFileSaved { get; set; } = "File is saved:";

    public string StrFileExists { get; set; } = "The file you specified is already added";

    public string StrTooLarge { get; set; } = "The file you specified is too large";

    public string StrMaxSize { get; set; } = "maximum size allowed(bytes)";

    public string GetTextForSelectedObject()
    {
      return "  " + (IdObjectIsSet() ? $"{StrObject} id = {IdObject};  {NameObject}" : StrNoObject);
    }

    public int GetIdObjectAsInt32(int Default = -1) => CxConvert.ToInt32(IdObject, Default);

    public long GetIdObjectAsInt64(long Default = -1) => CxConvert.ToInt64(IdObject, Default);

    public bool FindDuplicates() => DsList.GroupBy(x => x.IdRow).Where(g => g.Count() > 1).Select(y => y.Key).ToList().Count() > 0;
 
    public string GetTextValueOfClassificator(string idProperty) // for CardText only //
    {
      foreach (var item in DsText) if (item.IdObject == idProperty) return item.NameObject;
      return string.Empty;
    }

    public string GetTextValueOfClassificator(object dataSource, string idProperty)
    {
      if (dataSource is BindingList<HierarchicalClassifier>) return GetTextValueOfHierarchicalClassifier(dataSource as BindingList<HierarchicalClassifier>, idProperty);
      if (dataSource is GridConfig) return GetTextValueOfGridConfig(dataSource as GridConfig, idProperty);
      return string.Empty;
    }

    public string GetTextValueOfHierarchicalClassifier(BindingList<HierarchicalClassifier> list, string idProperty)
    {
      if (list == null) return string.Empty;
      foreach (var item in list) if (item.IdObject == idProperty) return item.NameObject;
      return string.Empty;
    }

    public string GetTextValueOfGridConfig(GridConfig config, string idProperty)
    {
      return config.GetNameObject(idProperty);
    }

    public void Add(PropertyType type, string idProperty, string idRow, string idParent, string name = "", string value = "", bool readOnly = false)
    {      
      DsList.Add
        (
        type, 
        idRow, 
        idParent, 
        idProperty,
        name, 
        value, 
        readOnly
        );
    }

    public CardProperty AddClassificator(BindingList<HierarchicalClassifier> list, bool EnableHierarchy, string idProperty, string idRow, string idParent, string name = "", string value = "", bool readOnly = false)
    {
      CardProperty item = CardProperty.Create(PropertyType.Classificator, idRow, idParent, idProperty, name, value, readOnly);
      item.Grid = new RadGridView();
      item.DataSource = list;
      if (EnableHierarchy) item.ByteArray = new byte[] { 0 };
      DsList.Add(item);
      return item;
    }

    public CardProperty AddClassificator(GridConfig gridConfig, bool EnableHierarchy, string idProperty, string idRow, string idParent, string name = "", string value = "", bool readOnly = false)
    {
      CardProperty item = CardProperty.Create(PropertyType.Classificator, idRow, idParent, idProperty, name, value, readOnly);
      item.Grid = gridConfig.GridClassificator;
      item.DataSource = gridConfig;
      if (EnableHierarchy) item.ByteArray = new byte[] { 0 };
      DsList.Add(item);
      return item;
    }

    public void AddGroup(string idRow, string name, PropertyType type = PropertyType.Unknown, bool readOnly = true)
    {
      DsList.Add(type, idRow, null, string.Empty, name, string.Empty, readOnly);
    }

    public void AddImage(string idProperty, string idRow, string name, string value, string base64, Bitmap bitmap = null)
    {
      CardProperty item = CardProperty.Create(PropertyType.Image, idRow, null, idProperty, name, value, false);
      item.StringValue = base64;
      item.ImageBitmap = bitmap ?? CxConvert.GetBitmapFromBase64(base64); // if file comes from the server then bitmap = null //
      DsList.Add(item);
    }

    public void AddFile(string idProperty, string idRow, string name, string value, string base64)
    {
      CardProperty item = CardProperty.Create(PropertyType.File, idRow, null, idProperty, name, value, false);
      item.StringValue = base64;
      DsList.Add(item);
    }

    public void AddGroupTextProperties(BindingList<HierarchicalClassifier> list, string name)
    {
      if (list.Count < 1) return;
      DsText = list;
      DsList.Add(PropertyType.TypedText, GroupTextUniqueName, null, GroupTextUniqueName, name, string.Empty, true);
      foreach (var item in DsList)
        if ((item.IdRow == GroupTextUniqueName) && (item.Type == PropertyType.TypedText))
        {
          GroupTextProperties = item; break;
        }
    }

    public void AddTypedTextProperty(string idProperty, string idRow, string nameProperty, string value, bool readOnly = false)
    {
      // idRow - уникальный код свойства //
      // idProperty - значение IdObject из справочника свойств A_TEXT //
      DsList.Add(PropertyType.TypedText, idRow, GroupTextUniqueName, idProperty, nameProperty, value, readOnly);
    }

    public bool DeleteProperty(string IdRow)
    {
      bool deleted = false;
      for (int i = DsList.Count - 1; i > MinusOne; i--)
        if (DsList[i].IdRow == IdRow)
        {
          if (DsList[i].ReadOnly) return deleted;
          DsList[i].StringValue = null;
          DsList[i].DisplayValue = null;
          DsList[i].ImageBitmap = null;
          DsList[i].InputField = null;
          DsList.RemoveAt(i);
          deleted = true;
          break;
        }
      return deleted;
    }

    public string GetValueText(string IdRow)
    {
      try
      {
        return GetProperty(IdRow).DisplayValue ?? string.Empty;
      }
      catch
      {
        return string.Empty;
      }
    }

    public string GetValuePassword(string IdRow)
    {
      try
      {
        return GetProperty(IdRow).StringValue ?? string.Empty;
      }
      catch
      {
        return string.Empty;
      }
    }

    public string ConvertBooleanToString(bool value) => value ? SwitcherOnCellText : SwitcherOffCellText;

    public int GetValueInt32(string IdRow)
    {
      try
      {
        return Int32.Parse(GetProperty(IdRow).DisplayValue);
      }
      catch
      {
        return int.MinValue;
      }
    }

    public int? GetValueNullableInt32(string IdRow)
    {
      if (string.IsNullOrWhiteSpace(GetProperty(IdRow).DisplayValue)) return null;
      try
      {
        return Int32.Parse(GetProperty(IdRow).DisplayValue);
      }
      catch
      {
        return null;
      }
    }

    public long GetValueInt64(string IdRow)
    {
      try
      {
        return Int64.Parse(GetProperty(IdRow).DisplayValue);
      }
      catch
      {
        return long.MinValue;
      }
    }

    public long? GetValueNullableInt64(string IdRow)
    {
      if (string.IsNullOrWhiteSpace(GetProperty(IdRow).DisplayValue)) return null;
      try
      {
        return Int64.Parse(GetProperty(IdRow).DisplayValue);
      }
      catch
      {
        return null;
      }
    }

    public decimal GetValueDecimal(string IdRow)
    {
      try
      {
        return decimal.Parse(GetProperty(IdRow).DisplayValue, CultureInfo.InvariantCulture);
      }
      catch
      {       
        return decimal.MinValue;
      }
    }

    public decimal? GetValueNullableDecimal(string IdRow)
    {
      if (string.IsNullOrWhiteSpace(GetProperty(IdRow).DisplayValue)) return null;
      try
      {
        return decimal.Parse(GetProperty(IdRow).DisplayValue, CultureInfo.InvariantCulture);
      }
      catch
      {
        return null;
      }
    }

    public bool GetValueBoolean(string IdRow)
    {
      try
      {
        return GetProperty(IdRow).DisplayValue == SwitcherOnCellText;
      }
      catch
      {
        return false;
      }
    }

    public DateTime GetValueDatetime(string IdRow)
    {
      try
      {
        return DateTime.Parse(GetProperty(IdRow).DisplayValue);
      }
      catch
      {
        return DateTime.MinValue;
      }
    }

    public int GetValueClassificatorAsInt32(string IdRow)
    {
      try
      {
        return Int32.Parse(GetProperty(IdRow).IdProperty);
      }
      catch
      {
        return int.MinValue;
      }
    }

    public string GetValueClassificatorAsText(string IdRow)
    {
      try
      {
        return GetProperty(IdRow).IdProperty ?? string.Empty;
      }
      catch
      {
        return string.Empty;
      }
    }

    internal void SaveBase64ToDisk(string FileName, string base64)
    {
      try
      {
        byte[] array = Convert.FromBase64String(base64);
        File.WriteAllBytes(FileName, array);
        Array.Clear(array, 0, array.Length);
      }
      catch (Exception ex)
      {
        RadMessageBox.Show(ex.Message, StrFileNotSaved, MessageBoxButtons.OK, RadMessageIcon.Error);
        return;
      }
      RadMessageBox.Show($"{StrFileSaved}\r\n\r\n" + Path.GetFileName(FileName), Path.GetDirectoryName(FileName), MessageBoxButtons.OK, RadMessageIcon.Info);
    }

    internal bool ThisFileAlreadyAdded(string IdProperty, string FileName)
    {
      bool FileAlreadyAdded = CountByIdProperty(IdProperty) > 0;
      if (FileAlreadyAdded)
      {
        RadMessageBox.Show(StrFileExists, FileName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
      }
      return FileAlreadyAdded;
    }

    internal bool FileIsTooBig(DialogResult result, string FileName, int MaximumSizeAllowed)
    {
      bool fileIsTooBig = false;
      if (result == DialogResult.OK)
      {
        long length = new FileInfo(FileName).Length;
        if (length > MaximumSizeAllowed)
        {
          RadMessageBox.Show(StrTooLarge, $"{StrMaxSize} = {MaximumSizeAllowed}", MessageBoxButtons.OK, RadMessageIcon.Error);
          fileIsTooBig = true;
        }
      }
      return fileIsTooBig;
    }
  }
}

/*

DEPRECATED METHODS:

public void SaveBitmapToDisk(string FileName, Bitmap bitmap)
{
try
{
  bitmap.Save(FileName);        
}
catch (Exception ex)
{
  RadMessageBox.Show(ex.Message, "Failed to save the specified file", MessageBoxButtons.OK, RadMessageIcon.Error);
  return;
}
RadMessageBox.Show("File is saved:\r\n\r\n" + Path.GetFileName(FileName), Path.GetDirectoryName(FileName), MessageBoxButtons.OK, RadMessageIcon.Info);
}


public void AddImage(string idProperty, string idRow, string name, string value, Bitmap bitmap, string base64)
{
  CardProperty item = CardProperty.Create(PropertyType.Image, idRow, null, idProperty, name, value, false);
  item.StringValue = base64;
  item.ImageBitmap = bitmap;
  DsList.Add(item);
}


public string GetTextValueOfClassificator(BindingList<Tuple<string, string>> list, string idProperty)
{
  if (list == null) return string.Empty;
  foreach (var item in list) if (item.Item1 == idProperty) return item.Item2;
  return string.Empty;
}


private void AddClassificatorOldVersion(BindingList<Tuple<string, string>> list, string idProperty, string idRow, string idParent, string name = "", string value = "", bool readOnly = false)
{
  //CardProperty item = CardProperty.Create(PropertyType.Classificator, idRow, idParent, idProperty, name, value, readOnly);
  //item.DataSource = list;
  //DsList.Add(item);
}


*/
