﻿namespace TJCard
{
  public enum PropertyType
  {
    Unknown = 0,

    Int32 = 1,

    IntNullable32 = 14,

    Int64 = 2,

    IntNullable64 = 15,

    Decimal = 3,

    DecimalNullable = 16,

    Text = 4,

    Password = 5,

    Datetime = 6,

    Boolean = 7,

    Barcode = 8,

    RfidCode = 9,

    Classificator = 10,

    Image = 11,

    File = 12,

    TypedText = 13
  }
}