﻿using System.Drawing;
using System.Diagnostics;
using Telerik.WinControls.UI;
using System.Collections.Generic;

namespace TJCard
{
  public class GridConfig // Данный класс применяется для вывода на экран справочника с произвольным количеством столбцов //
  {
    public RadGridView GridSource { get; set; } = null; // Грид который является источником данных и образцом для копирования //

    public RadGridView GridClassificator { get; private set; } = null; // Грид, скопированный из образца //

    public string IdObjectColumn { get; private set; } = string.Empty; // Имя столбца содержащего идентификатор объекта //

    public List<string> NameObjectColumnList { get; private set; } = new List<string>(); // Список имён столбцов, которые участвуют в формировании названия объекта //

    public int CellNumberForIdObjectColumn { get; private set; } = 0; // Номер столбца содержащего идентификатор объекта //

    public List<int> CellsForNameObjectColumn { get; private set; } = new List<int>(); // Список номеров столбцов, которые участвуют в формировании названия объекта //

    private GridConfig()
    {

    }

    public static GridConfig Create(RadGridView grid, string idObjectColumn, string nameObjectColumn)
    {
      List<string> list = new List<string>(); list.Add(nameObjectColumn); return Create(grid, idObjectColumn, list);
    }

    public static GridConfig Create(RadGridView grid, string idObjectColumn, List<string> nameObjectColumnList)
    {
      GridConfig config = new GridConfig
      {
        GridSource = grid,
        IdObjectColumn = idObjectColumn,
        NameObjectColumnList = nameObjectColumnList
      };
      config.GridClassificator = config.CreateGridForClassificator();
      return config;
    }

    public string GetIdObject(GridViewRowInfo row)
    {
      if (row == null) return string.Empty;
      return row.Cells[CellNumberForIdObjectColumn].Value.ToString();
    }

    public string GetNameObject(GridViewRowInfo row)
    {
      if (row == null) return string.Empty;
      string name = string.Empty;
      foreach (int index in CellsForNameObjectColumn) name += row.Cells[index].Value.ToString() + " ";
      return name.Trim();
    }

    public string GetNameObject(string IdObject)
    {
      foreach (var row in GridSource.Rows) if (GetIdObject(row) == IdObject) return GetNameObject(row);
      return string.Empty;
    }

    private void SetProperties(RadGridView grid)
    {
      grid.MasterView.TableSearchRow.ShowCloseButton = false;
      grid.MasterView.TableSearchRow.ShowClearButton = true;
      grid.TableElement.RowHeight = 25;
      grid.Font = Standard.MainFont;
      MasterGridViewTemplate MGridViewTemplate = grid.MasterTemplate;
      MGridViewTemplate.EnableGrouping = true;
      MGridViewTemplate.AllowAddNewRow = false;
      MGridViewTemplate.AllowDeleteRow = false;
      MGridViewTemplate.AllowRowResize = false;
      MGridViewTemplate.EnableFiltering = true;
      MGridViewTemplate.AutoGenerateColumns = false;
      MGridViewTemplate.AllowCellContextMenu = false;
      MGridViewTemplate.AllowColumnChooser = false;
      MGridViewTemplate.AllowColumnHeaderContextMenu = false;
      MGridViewTemplate.AllowColumnReorder = false;
      MGridViewTemplate.AllowDragToGroup = false;
      MGridViewTemplate.AllowEditRow = false;
      MGridViewTemplate.AllowRowHeaderContextMenu = false;
      MGridViewTemplate.AllowRowReorder = true;
      MGridViewTemplate.AllowSearchRow = true;
      MGridViewTemplate.ClipboardPasteMode = GridViewClipboardPasteMode.EnableWithNotifications;
      MGridViewTemplate.ShowColumnHeaders = true;
      MGridViewTemplate.ShowFilteringRow = true;
    }

    private void CreateTextBoxColumn(RadGridView grid, GridViewTextBoxColumn column)
    {
      GridViewTextBoxColumn cn = new GridViewTextBoxColumn();
      cn.FieldName = column.FieldName;
      cn.DataType = column.DataType;
      cn.Width = column.Width;
      cn.ReadOnly = true;
      cn.Name = column.Name;
      cn.IsVisible = column.IsVisible;
      cn.HeaderText = column.HeaderText;
      grid.MasterTemplate.Columns.Add(cn);

      if (cn.FieldName == IdObjectColumn) CellNumberForIdObjectColumn = cn.Index;
      foreach (var item in NameObjectColumnList) if (item == cn.FieldName) CellsForNameObjectColumn.Add(cn.Index);
    }

    private void CreateComboBoxColumn(RadGridView grid, GridViewComboBoxColumn column)
    {
      GridViewComboBoxColumn cn = new GridViewComboBoxColumn();
      cn.FieldName = column.FieldName;
      cn.DataType = column.DataType;
      cn.Width = column.Width;
      cn.ReadOnly = true;
      cn.Name = column.Name;
      cn.IsVisible = column.IsVisible;
      cn.DataSource = column.DataSource;
      cn.DisplayMember = column.DisplayMember;
      cn.ValueMember = column.ValueMember;
      cn.HeaderText = column.HeaderText;
      grid.MasterTemplate.Columns.Add(cn);

      if (cn.FieldName == IdObjectColumn) CellNumberForIdObjectColumn = cn.Index;
      foreach (var item in NameObjectColumnList) if (item == cn.FieldName) CellsForNameObjectColumn.Add(cn.Index);
    }

    private RadGridView CreateGridForClassificator()
    {
      RadGridView grid = new RadGridView();
      SetProperties(grid);
      foreach (var item in GridSource.Columns)
      {
        if (item is GridViewTextBoxColumn) CreateTextBoxColumn(grid, item as GridViewTextBoxColumn);
        if (item is GridViewComboBoxColumn) CreateComboBoxColumn(grid, item as GridViewComboBoxColumn);       
      }
      grid.DataSource = GridSource.DataSource;
      return grid;
    }
  }
}