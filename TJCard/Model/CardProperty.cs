﻿using System;
using System.Drawing;
using Newtonsoft.Json;
using Telerik.WinControls.UI;

namespace TJCard
{
  [Serializable]
  public class CardProperty
  {
    public PropertyType Type { get; set; } = PropertyType.Unknown;

    public string IdRow { get; set; } = string.Empty;

    public string IdParent { get; set; } = null;

    public string IdProperty { get; set; } = string.Empty;

    public string PropertyName { get; set; } = string.Empty;

    public string DisplayValue { get; set; } = string.Empty;

    public string StringValue { get; set; } = string.Empty;

    [JsonIgnore]
    public byte[] ByteArray { get; set; } = null; // for [Image] or [File] types only //

    [JsonIgnore]
    public Bitmap ImageBitmap { get; set; } = null; // for [Image] type only //

    [JsonIgnore]
    public object InputField { get; set; } = null;

    [JsonIgnore]
    public RadGridView Grid { get; set; } = null; // Если это классификатор, то здесь будет ссылка на Grid //


    /*
    Если значение ReadOnly = true у группировочной строки, то это означает, что нельзя добавлять и удалять элементы из этой группы.
    Если значение ReadOnly = true у обычной строки, то это означает, что нельзя изменять значение данного свойства.
    */
    public bool ReadOnly { get; set; } = false;

    //public BindingList<Tuple<string, string>> DataSource { get; set; } = null; // for [Classificator] type only //

    //public BindingList<HierarchicalClassifier> DataSource { get; set; } = null; // for [Classificator] type only //

    public object DataSource { get; set; } = null; // for [Classificator] type only //

    private CardProperty(PropertyType type, string idRow, string idParent, string idProperty, string name, string value, bool readOnly)
    {
      if (string.IsNullOrWhiteSpace(idRow)) throw new Exception("CardProperty constructor reports error! Empty [IdRow] is not allowed.");
      IdRow = idRow;
      IdParent = idParent;
      IdProperty = idProperty;
      PropertyName = name;
      DisplayValue = value;
      ReadOnly = readOnly;
      Type = type;
    }

    public static CardProperty Create(PropertyType type, string idRow, string idParent, string idProperty, string name, string value, bool readOnly)
    {
      idRow = string.IsNullOrWhiteSpace(idRow) ? KeyGenerator.GetNewIdRow() : idRow;
      idProperty = string.IsNullOrWhiteSpace(idProperty) ? idRow : idProperty;

      CardProperty property = new CardProperty(type, idRow, idParent, idProperty, name, value, readOnly);
      if ((type == PropertyType.Datetime) && (value == DataOperatorCard.FlagDateOnly))
        property.ByteArray = new byte[] { 0 }; // NOTE: [Date only] attribute //

      return property;
    }
  }
}