﻿using System.Drawing;
using TJCard.Tools;

namespace TJCard
{
  public class PictureDepot
  {
    public static Image ImgClear { get; private set; } = null;

    public static Image ImgPasswordYellowKey { get; private set; } = null;

    public static Image ImgDownload { get; private set; } = null;

    public static Image ImgAdd { get; private set; } = null;

    public static Image GetImageAdd()
    {
      if (ImgAdd == null) { ImgAdd = CxConvert.GetImageFromBase64(Add()); }
      return ImgAdd;
    }

    public static Image GetImageClear()
    {
      if (ImgClear==null) { ImgClear = CxConvert.GetImageFromBase64(Clear()); }  return ImgClear;
    }

    public static Image GetImageDownload()
    {
      if (ImgDownload == null) { ImgDownload = CxConvert.GetImageFromBase64(Download()); }
      return ImgDownload;
    }

    public static Image GetImagePasswordYellowKey()
    {
      if (ImgPasswordYellowKey == null) { ImgPasswordYellowKey = CxConvert.GetImageFromBase64(PasswordYellowKey()); }
      return ImgPasswordYellowKey;
    }

    private static string Add()
    {
      return @"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABiElEQVQ4jXVTTUvjUBQ9J304bU0nuFQQPxhmI/QnzMLNgItW8K/4R/whCtadW9cKLgUZpwidWY22xibY9h4XSdqQvMklm5dzzj3nvhvC83T2Q/68+nHu2q4PAIt0cXl9fHMyfpioig18AuvbrZBr7EmiJMKhF+60Q7COdbWTDBQIgsEIAAIgyNusLpCBYTIEOcdUc14RKFtj9hYihaAo1CIIcJ1v6wz32iGKeRCIDr5GBpFQgeNGN4ogASsz9j6cxjy6P7xorLEHcmWGReWHkpDXki/BZhq4oIm+AJJaDgwl0DJYVuVP4hf0XTYgVZg5i1UdzxDT549Lt9HosXxJBNkiKeauJCV5jMKBAYvXxYCtrSZbm80QXC1V1O1E3093nxCwuEd7PBvuvdxOxqsMsPRvGrtklCoZpW+l7mCbMJNIFd00+RWP/929jmGVCN5gynYgnyskAQbvnP6ziagJyMf2CgjQXGZmYJCxZIDmMJ+G9wdJfqfxx2g+sHfJptLsz3wwfUpiH/YT2oDQ53Eu/NQAAAAASUVORK5CYII=";
    }

    private static string Clear()
    {
      return 
        @"iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8" +
        @"YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABZSURBVChTjY7RCQAxCEO7k2u4oNO4wO2UI0KLphzcxxNJ" +
        @"HuLCg1/UcHcQLXtegZkhM4fMnRm7I6qs0hC7rBIZIi9FRNHfIJfES0TlS9qFyifs0qbno/gG6wVzofw3" +
        @"zSefxAAAAABJRU5ErkJggg==";
    }

    private static string Download()
    {
      return
        @"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8" +
        @"YQUAAAJLSURBVDhPlZJbSFRRGIVPF2qcRIuudsF8UAghyiiRSAqKwECM9EEqykpSoxx9cZDEUCqzhzAT" +
        @"QatJwiDzElg+JJUVXcgwzUozyRy1SXPmnLk4c85ofu3AXiwdW7DZL3t9+99rbWmydHHtSHuamR3bwrz4" +
        @"VnSJH/E73IXfkU8EHuth4tjU0qd0oc/pI/CijaVFMitKFdaaFGKfuQkpHPYNCDzRxaI8M8svy6y+YmVN" +
        @"iZWISpnc9wJQMAPA4szPBF3oJ7hMIeyajTCTzLZahcJON6FFVt+AIGM3IcUW1t20s6FSIeKWwu57Dq5+" +
        @"UQkvk30DggUg/Pp3NlU5iaq2s7XOwb5GJ3fMGhtvKL4BoXlf2VxpIbrexa4GBzvvO0l66uLRkJfICvvf" +
        @"AP+0fpYZB1h4ykyAQYxv7GBH1RAxD9zsbXQR/9BF2nM3DZZRtt92jc4xDrHqkpVQEe6CPJGJLnmAlWcG" +
        @"iL6rsKV8kMjSHmLqrCQ0udn/ZISjwpzyyoPhjcrpNpXiDxqmPq94jo35+ROZ6I9/Y32ZlYPNKvFNHhKF" +
        @"OUn0niqMmc0est+qGFo0HlvGsP8cJ6HKztyMSY3MT7UQVWEjvVUj+aWHk689ZAlj7juVnDaNmt5R+tVx" +
        @"4kSwUvoUderSB4mplTnb6SVb3JjfLpYYucY8xg8vHKh3IBls0zfhl2XlkAjP1DtGQYeXarEPC3NK4wiS" +
        @"cQb/4Lf0uTYyXrhplccZ1sAgwpRy/lHhdPIvsFPerXFe5DDrnPP/zH+0pNRJQIl495SSpF/ZNZywZDCG" +
        @"VAAAAABJRU5ErkJggg==";
    }

    private static string PasswordYellowKey()
    {
      return
        @"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8" +
        @"YQUAAAYASURBVFhHxZZpTJRXFIbNxJimMaYxhPSHTW3TVKosOspm1Var1h/SKhYMFAQRy7AMm8MsoJgm" +
        @"djPxh23aWC2tRsWmlaogM6CCILLKqrKIFZRFUIsCAsMwzLx97yxRW1sFbD3Jy02++e59n3vuuedjEoDn" +
        @"qsc+/D9l/fMM4qjK00u71TPjVKoncre5o/Bzb8uYu91Tr0v1zvhig/M026uPxjMAmJy91SOj4Kt30V2+" +
        @"B+itBAytwHALdQ0jt8vQVvA18ne8A22q9z6+L7FOs8UEASbnbHPvqv9FAfNAI81LYGzejeFaJfQ1CgxX" +
        @"J8LQuBO4UwBz70XUHozEya1ex8Q863TGBAAkx1QL0up/VQLGFoy27oG+PARDZSEcN0BfFgxDeSD0JX4Y" +
        @"LPwQhsufAoP1qD0UhcwU7zQx37LKeAH2Rs1dUvDlUoA7H72SiuGqCJtkMFRH4KY2AKW7V6CM6shcg6Hi" +
        @"dRiuiID5XiXyP1sK/0WvOFoWGifAlIwkaVZ7yT6gMw2GOjkVh+EaOYx1MejKCUKG0rszZOmripBlMxXH" +
        @"VG93dmT6YvD8Ohiv7ELbue9EFrK4zuTxAkzTaubrjXfOwdSowMjlLTBe5ngpAaP18dz1SkS8/1oU33MS" +
        @"ilj1epR4Nlz6MQaLfDF8I4u3Y6Gev00dL4CDTi1l0Z1g+tUYbUhCywkfjkqYCVS08z1ofGcF8r2pQpp1" +
        @"ToHiKAwVIRgsXg9Ty7fITV0ojB3GC+Co0ywAbqfBdEUDc18VM6FEC88aBOo+swmZao/W/XLp8v3y+csz" +
        @"k71aRU3oL4RhqCQIo03bkbdjiTB2HDeAVk2Aa1sJoCZAJUxNKioZrVlrgWYNuvM2sQhXWo6jKycYxloZ" +
        @"AcIxVL6RxRiMnO2LJgigcbek38wMmAgw2mgFMDWlWCHEb01KmOoTWJix7AlRBOAtqdzMMZQ1sHhCAA6/" +
        @"KaTt17MCMdqcTHMa9VYxtWoLgKk5xXIcpgYVC5TNqJYANTGEiIShRsaM+AsAA9cZF4DkB5lLeG7yAvSc" +
        @"l2GgOpaZ0FggzALCkgkBomJhfgAjC9RwMQEjdfEYuRiLAR5DxTcr8H2k9ADXmj5WAMmROLfQU8lzoe/M" +
        @"A37XECAG9yt5/4Upzc38Fpj7LgB91ZaMjDYIKS3XdLBShqtHfUUP6OFaztSLYwGguWtorsYN+vYcdsAS" +
        @"3CkMsqR7oDYe98oiMFATC2N9Esw8FvPVFGobM6DGUG0cBmhetXcVb4eXMPegHMSaTwtAcxeau9JcB/QX" +
        @"4fbZ9ei/IKNZCnqKN6H9dADHcELIcV9kpVpOMDmfRaDleADb72L8nOBex7W8qJcp6wfpKQAk6XLn0Fw1" +
        @"zdu48/5zNPdHf8Un3L0GfwjzXH/czA9A/eHVyFYugFbtDnFLdBoP6FI88WP03KKAxTNEZ5RSD8xFPAHA" +
        @"Yp6jcsFQm5bnajffzJ2raR6GjlN+6MoPREO6D9JkroWcs5Ja9pCWUMJ4JvUS9cBcxL8ASNJj5tDcmebZ" +
        @"NC/ArTw/pj2cDUiN3vJwdOWtx62CQDQeWY2fIi3mwmgGJb50domzFv8NTaH+Hv8AIEmPprlyDoZunGTP" +
        @"P0tzX/SVh7HylTge54JshRRa1Xzo2BEPRLsVc44wtxbWWOIxAJLD0bNDdUmzac6dC/Mza9FXupFdLxG9" +
        @"ZRtxMmmemLSKsqfYlRq7uYi/AEgOR71FcycMXrfuvPvMGvSWhvBOJ+JuSQhuFfpDp5KKSW9S9hSLr96j" +
        @"Z/u08RCA5HAkzRWzaJ4F3MtD92malwSzkcShh2MmU69VzkN6rFsR3xfGEw8bAM/cKUy7RZhn2sx90FvM" +
        @"RtMQS/MgdJ/9CNnW1C+nZlNi1xMPG8ALOl41fYdoMvk8cx/co+nIpSh05/vhRKwourk4GO0siu0Navwp" +
        @"/2vYAKaX7VoEtMTg7nk/9vcQftPluF3ox52vo7nbs9+5PWwAjtkKF2QlzEFmvE0JzshK5JkrXHEo6j/Y" +
        @"uT1sAKJDiXv8cAezS1yzZ79ze9gARJcSEA93MLsmds2eFDaA5xSTJv0JoSFYOucY6VYAAAAASUVORK5C" +
        @"YII=";
    }
  }
}