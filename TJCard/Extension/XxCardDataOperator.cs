﻿using System.ComponentModel;

namespace TJCard
{
  public static class XxCardDataOperator
  {
    public static void Add
      (
      this BindingList<CardProperty> list, 
      PropertyType type,
      string idRow,
      string idParent, 
      string idProperty, 
      string name, 
      string value, 
      bool readOnly
      )
    {
      list.Add
        (
        CardProperty.Create
          (
          type,
          idRow, 
          idParent, 
          idProperty, 
          name, 
          value, 
          readOnly
          )
        );
    }
  }
}