﻿using Telerik.WinControls.UI;

namespace TJCard
{
  public static class XxRadGridView
  {
    public static void ZzGridClearSelection(this RadGridView grid)
    {
      if (grid == null) return;
      grid.FilterDescriptors.Clear();
      grid.GridNavigator.ClearSelection();
      grid.CurrentRow = null;
    }
  }
}