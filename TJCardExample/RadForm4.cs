﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace TJCardExample
{
  public partial class RadForm4 : Telerik.WinControls.UI.RadForm
  {
    public RadForm4()
    {
      InitializeComponent();
      this.Shown += RadForm4_Shown;
    }

    private void RadForm4_Shown(object sender, EventArgs e)
    {
      this.TxDate.DateTimePickerElement.ShowTimePicker = true;
      this.TxDate.Format = DateTimePickerFormat.Custom;
      this.TxDate.CustomFormat = "yyyy - MM - dd     HH : mm : ss";
      (this.TxDate.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMinSize = new System.Drawing.Size(350, 350);
    }
  }
}
