﻿namespace TJCardExample
{
    partial class RadForm6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadForm6));
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
      this.TxFileName = new Telerik.WinControls.UI.RadTextBoxControl();
      this.PnTop = new Telerik.WinControls.UI.RadPanel();
      this.PnBottom = new Telerik.WinControls.UI.RadPanel();
      this.BxSave = new Telerik.WinControls.UI.RadButton();
      this.BxClear = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      this.radPageViewPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxFileName)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).BeginInit();
      this.PnBottom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.BxSave)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxClear)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvMain
      // 
      this.PvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PvMain.Controls.Add(this.radPageViewPage1);
      this.PvMain.Location = new System.Drawing.Point(51, -2);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.radPageViewPage1;
      this.PvMain.Size = new System.Drawing.Size(395, 455);
      this.PvMain.TabIndex = 6;
      // 
      // radPageViewPage1
      // 
      this.radPageViewPage1.Controls.Add(this.TxFileName);
      this.radPageViewPage1.Controls.Add(this.PnTop);
      this.radPageViewPage1.Controls.Add(this.PnBottom);
      this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(112F, 28F);
      this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
      this.radPageViewPage1.Name = "radPageViewPage1";
      this.radPageViewPage1.Size = new System.Drawing.Size(374, 407);
      this.radPageViewPage1.Text = "radPageViewPage1";
      // 
      // TxFileName
      // 
      this.TxFileName.Dock = System.Windows.Forms.DockStyle.Top;
      this.TxFileName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxFileName.IsReadOnly = true;
      this.TxFileName.Location = new System.Drawing.Point(0, 35);
      this.TxFileName.MaxLength = 2000;
      this.TxFileName.Name = "TxFileName";
      this.TxFileName.Size = new System.Drawing.Size(374, 35);
      this.TxFileName.TabIndex = 3;
      // 
      // PnTop
      // 
      this.PnTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTop.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTop.Location = new System.Drawing.Point(0, 0);
      this.PnTop.Name = "PnTop";
      this.PnTop.Size = new System.Drawing.Size(374, 35);
      this.PnTop.TabIndex = 2;
      // 
      // PnBottom
      // 
      this.PnBottom.Controls.Add(this.BxClear);
      this.PnBottom.Controls.Add(this.BxSave);
      this.PnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnBottom.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnBottom.Location = new System.Drawing.Point(0, 372);
      this.PnBottom.Name = "PnBottom";
      this.PnBottom.Size = new System.Drawing.Size(374, 35);
      this.PnBottom.TabIndex = 2;
      // 
      // BxSave
      // 
      this.BxSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.BxSave.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
      this.BxSave.Image = ((System.Drawing.Image)(resources.GetObject("BxSave.Image")));
      this.BxSave.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      this.BxSave.Location = new System.Drawing.Point(8, 6);
      this.BxSave.Name = "BxSave";
      this.BxSave.Size = new System.Drawing.Size(26, 24);
      this.BxSave.TabIndex = 1;
      this.BxSave.Text = "radButton1";
      // 
      // BxClear
      // 
      this.BxClear.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.BxClear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
      this.BxClear.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      this.BxClear.Location = new System.Drawing.Point(341, 6);
      this.BxClear.Name = "BxClear";
      this.BxClear.Size = new System.Drawing.Size(26, 24);
      this.BxClear.TabIndex = 1;
      this.BxClear.Text = "radButton1";
      // 
      // RadForm6
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(497, 450);
      this.Controls.Add(this.PvMain);
      this.Name = "RadForm6";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadForm6";
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      this.radPageViewPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TxFileName)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).EndInit();
      this.PnBottom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.BxSave)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxClear)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPageView PvMain;
    private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
    private Telerik.WinControls.UI.RadPanel PnTop;
    private Telerik.WinControls.UI.RadPanel PnBottom;
    public Telerik.WinControls.UI.RadTextBoxControl TxFileName;
    private Telerik.WinControls.UI.RadButton BxClear;
    private Telerik.WinControls.UI.RadButton BxSave;
  }
}
