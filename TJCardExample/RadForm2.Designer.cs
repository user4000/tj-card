﻿namespace TJCardExample
{
    partial class RadForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadForm2));
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
      this.PnTop = new Telerik.WinControls.UI.RadPanel();
      this.PnBottom = new Telerik.WinControls.UI.RadPanel();
      this.TxPassword = new Telerik.WinControls.UI.RadButtonTextBox();
      this.BxClear = new Telerik.WinControls.UI.RadImageButtonElement();
      this.BxKey = new Telerik.WinControls.UI.RadImageButtonElement();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      this.radPageViewPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxPassword)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvMain
      // 
      this.PvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PvMain.Controls.Add(this.radPageViewPage1);
      this.PvMain.Location = new System.Drawing.Point(204, 28);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.radPageViewPage1;
      this.PvMain.Size = new System.Drawing.Size(452, 498);
      this.PvMain.TabIndex = 3;
      // 
      // radPageViewPage1
      // 
      this.radPageViewPage1.Controls.Add(this.TxPassword);
      this.radPageViewPage1.Controls.Add(this.PnTop);
      this.radPageViewPage1.Controls.Add(this.PnBottom);
      this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(112F, 28F);
      this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
      this.radPageViewPage1.Name = "radPageViewPage1";
      this.radPageViewPage1.Size = new System.Drawing.Size(431, 450);
      this.radPageViewPage1.Text = "radPageViewPage1";
      // 
      // PnTop
      // 
      this.PnTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTop.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTop.Location = new System.Drawing.Point(0, 0);
      this.PnTop.Name = "PnTop";
      this.PnTop.Size = new System.Drawing.Size(431, 35);
      this.PnTop.TabIndex = 2;
      // 
      // PnBottom
      // 
      this.PnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnBottom.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnBottom.Location = new System.Drawing.Point(0, 415);
      this.PnBottom.Name = "PnBottom";
      this.PnBottom.Size = new System.Drawing.Size(431, 35);
      this.PnBottom.TabIndex = 2;
      // 
      // TxPassword
      // 
      this.TxPassword.AutoSize = false;
      this.TxPassword.Dock = System.Windows.Forms.DockStyle.Top;
      this.TxPassword.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxPassword.LeftButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.BxKey});
      this.TxPassword.Location = new System.Drawing.Point(0, 35);
      this.TxPassword.MaxLength = 4096;
      this.TxPassword.Name = "TxPassword";
      this.TxPassword.PasswordChar = '*';
      this.TxPassword.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.BxClear});
      this.TxPassword.Size = new System.Drawing.Size(431, 35);
      this.TxPassword.TabIndex = 3;
      // 
      // BxClear
      // 
      this.BxClear.AutoSize = false;
      this.BxClear.Bounds = new System.Drawing.Rectangle(0, 0, 17, 33);
      this.BxClear.Image = ((System.Drawing.Image)(resources.GetObject("BxClear.Image")));
      this.BxClear.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.BxClear.Name = "BxClear";
      this.BxClear.ShowBorder = false;
      this.BxClear.Text = "";
      // 
      // BxKey
      // 
      this.BxKey.AutoSize = false;
      this.BxKey.Bounds = new System.Drawing.Rectangle(0, 0, 39, 30);
      this.BxKey.Image = ((System.Drawing.Image)(resources.GetObject("BxKey.Image")));
      this.BxKey.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
      this.BxKey.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.BxKey.Name = "BxKey";
      this.BxKey.ShowBorder = false;
      this.BxKey.Text = "";
      // 
      // RadForm2
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(861, 554);
      this.Controls.Add(this.PvMain);
      this.Name = "RadForm2";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadForm2";
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      this.radPageViewPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxPassword)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPageView PvMain;
    private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
    private Telerik.WinControls.UI.RadPanel PnTop;
    private Telerik.WinControls.UI.RadPanel PnBottom;
    public Telerik.WinControls.UI.RadButtonTextBox TxPassword;
    private Telerik.WinControls.UI.RadImageButtonElement BxClear;
    public Telerik.WinControls.UI.RadImageButtonElement BxKey;
  }
}
