﻿namespace TJCardExample
{
    partial class RadForm7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
      Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
      this.PnTop = new Telerik.WinControls.UI.RadPanel();
      this.PnBottom = new Telerik.WinControls.UI.RadPanel();
      this.GxClassificator = new Telerik.WinControls.UI.RadGridView();
      this.TxValue = new Telerik.WinControls.UI.RadTextBox();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      this.radPageViewPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).BeginInit();
      this.PnTop.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxClassificator)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxClassificator.MasterTemplate)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxValue)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvMain
      // 
      this.PvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PvMain.Controls.Add(this.radPageViewPage1);
      this.PvMain.Location = new System.Drawing.Point(101, 18);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.radPageViewPage1;
      this.PvMain.Size = new System.Drawing.Size(486, 525);
      this.PvMain.TabIndex = 6;
      // 
      // radPageViewPage1
      // 
      this.radPageViewPage1.Controls.Add(this.GxClassificator);
      this.radPageViewPage1.Controls.Add(this.PnTop);
      this.radPageViewPage1.Controls.Add(this.PnBottom);
      this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(112F, 28F);
      this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
      this.radPageViewPage1.Name = "radPageViewPage1";
      this.radPageViewPage1.Size = new System.Drawing.Size(465, 477);
      this.radPageViewPage1.Text = "radPageViewPage1";
      // 
      // PnTop
      // 
      this.PnTop.Controls.Add(this.TxValue);
      this.PnTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTop.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTop.Location = new System.Drawing.Point(0, 0);
      this.PnTop.Name = "PnTop";
      this.PnTop.Size = new System.Drawing.Size(465, 35);
      this.PnTop.TabIndex = 2;
      // 
      // PnBottom
      // 
      this.PnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnBottom.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnBottom.Location = new System.Drawing.Point(0, 442);
      this.PnBottom.Name = "PnBottom";
      this.PnBottom.Size = new System.Drawing.Size(465, 35);
      this.PnBottom.TabIndex = 2;
      // 
      // GxClassificator
      // 
      this.GxClassificator.AutoScroll = true;
      this.GxClassificator.Dock = System.Windows.Forms.DockStyle.Fill;
      this.GxClassificator.EnableAnalytics = false;
      this.GxClassificator.EnableGestures = false;
      this.GxClassificator.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.GxClassificator.Location = new System.Drawing.Point(0, 35);
      // 
      // 
      // 
      this.GxClassificator.MasterTemplate.AllowAddNewRow = false;
      this.GxClassificator.MasterTemplate.AllowCellContextMenu = false;
      this.GxClassificator.MasterTemplate.AllowColumnChooser = false;
      this.GxClassificator.MasterTemplate.AllowColumnHeaderContextMenu = false;
      this.GxClassificator.MasterTemplate.AllowColumnReorder = false;
      this.GxClassificator.MasterTemplate.AllowDeleteRow = false;
      this.GxClassificator.MasterTemplate.AllowDragToGroup = false;
      this.GxClassificator.MasterTemplate.AllowEditRow = false;
      this.GxClassificator.MasterTemplate.AllowRowHeaderContextMenu = false;
      this.GxClassificator.MasterTemplate.AllowRowReorder = true;
      this.GxClassificator.MasterTemplate.AllowRowResize = false;
      this.GxClassificator.MasterTemplate.AllowSearchRow = true;
      this.GxClassificator.MasterTemplate.AutoGenerateColumns = false;
      this.GxClassificator.MasterTemplate.ClipboardPasteMode = Telerik.WinControls.UI.GridViewClipboardPasteMode.EnableWithNotifications;
      gridViewTextBoxColumn1.AllowHide = false;
      gridViewTextBoxColumn1.HeaderText = "id";
      gridViewTextBoxColumn1.MaxLength = 500;
      gridViewTextBoxColumn1.Name = "CnIdObject";
      gridViewTextBoxColumn1.Width = 120;
      gridViewTextBoxColumn2.HeaderText = "Name";
      gridViewTextBoxColumn2.MaxLength = 4000;
      gridViewTextBoxColumn2.Name = "CnNameObject";
      gridViewTextBoxColumn2.ReadOnly = true;
      gridViewTextBoxColumn2.Width = 600;
      this.GxClassificator.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
      this.GxClassificator.MasterTemplate.EnableGrouping = false;
      this.GxClassificator.MasterTemplate.ShowRowHeaderColumn = false;
      this.GxClassificator.MasterTemplate.ViewDefinition = tableViewDefinition1;
      this.GxClassificator.Name = "GxClassificator";
      this.GxClassificator.ReadOnly = true;
      this.GxClassificator.ShowGroupPanel = false;
      this.GxClassificator.ShowGroupPanelScrollbars = false;
      this.GxClassificator.ShowNoDataText = false;
      this.GxClassificator.Size = new System.Drawing.Size(465, 407);
      this.GxClassificator.TabIndex = 3;
      // 
      // TxValue
      // 
      this.TxValue.AutoSize = false;
      this.TxValue.Location = new System.Drawing.Point(101, 5);
      this.TxValue.MaxLength = 1000;
      this.TxValue.Name = "TxValue";
      this.TxValue.ReadOnly = true;
      this.TxValue.Size = new System.Drawing.Size(248, 24);
      this.TxValue.TabIndex = 0;
      this.TxValue.Visible = false;
      this.TxValue.WordWrap = false;
      // 
      // RadForm7
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(688, 561);
      this.Controls.Add(this.PvMain);
      this.Name = "RadForm7";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadForm7";
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      this.radPageViewPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).EndInit();
      this.PnTop.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxClassificator.MasterTemplate)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GxClassificator)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxValue)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPageView PvMain;
    private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
    public Telerik.WinControls.UI.RadGridView GxClassificator;
    public Telerik.WinControls.UI.RadTextBox TxValue;
    public Telerik.WinControls.UI.RadPanel PnTop;
    public Telerik.WinControls.UI.RadPanel PnBottom;
  }
}
