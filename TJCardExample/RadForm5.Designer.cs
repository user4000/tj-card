﻿namespace TJCardExample
{
    partial class RadForm5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
      this.PnTop = new Telerik.WinControls.UI.RadPanel();
      this.PnBottom = new Telerik.WinControls.UI.RadPanel();
      this.TsBoolean = new Telerik.WinControls.UI.RadToggleSwitch();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      this.radPageViewPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TsBoolean)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvMain
      // 
      this.PvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PvMain.Controls.Add(this.radPageViewPage1);
      this.PvMain.Location = new System.Drawing.Point(85, 30);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.radPageViewPage1;
      this.PvMain.Size = new System.Drawing.Size(395, 455);
      this.PvMain.TabIndex = 6;
      // 
      // radPageViewPage1
      // 
      this.radPageViewPage1.Controls.Add(this.TsBoolean);
      this.radPageViewPage1.Controls.Add(this.PnTop);
      this.radPageViewPage1.Controls.Add(this.PnBottom);
      this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(112F, 28F);
      this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
      this.radPageViewPage1.Name = "radPageViewPage1";
      this.radPageViewPage1.Size = new System.Drawing.Size(374, 407);
      this.radPageViewPage1.Text = "radPageViewPage1";
      // 
      // PnTop
      // 
      this.PnTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTop.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTop.Location = new System.Drawing.Point(0, 0);
      this.PnTop.Name = "PnTop";
      this.PnTop.Size = new System.Drawing.Size(374, 35);
      this.PnTop.TabIndex = 2;
      // 
      // PnBottom
      // 
      this.PnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnBottom.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnBottom.Location = new System.Drawing.Point(0, 372);
      this.PnBottom.Name = "PnBottom";
      this.PnBottom.Size = new System.Drawing.Size(374, 35);
      this.PnBottom.TabIndex = 2;
      // 
      // TsBoolean
      // 
      this.TsBoolean.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TsBoolean.Location = new System.Drawing.Point(107, 50);
      this.TsBoolean.Name = "TsBoolean";
      this.TsBoolean.Size = new System.Drawing.Size(153, 35);
      this.TsBoolean.TabIndex = 3;
      this.TsBoolean.Value = false;
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsBoolean.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsBoolean.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(107)))), ((int)(((byte)(168)))));
      ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.TsBoolean.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      // 
      // RadForm5
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(564, 515);
      this.Controls.Add(this.PvMain);
      this.Name = "RadForm5";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadForm5";
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      this.radPageViewPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TsBoolean)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPageView PvMain;
    private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
    private Telerik.WinControls.UI.RadPanel PnTop;
    private Telerik.WinControls.UI.RadPanel PnBottom;
    private Telerik.WinControls.UI.RadToggleSwitch TsBoolean;
  }
}
