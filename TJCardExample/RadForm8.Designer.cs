﻿namespace TJCardExample
{
    partial class RadForm8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadForm8));
      this.PvMain = new Telerik.WinControls.UI.RadPageView();
      this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
      this.PnTop = new Telerik.WinControls.UI.RadPanel();
      this.PnBottom = new Telerik.WinControls.UI.RadPanel();
      this.TxFileName = new Telerik.WinControls.UI.RadTextBoxControl();
      this.PnTool = new Telerik.WinControls.UI.RadPanel();
      this.BxOpenFile = new Telerik.WinControls.UI.RadButton();
      this.BxAddFile = new Telerik.WinControls.UI.RadButton();
      this.BxGetDataFromServer = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).BeginInit();
      this.PvMain.SuspendLayout();
      this.radPageViewPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).BeginInit();
      this.PnBottom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxFileName)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnTool)).BeginInit();
      this.PnTool.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.BxOpenFile)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxAddFile)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxGetDataFromServer)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvMain
      // 
      this.PvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PvMain.Controls.Add(this.radPageViewPage1);
      this.PvMain.Location = new System.Drawing.Point(39, -1);
      this.PvMain.Name = "PvMain";
      this.PvMain.SelectedPage = this.radPageViewPage1;
      this.PvMain.Size = new System.Drawing.Size(395, 455);
      this.PvMain.TabIndex = 7;
      // 
      // radPageViewPage1
      // 
      this.radPageViewPage1.Controls.Add(this.PnTool);
      this.radPageViewPage1.Controls.Add(this.TxFileName);
      this.radPageViewPage1.Controls.Add(this.PnTop);
      this.radPageViewPage1.Controls.Add(this.PnBottom);
      this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(112F, 28F);
      this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
      this.radPageViewPage1.Name = "radPageViewPage1";
      this.radPageViewPage1.Size = new System.Drawing.Size(374, 407);
      this.radPageViewPage1.Text = "radPageViewPage1";
      // 
      // PnTop
      // 
      this.PnTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTop.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTop.Location = new System.Drawing.Point(0, 0);
      this.PnTop.Name = "PnTop";
      this.PnTop.Size = new System.Drawing.Size(374, 35);
      this.PnTop.TabIndex = 2;
      // 
      // PnBottom
      // 
      this.PnBottom.Controls.Add(this.BxGetDataFromServer);
      this.PnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.PnBottom.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnBottom.Location = new System.Drawing.Point(0, 372);
      this.PnBottom.Name = "PnBottom";
      this.PnBottom.Size = new System.Drawing.Size(374, 35);
      this.PnBottom.TabIndex = 2;
      // 
      // TxFileName
      // 
      this.TxFileName.Dock = System.Windows.Forms.DockStyle.Top;
      this.TxFileName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxFileName.IsReadOnly = true;
      this.TxFileName.Location = new System.Drawing.Point(0, 35);
      this.TxFileName.MaxLength = 2000;
      this.TxFileName.Name = "TxFileName";
      this.TxFileName.Size = new System.Drawing.Size(374, 35);
      this.TxFileName.TabIndex = 4;
      // 
      // PnTool
      // 
      this.PnTool.Controls.Add(this.BxAddFile);
      this.PnTool.Controls.Add(this.BxOpenFile);
      this.PnTool.Dock = System.Windows.Forms.DockStyle.Top;
      this.PnTool.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PnTool.Location = new System.Drawing.Point(0, 70);
      this.PnTool.Name = "PnTool";
      this.PnTool.Size = new System.Drawing.Size(374, 63);
      this.PnTool.TabIndex = 5;
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnTool.GetChildAt(0).GetChildAt(1))).Width = 1F;
      ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnTool.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
      // 
      // BxOpenFile
      // 
      this.BxOpenFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.BxOpenFile.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("BxOpenFile.Image")));
      this.BxOpenFile.Location = new System.Drawing.Point(3, 30);
      this.BxOpenFile.Name = "BxOpenFile";
      this.BxOpenFile.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.BxOpenFile.Size = new System.Drawing.Size(112, 25);
      this.BxOpenFile.TabIndex = 0;
      this.BxOpenFile.Text = "Открыть";
      // 
      // BxAddFile
      // 
      this.BxAddFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.BxAddFile.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxAddFile.Image = ((System.Drawing.Image)(resources.GetObject("BxAddFile.Image")));
      this.BxAddFile.Location = new System.Drawing.Point(259, 30);
      this.BxAddFile.Name = "BxAddFile";
      this.BxAddFile.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.BxAddFile.Size = new System.Drawing.Size(112, 25);
      this.BxAddFile.TabIndex = 0;
      this.BxAddFile.Text = "Добавить";
      // 
      // BxGetDataFromServer
      // 
      this.BxGetDataFromServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.BxGetDataFromServer.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxGetDataFromServer.Image = ((System.Drawing.Image)(resources.GetObject("BxGetDataFromServer.Image")));
      this.BxGetDataFromServer.Location = new System.Drawing.Point(68, 5);
      this.BxGetDataFromServer.Name = "BxGetDataFromServer";
      this.BxGetDataFromServer.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.BxGetDataFromServer.Size = new System.Drawing.Size(230, 25);
      this.BxGetDataFromServer.TabIndex = 6;
      this.BxGetDataFromServer.Text = "  Получить данные с сервера";
      // 
      // RadForm8
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(472, 452);
      this.Controls.Add(this.PvMain);
      this.Name = "RadForm8";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadForm8";
      ((System.ComponentModel.ISupportInitialize)(this.PvMain)).EndInit();
      this.PvMain.ResumeLayout(false);
      this.radPageViewPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PnTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnBottom)).EndInit();
      this.PnBottom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TxFileName)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.PnTool)).EndInit();
      this.PnTool.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.BxOpenFile)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxAddFile)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxGetDataFromServer)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    public Telerik.WinControls.UI.RadPageView PvMain;
    private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
    private Telerik.WinControls.UI.RadPanel PnTop;
    private Telerik.WinControls.UI.RadPanel PnBottom;
    private Telerik.WinControls.UI.RadPanel PnTool;
    public Telerik.WinControls.UI.RadTextBoxControl TxFileName;
    private Telerik.WinControls.UI.RadButton BxAddFile;
    private Telerik.WinControls.UI.RadButton BxOpenFile;
    public Telerik.WinControls.UI.RadButton BxGetDataFromServer;
  }
}
