﻿namespace TJCardExample
{
    partial class RadForm10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadForm10));
      this.radImageEditor1 = new Telerik.WinControls.UI.RadImageEditor();
      ((System.ComponentModel.ISupportInitialize)(this.radImageEditor1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // radImageEditor1
      // 
      this.radImageEditor1.AllowDrop = true;
      this.radImageEditor1.Location = new System.Drawing.Point(83, 56);
      this.radImageEditor1.Name = "radImageEditor1";
      this.radImageEditor1.Size = new System.Drawing.Size(599, 482);
      this.radImageEditor1.TabIndex = 0;
      this.radImageEditor1.Text = "radImageEditor1";
      ((Telerik.WinControls.UI.RadButtonElement)(this.radImageEditor1.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(0))).Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
      ((Telerik.WinControls.UI.RadButtonElement)(this.radImageEditor1.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(0))).ToolTipText = "Open Image";
      ((Telerik.WinControls.UI.RadButtonElement)(this.radImageEditor1.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(0))).Enabled = false;
      ((Telerik.WinControls.UI.RadButtonElement)(this.radImageEditor1.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(1))).Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
      ((Telerik.WinControls.UI.RadButtonElement)(this.radImageEditor1.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(1))).ToolTipText = "Save Image";
      ((Telerik.WinControls.UI.RadButtonElement)(this.radImageEditor1.GetChildAt(0).GetChildAt(0).GetChildAt(0).GetChildAt(2).GetChildAt(3).GetChildAt(0).GetChildAt(1))).Enabled = false;
      // 
      // RadForm10
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(767, 594);
      this.Controls.Add(this.radImageEditor1);
      this.Name = "RadForm10";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "RadForm10";
      ((System.ComponentModel.ISupportInitialize)(this.radImageEditor1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private Telerik.WinControls.UI.RadImageEditor radImageEditor1;
  }
}
